from flask import Flask, render_template, redirect, jsonify, request, url_for
from restaurant import MenuItem, Order, Table
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import distinct, desc
from flask_cors import CORS
from werkzeug.local import LocalProxy
from werkzeug.local import Local

app = Flask(__name__)
CORS(app)

# Database connection
engine = create_engine('sqlite:///restaurant.db')
Session = sessionmaker(bind=engine)

local = Local()

@app.before_request
def before_request():
    local.session = Session()

@app.teardown_request
def teardown_request(exception=None):
    session = getattr(local, 'session', None)
    if session is not None:
        session.close()

@app.route('/api/restaurants')
def get_restaurants():
    # Query your database to retrieve 
    restaurants = local.session.query(Table.branch_number).distinct().all()
    print("Restaurants :", restaurants)
    return jsonify([{"Restaurants": restaurant[0]} for restaurant in restaurants])

@app.route('/api/branches')
def get_branches():
    # Query your database to retrieve the branches
    branches = local.session.query(Table.branch).distinct().all()
    print("Branches: ", branches)
    return jsonify([{"branch": branch[0]} for branch in branches])


@app.route('/api/menuitems')
def get_menuitems():
    # Query your database to retrieve 
    menu_items= local.session.query(MenuItem.item, MenuItem.price).all()
    print("Menu items: ", menu_items)
    return jsonify([{"menuitem": menu_item[0], "price": menu_item[1]} for menu_item in menu_items])

@app.route('/')
def order_page():
    # Fetch menu items from the database
    # menu_items = session.query(MenuItem).all()
    # branches = session.query(Table.branch).distinct().all()
    # restaurants = session.query(Table.branch_number).distinct().all()
    # return render_template('order_page.html', menu_items=menu_items, branches = branches, restaurants = restaurants )
    return render_template('order_page.html' )
    

@app.route('/place_order', methods=['POST'])
def place_order():
    # Retrieve order item names, quantities, and prices from the form
    order_names = request.form.getlist('orderItemName[]')
    order_quantities = request.form.getlist('orderItemQuantity[]')
    order_prices = request.form.getlist('orderItemPrice[]')

    order_items = []
    total_cost = 0.0

    # Iterate over the order items
    for name, quantity, price in zip(order_names, order_quantities, order_prices):
        item_name = name
        item_quantity = int(quantity)
        item_price = float(price)
        item_total_cost = item_price * item_quantity
        total_cost += item_total_cost

        # Append the formatted item string to the order items list
        order_items.append(f"{item_name} - x {item_quantity} ${item_total_cost:.2f}")

    # Retrieve branch and restaurant from the form
    branch = request.form['branch']
    restaurant = request.form['restaurant']

    # Insert order into the database
    order_info = ', '.join(order_items)

    # Render the payment.html template with order information
    return render_template('payment.html', orderinfo=order_info, total_cost=total_cost, branch=branch, restaurant=restaurant)


@app.route('/payment', methods=['POST'])
def payment():
    branch = request.form.get('branch')
    restaurant = request.form.get('restaurant')
    orderinfo = request.form.get('orderinfo')
    total_cost = request.form.get('total_cost')

    print("Branch:", branch)
    print("Restaurant:", restaurant)
    print("Order info:", orderinfo)
    print("Total cost:", total_cost)

    # Insert order into the database
    new_order = Order(orderinfo=orderinfo, cost=total_cost, branch=branch, restaurant=restaurant)
    local.session.add(new_order)
    local.session.commit()

    new_order_id = local.session.query(Order.id).order_by(desc(Order.id)).first()[0]
    print("order id: ", new_order_id)

    return render_template('order_confirmation.html', orderinfo=orderinfo, total_cost=total_cost, branch=branch, restaurant=restaurant, order_id=new_order_id)


@app.route('/search', methods=['POST'])
def search():
    # Get the order ID from the form
    order_id = request.form.get('orderID')

    order = local.session.query(Order).filter_by(id=order_id).first()

    if order:
        return render_template("order_search.html", orderinfo=order.orderinfo, cost = order.cost , branch = order.branch, restaurant = order.restaurant)
    else:
        return jsonify({'error': 'Order not found'}), 404

if __name__ == '__main__':
    app.run(debug=True, port=8000, host='192.168.1.6')
