from os import error
from tkinter import *
import json
import sqlite3
from sqlalchemy import create_engine, Column, String, Integer, ForeignKey
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base
from tkinter import Text  # Make sure you have this import


Base = declarative_base()

class FoodItem(Base):
    __tablename__ = 'fooditem'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, nullable=False)
    quantity = Column(Integer, nullable=False)

class FoodCategory(Base):
    __tablename__ = 'foodcategory'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, nullable=False)
    description = Column(String, nullable=False)

engine = create_engine('sqlite:///item.db', echo=True)
Base.metadata.create_all(engine)

class FoodItemApp:
    def __init__(self, parent, session):
        self.parent = parent
        self.session = session
        self.parent.title("Items")
        self.center_window(self.parent)

        self.parent.geometry("700x200")

        self.uName = StringVar()
        self.uQuantity = StringVar()

        engine = create_engine('sqlite:///item.db', echo=True)
        Base.metadata.create_all(engine)
        Session = sessionmaker(bind=engine)
        self.session = Session()

        self.lstName = Listbox(self.parent, width=15, height=10)
        self.yscroll = Scrollbar(command=self.lstName.yview)
        self.yscroll.grid(row=0, column=3, sticky=NS, rowspan=3)

        self.lstName.grid(row=0, padx=0, pady=0, sticky=W, columnspan=2, rowspan=3)
        self.lstName.configure(yscrollcommand=self.yscroll.set)
        self.yscroll.config(command=self.lstName.yview)

        self.txt = Text(self.parent, height=10)
        self.txt.grid(padx=5, pady=0, row=0, column=5)

        f = Frame(self.parent)
        f.grid(row=1, column=5)

        Button(f, text="Add", command=self.add_new, width=10).grid(row=0, column=0, sticky=W, pady=5)
        Button(f, text="Edit", command=self.edit, width=10).grid(row=0, column=1, sticky=W, pady=5)
        Button(f, text="Remove", command=self.remove, width=10).grid(row=0, column=2, sticky=E, pady=5)

        self.loaded_list = self.upload()
        if self.loaded_list is not None:
            for line in self.loaded_list:
                countryName = line[0]
                self.lstName.insert('end', countryName)

        self.lstName.bind('<<ListboxSelect>>', self.select)

    def upload(self):
        food_items = self.session.query(FoodItem).order_by(FoodItem.name).all()
        lst = [(item.name, item.quantity) for item in food_items]
        return lst

    def insert_data(self, lst):
        try:
            food_item = FoodItem(name=lst[0], quantity=lst[1])
            self.session.add(food_item)
            self.session.commit()
        except Exception as err:
            print("Failed to insert", err)
        finally:
            print("Connection closed")

    def delete_data(self, lst):
        try:
            food_item = self.session.query(FoodItem).filter_by(name=lst).first()
            if food_item:
                self.session.delete(food_item)
                self.session.commit()
        except Exception as error:
            print("Failed to delete", error)
        finally:
            print("Connection closed")

    


    def select(self, evt):
        value = self.lstName.curselection()[0]
        templist = self.loaded_list[value]

        self.txt.delete(1.0, END)
        for i in templist:
            self.txt.insert(END, str(i) + '\n')

    def add_new(self):
        nMem = Toplevel(self.parent)
        nMem.title("New Item")
        nMem.geometry("300x250")

        Label(nMem, text="Item Name:").grid(row=0, column=0)
        Entry(nMem, textvariable=self.uName).grid(row=0, column=1)
        Label(nMem, text="Quantity:").grid(row=1, column=0)
        Entry(nMem, textvariable=self.uQuantity).grid(row=1, column=1)

        Button(nMem, text="Add", command=self.register).grid(row=4, column=0)
        Button(nMem, text="Cancel", command=nMem.destroy).grid(row=4, column=1)


    def update(self, old_name, listbox):
        new_data = [self.uName.get(), self.uQuantity.get()]
        self.update_data(new_data, old_name)

    # Reload the list
        self.loaded_list = self.upload()
        listbox.delete(0, END)
        for line in self.loaded_list:
            itemName = line[0]

            listbox.insert('end', itemName)

    
    def edit(self):
        selected_index = self.lstName.curselection()
        if selected_index:
            nMem = Toplevel(self.parent)
            nMem.title("Edit Item")
            nMem.geometry("300x250")

        # Get the selected item from the listbox
            selected_item = self.lstName.get(selected_index)

        # Extract the name from the selected item
            old_name = selected_item
            self.uName.set(old_name)

            Label(nMem, text="Item Name:").grid(row=0, column=0)
            Entry(nMem, textvariable=self.uName).grid(row=0, column=1)
            Label(nMem, text="Quantity:").grid(row=1, column=0)
            Entry(nMem, textvariable=self.uQuantity).grid(row=1, column=1)

        # Pass the listbox as an argument to the update_data method
            Button(nMem, text="Update", command=lambda: self.update_data([self.uName.get(), self.uQuantity.get()], old_name, self.lstName)).grid(row=4, column=0)
            Button(nMem, text="Cancel", command=nMem.destroy).grid(row=4, column=1)


    def update_data(self, new_data, old_name, listbox): 
        try:
        # Retrieve the FoodItem object based on the old name
            food_item = self.session.query(FoodItem).filter_by(name=old_name).first()
            print("old name", old_name)
            print("item ",food_item)

            if food_item:
            # Update the FoodItem object with the new data
                food_item.name = new_data[0]
                food_item.quantity = int(new_data[1])  # Convert quantity to int

                # new_food_item = FoodItem(name=new_data[0], quantity=int(new_data[1]))
                # self.session.add(new_food_item)
                self.session.commit()

            # Reload the list
                self.loaded_list = self.upload()
                listbox.delete(0, END)
                for line in self.loaded_list:
                    itemName = line[0]
                    listbox.insert('end', itemName)
        except Exception as error:
            print("Failed to update", error)
        finally:
            print("Connection closed")

    
    def center_window(self, window):
        window.update_idletasks()  # Ensure that window attributes are updated
        width = window.winfo_width()
        height = window.winfo_height()

        x_position = (window.winfo_screenwidth() - width) // 2
        y_position = (window.winfo_screenheight() - height) // 2

    def remove(self):
        value = self.lstName.curselection()[0]
        templist = self.loaded_list[value]

        self.delete_data(templist[0])
        self.loaded_list.remove(templist)

        self.lstName.delete(0, END)
        self.txt.delete(1.0, END)

        for line in self.loaded_list:
            ItemName = line[0]
            self.lstName.insert('end', ItemName)

    def register(self):
        list1 = [self.uName.get(), self.uQuantity.get()]
        self.loaded_list.append(list1)
        self.insert_data(list1)
        self.lstName.insert(END, list1[0])

class FoodCategoryApp:
    def __init__(self, parent, session):
        self.parent = parent
        self.session = session
        self.parent.title("Category")
        self.center_window(self.parent)

        self.parent.geometry("700x200")

        self.uName = StringVar()
        self.uDescription = StringVar()

        self.lstName = Listbox(self.parent, width=15, height=10)
        self.yscroll = Scrollbar(command=self.lstName.yview)
        self.yscroll.grid(row=0, column=3, sticky=NS, rowspan=3)

        self.lstName.grid(row=0, padx=0, pady=0, sticky=W, columnspan=2, rowspan=3)
        self.lstName.configure(yscrollcommand=self.yscroll.set)
        self.yscroll.config(command=self.lstName.yview)

        self.txt = Text(self.parent, width=30, height=10)
        self.txt.grid(padx=5, pady=0, row=0, column=5)

        f = Frame(self.parent)
        f.grid(row=1, column=5)

        Button(f, text="Add", command=self.add_new, width=10).grid(row=0, column=0, sticky=W, pady=5)
        Button(f, text="Edit", command=self.edit, width=10).grid(row=0, column=1, sticky=W, pady=5)
        Button(f, text="Remove", command=self.remove, width=10).grid(row=0, column=2, sticky=E, pady=5)

        self.loaded_list = self.upload()
        if self.loaded_list is not None:
            for line in self.loaded_list:
                categoryName = line[0]
                self.lstName.insert('end', categoryName)

        self.lstName.bind('<<ListboxSelect>>', self.select)

    def upload(self):
        try:
            categories = self.session.query(FoodCategory).order_by(FoodCategory.name).all()
            lst = [(category.name, category.description) for category in categories]
            return lst
        except Exception as error:
            print("Failed to fetch data", error)
        finally:
            print("Connection closed")

    def insert_data(self, lst):
        try:
            new_category = FoodCategory(name=lst[0], description=lst[1])
            self.session.add(new_category)
            self.session.commit()
        except Exception as error:
            print("Failed to insert", error)
        finally:
            print("Connection closed")

    def delete_data(self, lst):
        try:
            category_to_delete = self.session.query(FoodCategory).filter_by(name=lst).first()
            if category_to_delete:
                self.session.delete(category_to_delete)
                self.session.commit()
            else:
                print("Category not found.")
        except Exception as error:
            print("Failed to delete", error)
        finally:
            print("Connection closed")

    def update_data(self, lst, old_name, listbox):
        try:
            category_to_update = self.session.query(FoodCategory).filter_by(name=old_name).first()
            if category_to_update:
                category_to_update.name = lst[0]
                category_to_update.description = lst[1]
                # new_food_item = FoodItem(name = lst[0], description=lst[1])
                # self.session.add(new_food_item)
                self.session.commit()
            else:
                print("Category not found.")
        except Exception as error:
            print("Failed to update", error)
        finally:
            print("Connection closed")

    def select(self, evt):
        value = self.lstName.curselection()[0]
        templist = self.loaded_list[value]

        self.txt.delete(1.0, END)
        for i in templist:
            self.txt.insert(END, str(i) + '\n')

    def add_new(self):
        nMem = Toplevel(self.parent)
        nMem.title("New Category")
        nMem.geometry("300x250")

        Label(nMem, text="Category Name:").grid(row=0, column=0)
        Entry(nMem, textvariable=self.uName).grid(row=0, column=1)
        Label(nMem, text="Description:").grid(row=1, column=0)
        Entry(nMem, textvariable=self.uDescription).grid(row=1, column=1)

        Button(nMem, text="Add", command=self.register).grid(row=4, column=0)
        Button(nMem, text="Cancel", command=nMem.destroy).grid(row=4, column=1)

    
    def update(self, old_name, listbox):
        new_data = [self.uName.get(), self.uDescription.get()]
        self.update_data(new_data, old_name)

        # Reload the list
        self.loaded_list = self.upload()
        listbox.delete(0, END)
        for line in self.loaded_list:
            categoryName = line[0]
            self.lstName.insert('end', categoryName)
    

    def edit(self):
        selected_index = self.lstName.curselection()
        if selected_index:
            nMem = Toplevel(self.parent)
            nMem.title("Edit Category")
            nMem.geometry("300x250")

            old_name = self.lstName.get(selected_index)
            self.uName.set(old_name)

            Label(nMem, text="Category Name:").grid(row=0, column=0)
            Entry(nMem, textvariable=self.uName).grid(row=0, column=1)
            Label(nMem, text="Description:").grid(row=1, column=0)
            Entry(nMem, textvariable=self.uDescription).grid(row=1, column=1)

            Button(nMem, text="Update", command=lambda: self.update_data([self.uName.get(), self.uDescription.get()], old_name, self.lstName)).grid(row=4, column=0)
            Button(nMem, text="Cancel", command=nMem.destroy).grid(row=4, column=1)


    def center_window(self, window):
        window.update_idletasks()  # Ensure that window attributes are updated
        width = window.winfo_width()
        height = window.winfo_height()

        x_position = (window.winfo_screenwidth() - width) // 2
        y_position = (window.winfo_screenheight() - height) // 2

    def remove(self):
        value = self.lstName.curselection()[0]
        templist = self.loaded_list[value]

        self.delete_data(templist[0])
        self.loaded_list.remove(templist)

        self.lstName.delete(0, END)
        self.txt.delete(1.0, END)

        for line in self.loaded_list:
            categoryName = line[0]
            self.lstName.insert('end', categoryName)

    def register(self):
        list1 = [self.uName.get(), self.uDescription.get()]
        self.loaded_list.append(list1)
        self.insert_data(list1)
        self.lstName.insert(END, list1[0])
        
class Inventory:
    def __init__(self, parent):
        self.parent = parent
        self.parent.title("Inventory")

        self.center_window(self.parent)

        self.parent.geometry("300x100")
        self.parent.resizable(False, False)

        frame = Frame(self.parent)
        frame.place(relx=0.5, rely=0.5, anchor=CENTER)

        Button(frame, text="Items", command=self.open_food_items, width=20).pack(pady=10)
        Button(frame, text="Categories", command=self.open_food_category, width=20).pack(pady=10)

        # SQLite database setup
        engine = create_engine('sqlite:///item.db', echo=True)
        Base.metadata.create_all(engine)
        Session = sessionmaker(bind=engine)
        self.session = Session()

    def center_window(self, window):
        # Update idle tasks to ensure that the window size is up-to-date
        window.update_idletasks()

        # Get the screen width and height
        screen_width = window.winfo_screenwidth()
        screen_height = window.winfo_screenheight()

        # Get the requested width and height of the window
        requested_width = window.winfo_reqwidth()
        requested_height = window.winfo_reqheight()

        # Calculate the centered position, taking the window size into account
        x_position = (screen_width - requested_width) // 2
        y_position = (screen_height - requested_height) // 2

        # Set the window position
        window.geometry(f"+{x_position}+{y_position}")

    def open_food_items(self):
        food_items_window = Toplevel(self.parent)
        self.center_window(food_items_window)
        food_items_window.geometry("530x200")
        FoodItemApp(food_items_window, self.session)

    def open_food_category(self):
        food_category_window = Toplevel(self.parent)
        self.center_window(food_category_window)
        food_category_window.geometry("530x200")
        FoodCategoryApp(food_category_window, self.session)


if __name__ == "__main__":
    root = Tk()
    reservation_app = Inventory(root)
    root.mainloop()








