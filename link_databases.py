import sqlite3

source_db_path = '/Users/greatmann/Documents/Py programs/reservation.db'
destination_db_path = '/Users/greatmann/Documents/Py programs/restaurant.db'

# Connect to the source and destination databases
source_conn = sqlite3.connect(source_db_path)
destination_conn = sqlite3.connect(destination_db_path)

# Attach databases
cursor = source_conn.cursor()
cursor.execute(f"ATTACH DATABASE '{destination_db_path}' AS destination;")
cursor.execute(f"ATTACH DATABASE '{source_db_path}' AS source;")

# Get the list of tables from the source database (excluding sqlite_sequence)
cursor.execute("SELECT name FROM sqlite_master WHERE type='table' AND name!='sqlite_sequence';")
tables = cursor.fetchall()

# Iterate through tables and copy them to the destination database
for table in tables:
    source_table_name = table[0]
    destination_table_name = f"{source_table_name}"  # Modify as needed

    # Get column names for the source table
    cursor.execute(f"PRAGMA source.table_info({source_table_name});")
    columns = cursor.fetchall()
    column_names = [column[1] for column in columns]

    # Generate the column list for the CREATE TABLE statement
    column_list = ', '.join(column_names)

    # Copy the structure and data from the source table to the destination
    cursor.execute(f"CREATE TABLE IF NOT EXISTS destination.{destination_table_name} AS SELECT {column_list} FROM source.{source_table_name};")

# Detach databases
cursor.execute("DETACH DATABASE source;")
cursor.execute("DETACH DATABASE destination;")

# Commit and close connections
destination_conn.commit()
source_conn.close()
destination_conn.close()
