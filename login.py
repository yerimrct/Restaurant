import tkinter as tk
from tkinter import messagebox, simpledialog, Toplevel
from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from PIL import Image, ImageTk
import hashlib

Base = declarative_base()

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    first_name = Column(String, nullable=False)
    last_name = Column(String, nullable=False)
    username = Column(String, nullable=False, unique=True)
    password = Column(String, nullable=False)
    role = Column(String, nullable=False)
    usertype = Column(String, nullable=False)
    branch = Column(String, nullable=False)
    restaurant_location = Column(String, nullable=False)

class Login:
    def __init__(self, root):
        self.root = root
        self.root.title("HORIZON RESTAURANT")
        self.root.geometry("320x250")

        try:
            logo_image = Image.open("/Users/greatmann/Documents/Py programs/Restaurant/vecteezy_spaghetti-with-white-background-high-quality-ultra_30660701-removebg-preview.png")
        except Exception as e:
            print(f"Error loading logo image: {e}")
            return
        logo_image = logo_image.resize((100, 100), Image.BOX)
        self.logo_image = ImageTk.PhotoImage(logo_image)

        self.canvas = tk.Canvas(root, width=200, height=110)
        self.canvas.grid(row=0, column=0, columnspan=5, rowspan=5, padx=10, pady=10)

        self.canvas.create_image(110, 55, anchor=tk.CENTER, image=self.logo_image)

        tk.Label(root, text="Username:").grid(row=5, column=0)
        tk.Label(root, text="Password:").grid(row=6, column=0)

        self.username_entry = tk.Entry(root)
        self.password_entry = tk.Entry(root, show="*")

        self.username_entry.grid(row=5, column=1)
        self.password_entry.grid(row=6, column=1)

        login_button = tk.Button(root, text="Login", command=self.login)
        login_button.grid(row=7, column=0, pady=10)

        forgotten_password = tk.Button(root, text="Forgotten password or username ?", command=self.forgotten_password_or_username)
        forgotten_password.grid(row=7, column=1, pady=10)

        self.engine = create_engine('sqlite:///restaurant.db', echo=True)
        Base.metadata.create_all(self.engine)
        Session = sessionmaker(bind=self.engine)
        self.session = Session()
    
        self.center_window(self.root)

    def center_window(self, window):
        window.update_idletasks()
        requested_width = window.winfo_reqwidth()
        requested_height = window.winfo_reqheight()
        x_position = (window.winfo_screenwidth() - requested_width) // 2
        y_position = (window.winfo_screenheight() - requested_height) // 2

        window.geometry(f"+{x_position}+{y_position}")


    def login(self):
        entered_username = self.username_entry.get()
        entered_password = self.password_entry.get()

        hashed_password = hashlib.sha256(entered_password.encode('utf-8')).hexdigest()

        user = self.session.query(User).filter_by(username=entered_username, password=hashed_password).first()

        if user:
            messagebox.showinfo("Login Successful", "Welcome, {}!".format(user.username))
        else:
            messagebox.showerror("Login Failed", "Invalid username or password")
    
    def forgotten_password_or_username(self):
        password_or_username = simpledialog.askstring("Forgotten Password", "Have you forgotten your password or username?")
        
        if password_or_username == "password":
            username = simpledialog.askstring("Forgotten Password", "Enter your username:")
       
            if username:
                user = self.session.query(User).filter_by(username=username).first()
            
                if user:
                    self.reset_password_window(user)
                else:
                    messagebox.showerror("Error", "Username not found.")
            else:
                return False
            
        elif password_or_username == "PASSWORD":
            username = simpledialog.askstring("Forgotten Password", "Enter your username:")
       
            if username:
                user = self.session.query(User).filter_by(username=username).first()
            
                if user:
                    self.reset_password_window(user)
                else:
                    messagebox.showerror("Error", "Username not found.")
            else:
                return False

        elif password_or_username == "username":
            id  = simpledialog.askinteger("Forgotten username", "Enter your ID:")
            if id:
                user = self.session.query(User).filter_by(id=id).first()

                if user:
                    self.retrieve_username_window(user)
                else:
                    messagebox.showerror("Error", "ID not found.")
            else:
                return False
            
        elif password_or_username ==  "USERNAME":
            id  = simpledialog.askinteger("Forgotten username", "Enter your ID:")
            if id:
                user = self.session.query(User).filter_by(id=id).first()

                if user:
                    self.retrieve_username_window(user)
                else:
                    messagebox.showerror("Error", "ID not found.")
        else:
            messagebox.showerror("Error", "Invalid input")
            return False

    
    def reset_password_window(self, user):
        self.root.iconify()

        reset_password_window = tk.Toplevel(self.root)
        reset_password_window.title("PASSWORD RESET")
        self.center_window(reset_password_window )

        tk.Label(reset_password_window, text=f"Resetting password for {user.username}").pack(pady=10)

        new_password_label = tk.Label(reset_password_window, text="Enter new password:")
        new_password_label.pack()

        new_password_entry = tk.Entry(reset_password_window)
        new_password_entry.pack()

        confirm_button = tk.Button(reset_password_window, text="Reset Password", command=lambda: self.confirm_reset_password(user, new_password_entry.get(), reset_password_window))
        confirm_button.pack(pady=10)
        

    def confirm_reset_password(self, user, new_password, window):
        hashed_new_password = hashlib.sha256(new_password.encode('utf-8')).hexdigest()

        user.password = hashed_new_password
        self.session.commit()

        messagebox.showinfo("Password Reset", "Password reset successful!")
        window.destroy()

        self.root.deiconify()
    
    
    def retrieve_username_window(self, user):
        self.root.iconify()
        retrieve_username_window = tk.Toplevel(self.root)
        retrieve_username_window.title("Username Retrieval")
        self.center_window(retrieve_username_window )

        tk.Label(retrieve_username_window, text="Your username is:").pack(pady=10)
        tk.Label(retrieve_username_window, text=user.username).pack()

        confirm_button = tk.Button(retrieve_username_window, text="Finish", command=retrieve_username_window.destroy)
        confirm_button.pack(pady=10)
        self.root.deiconify()


if __name__ == "__main__":
    root = tk.Tk()
    app = Login(root)
    root.mainloop()
