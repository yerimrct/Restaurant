import tkinter as tk
from tkinter import messagebox, simpledialog
from sqlalchemy import create_engine, Column, Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()

class MenuItem(Base):
    __tablename__ = 'menu'
    id = Column(Integer, primary_key=True)
    item = Column(String, nullable=False)
    price = Column(Float, nullable=False)
    category = Column(String, nullable=False)

    def __init__(self, item, price,category):
        self.item = item
        self.price = price
        self.category = category


class Menu:
    def __init__(self):
        self.engine = create_engine('sqlite:///orders.db', echo=True)
        Base.metadata.create_all(self.engine)
        self.Session = sessionmaker(bind=self.engine)
    
    def get_items(self):
        session = self.Session()
        items = session.query(MenuItem).all()
        session.close()
        return items
    
    def add_item(self, menu_item):
        session = self.Session()
        session.add(menu_item)
        session.commit()
        session.close()

    def remove_item(self, menu_item):
        session = self.Session()
        session.delete(menu_item)
        session.commit()
        session.close()

    def update_item_price(self, menu_item, new_price):
        session = self.Session()
        
        # Query for the item using its primary key
        existing_item = session.query(MenuItem).get(menu_item.id)

        if existing_item:
            existing_item.price = new_price
            session.commit()

        session.close()

class App:
    def __init__(self, root):
        self.root = root
        self.root.title("Menu")
        self.root.geometry("500x500")

        self.menu = Menu()

        self.label = tk.Label(root, text="Menu Items:")
        self.label.pack()

        self.listbox = tk.Listbox(root, selectmode=tk.SINGLE, exportselection=False)
        self.listbox.pack()
        self.listbox.bind('<<ListboxSelect>>', self.display_details)

        self.addButton = tk.Button(root, text="Add Item", command=self.input_item)
        self.addButton.pack()

        self.removeButton = tk.Button(root, text="Remove Item", command=self.remove_item)
        self.removeButton.pack()

        self.updateButton = tk.Button(root, text="Update Price", command=self.update_price)
        self.updateButton.pack()

        self.showAllButton = tk.Button(root, text="Show All Items", command=self.show_all_items)
        self.showAllButton.pack()

        self.showMenuButton = tk.Button(root, text="Show Menu", command=self.show_menu)
        self.showMenuButton.pack()

        self.txt_details = tk.Text(root, width=50, height=10)
        self.txt_details.pack()
        self.center_window(self.root)

        self.update_listbox()


    def center_window(self, window):
        # Update idle tasks to ensure that the window size is up-to-date
        window.update_idletasks()

        # Get the screen width and height
        screen_width = window.winfo_screenwidth()
        screen_height = window.winfo_screenheight()

        # Get the requested width and height of the window
        requested_width = window.winfo_reqwidth()
        requested_height = window.winfo_reqheight()

        # Calculate the centered position, taking the window size into account
        x_position = (screen_width - requested_width) // 2
        y_position = (screen_height - requested_height) // 2

        # Set the window position
        window.geometry(f"+{x_position}+{y_position}")

    def input_item(self):
        item = simpledialog.askstring("Input", "Enter item name:")
        if item:
            price = simpledialog.askfloat("Input", "Enter item price:")
            category = simpledialog.askstring("Input", "Enter item category:")

            new_item = MenuItem(item, price, category)
            self.menu.add_item(new_item)
            messagebox.showinfo("Successfull", "Added successfully")

            self.update_listbox()

    def remove_item(self):
        selected_index = self.listbox.curselection()
        if selected_index:
            selected_item = self.menu.get_items()[selected_index[0]]
            self.menu.remove_item(selected_item)
            messagebox.showinfo("Successfull", "Removed successfully")
            self.update_listbox()
        else:
            messagebox.showinfo("Error", "Please select an item to remove.")

    def update_price(self):
        selected_index = self.listbox.curselection()
        if selected_index:
            selected_item = self.menu.get_items()[selected_index[0]]
            new_price = simpledialog.askfloat("Update Price", f"Enter new price for {selected_item.item}:")
            if new_price is not None:
                # Call the update_item_price method with both the item and the new price
                self.menu.update_item_price(selected_item, new_price)
                self.update_listbox()
        else:
            messagebox.showinfo("Error", "Please select an item to update.")

    def show_all_items(self):
        self.txt_details.config(state=tk.NORMAL)
        all_items = self.menu.get_items()
        details_text = ""
        for item in all_items:
            details_text += f"Name: {item.item}\nPrice: ${item.price:.2f}\n"
        self.txt_details.delete(1.0, tk.END)
        self.txt_details.insert(tk.END, details_text)
        self.txt_details.config(state=tk.DISABLED)

    def show_menu(self):
        all_items = self.menu.get_items()

        # Group items by category
        items_by_category = {}
        for item in all_items:
            category = item.category  # Assuming there is a 'category' attribute in MenuItem class
            if category not in items_by_category:
                items_by_category[category] = []
            items_by_category[category].append(item)

        self.txt_details.config(state=tk.NORMAL)

        details_text = ""
        for category, items in items_by_category.items():
            details_text += f"{category}:\n"
            for item in items:
                details_text += f"  Name: {item.item} - Price: ${item.price:.2f}\n"

        self.txt_details.delete(1.0, tk.END)
        self.txt_details.insert(tk.END, details_text)
        self.txt_details.config(state=tk.DISABLED)

    def update_listbox(self):
        self.listbox.delete(0, tk.END)
        for item in self.menu.get_items():
            self.listbox.insert(tk.END, f"{item.item}")

    def display_details(self, event):
        selected_index = self.listbox.curselection()
        if selected_index:
            selected_item = self.menu.get_items()[selected_index[0]]
            details_text = f"Name: {selected_item.item}\nPrice: ${selected_item.price:.2f}\n"
            self.txt_details.delete(1.0, tk.END)
            self.txt_details.insert(tk.END, details_text)

if __name__ == "__main__":
    root = tk.Tk()
    app = App(root)
    root.mainloop()

