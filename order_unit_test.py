import unittest
from tkinter import Tk
from unittest.mock import Mock, patch
from orders import OrderManagementApp

class TestOrderManagementApp(unittest.TestCase):
    def setUp(self):
        self.root = Tk()
        self.app = OrderManagementApp(self.root)

    def tearDown(self):
        self.root.destroy()

    def test_add_to_order(self):
        # Mock the menu_listbox's curselection method
        with patch.object(self.app.menu_listbox, 'curselection', return_value=(0,)):
            with patch.object(self.app.menu_listbox ,'get',return_value="Burger - $5.99"):
            # Test adding an item to the orde
               
                self.app.add_to_order()
                self.assertEqual(len(self.app.order_items), 1)

    def test_remove_from_order(self):
        # Mock the order_listbox's curselection method
        with patch.object(self.app.order_listbox, 'curselection', return_value=(0,)):
            # Add an item to the order
            self.app.order_items.append(("Burger", 1, 5.99))
            # Test removing an item from the order
            self.app.remove_from_order()
            self.assertEqual(len(self.app.order_items), 0)

    def test_place_order_empty(self):
        # Mock the messagebox.showinfo method
        with patch( 'tkinter.messagebox.showinfo') as mock_showinfo:
            # Test placing an order with an empty order_items list
            self.app.place_order()
            mock_showinfo.assert_called_with("Order Empty", "Please add items to your order before placing.")
    
    # Add more test cases for place_order, view_order, etc.

if __name__ == "__main__":
    unittest.main()
