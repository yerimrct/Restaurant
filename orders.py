import tkinter as tk
from tkinter import messagebox, Toplevel
from sqlalchemy import create_engine, Column, Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import re

Base = declarative_base()

class MenuItem(Base):
    __tablename__ = 'menu'
    id = Column(Integer, primary_key=True)
    item = Column(String, nullable=False)
    price = Column(Float, nullable=False)
    category = Column(String, nullable=False )

class Order(Base):
    __tablename__ = 'orders'
    id = Column(Integer, primary_key=True)
    orderinfo = Column(String, nullable=False)
    cost = Column(Float, nullable=False)

class OrderManagementApp:
    # Menu items

    def __init__(self, master):
        self.master = master
        self.master.title("Order Management System")
        self.master.geometry("800x275")
        # self.master.resizable(False,False)
 

        # Connect to SQLite database
        engine = create_engine('sqlite:///orders.db', echo=True)
        Base.metadata.create_all(engine)
        Session = sessionmaker(bind=engine)
        self.session = Session()

        # Variables to store order information
        self.order_items = []
        
        self.session.commit()

        # GUI components
        self.menu_label = tk.Label(master, text="Menu:")
        self.menu_label.grid(row=0, column=0, padx=10, pady=10, sticky=tk.W)

        self.menu_textbox = tk.Text(master, width=40, height=10)
        self.menu_textbox.grid(row=1, column=0, padx=10, pady=10, sticky=tk.W)

        self.show_menu_by_category()

        # Assuming you have a SQLAlchemy model named MenuItem
        menu_items = self.session.query(MenuItem.item, MenuItem.price).all()

# Process the result as needed
        for item, price in menu_items:
            print(f"Item: {item}, Price: {price}")

        self.menu_listbox = tk.Listbox(master, selectmode=tk.MULTIPLE, height=4)
        self.menu_items = self.fetch_menu_items()

        # Populate the menu_listbox with items from the 'menu' table
        for item, price in self.menu_items.items():
            self.menu_listbox.insert(tk.END, f"{item} - ${self.menu_items[item]['price']:.2f} ")
        self.menu_listbox.grid(row=1, column=1, padx=10, pady=10, sticky=tk.W)

        self.add_to_order_button = tk.Button(master, text="ADD TO ORDER", command=self.add_to_order)
        self.add_to_order_button.grid(row=2, column=1, padx=35, sticky=tk.W)

        # self.order_label = tk.Label(master, text="Order Information:")
        # self.order_label.grid(row=0, column=1, padx=10, pady=10, sticky=tk.W)

        self.order_listbox = tk.Listbox(master, height=4)
        self.order_listbox.grid(row=1, column=2, padx=10, pady=10, sticky=tk.W)

        self.remove_from_order_button = tk.Button(master, text="REMOVE FROM ORDER", command=self.remove_from_order)
        self.remove_from_order_button.grid(row=2, column= 2, padx=17, sticky=tk.W)

        self.place_order_button = tk.Button(master, text="PLACE ORDER", command=self.place_order)
        self.place_order_button.grid(row=4, column=1, padx=38, sticky=tk.W)

        self.view_order_button = tk.Button(master, text="VIEW ORDERS", command=self.view_order)
        self.view_order_button.grid(row=4, column=2,padx=42, sticky=tk.W)

        self.total_cost_label = tk.Label(master, text="Total Cost: $0.00")
        self.total_cost_label.grid(row=0, column=2, padx=10, pady=10, sticky=tk.W)

        # Close the SQLite connection when the GUI is closed
        master.protocol("WM_DELETE_WINDOW", self.on_close)
        self.center_window(master)
    
    def show_menu_by_category(self):
        # Fetch menu items from the 'menu' table in the database
        menu_items = self.session.query(MenuItem).all()

        # Group menu items by category
        menu_by_category = {}
        for item in menu_items:
            if item.category not in menu_by_category:
                menu_by_category[item.category] = []
            menu_by_category[item.category].append(f"{item.item} - ${item.price:.2f}")

        self.menu_textbox.config(state=tk.NORMAL)

        # Display menu by category in the Text widget
        for category, items in menu_by_category.items():
            self.menu_textbox.insert(tk.END, f"{category}:\n")
            for item in items:
                self.menu_textbox.insert(tk.END, f"  {item}\n")
            self.menu_textbox.insert(tk.END, "\n")

        self.menu_textbox.config(state=tk.DISABLED)

    
    def center_window(self, window):
        window.update_idletasks()  # Ensure that window attributes are updated
        width = window.winfo_width()
        height = window.winfo_height()

        x_position = (window.winfo_screenwidth() - width) // 2
        y_position = (window.winfo_screenheight() - height) // 2

        window.geometry(f"+{x_position}+{y_position}")


    # def fetch_menu_items(self):
    #     # Fetch menu items from the 'menu' table in the database
    #     menu_items = self.session.query(MenuItem).all()
    #     return {item.item: item.price for item in menu_items}
    
    def fetch_menu_items(self):
    # Fetch menu items from the 'menu' table in the database
        menu_items = self.session.query(MenuItem).all()
        return {item.item: {"price": item.price, "category": item.category} for item in menu_items}

    
    
    def add_to_order(self):
        selected_indices = self.menu_listbox.curselection()

        for idx in selected_indices:
            item_entry = self.menu_listbox.get(idx)
            item_name, item_price_str = item_entry.split(" - ")
            item_price = float(item_price_str.replace("$", ""))

            # Extract item name and category
            menu_item = self.menu_items.get(item_name)
            item_category = menu_item.get("category", "Unknown")

        # Check if the item is already in order_items
            existing_item = next((order_item for order_item in self.order_items if order_item[0] == item_name), None)

            if existing_item:
            # If the item is already in order_items, create a new tuple with updated quantity
                updated_item = (existing_item[0], existing_item[1] + 1, existing_item[2])
                self.order_items.remove(existing_item)
                self.order_items.append(updated_item)
            else:
            # If the item is not in order_items, add a new tuple with quantity 1
                self.order_items.append((item_name, 1, item_price))

    # Update the order_listbox
        self.update_order_listbox()

    def remove_from_order(self):
        selected_indices = self.order_listbox.curselection()
        for idx in reversed(selected_indices):
            item = self.order_items[idx]
            if item[1] > 1:
                # If quantity > 1, decrement the quantity
                self.order_items[idx] = (item[0], item[1] - 1)
                self.order_listbox.delete(idx)
                self.order_listbox.insert(idx, f"{item[0]} x{item[1] - 1} - ${self.menu_items[item[0]] * (item[1] - 1):.2f}")
            else:
                # If quantity is 1, remove the item
                del self.order_items[idx]
                self.order_listbox.delete(idx)

    def place_order(self):  
        if not self.order_items:
            messagebox.showinfo("Order Empty", "Please add items to your order before placing.")
        else:
            # Initialize variables for ordered items and total cost
            ordered_items = []
            total_cost = 0

            # Extract item names and calculate total cost
            for item_info in self.order_items:
                item_name, quantity, item_price = item_info
                total_cost += quantity * item_price
                ordered_items.append(f"{item_name} - x{quantity}")

            # Create a comma-separated string of ordered items
            order_details = ", ".join(ordered_items)

            # Create a new window for payment information
            payment_window = Toplevel(self.master)
            payment_window.title("Payment Information")
            payment_window.geometry("400x300")
            payment_window.resizable(False, False)
            self.center_window(payment_window)

            # Validation functions
            validate_card_number = (payment_window.register(self.validate_card_number), "%P")
            
            validate_cvv = (payment_window.register(self.validate_cvv), "%P")
            
            validate_name = (payment_window.register(self.validate_name), "%P")

            # Create labels and entry widgets for payment information
            name_label = tk.Label(payment_window, text="Name on Card:")
            name_label.grid(row=0, column=0, padx=10, pady=10, sticky=tk.W)

            name_entry = tk.Entry(payment_window, validate="key", validatecommand=validate_name)
            name_entry.grid(row=0, column=1, padx=10, pady=10, sticky=tk.W)

            card_number_label = tk.Label(payment_window, text="Card Number:")
            card_number_label.grid(row=1, column=0, padx=10, pady=10, sticky=tk.W)

            card_number_entry = tk.Entry(payment_window, validate="key", validatecommand=validate_card_number)
            card_number_entry.grid(row=1, column=1, padx=10, pady=10, sticky=tk.W)

            expiry_label = tk.Label(payment_window, text="Expiry Date (MM/YYYY):")
            expiry_label.grid(row=2, column=0, padx=10, pady=10, sticky=tk.W)

            expiry_date_entry = tk.Entry(payment_window, validate="key", validatecommand=(payment_window.register(self.validate_expiry_date), "%P"))
            expiry_date_entry.grid(row=2, column=1, padx=10, pady=10, sticky=tk.W)

            cvv_label = tk.Label(payment_window, text="CVV:")
            cvv_label.grid(row=3, column=0, padx=10, pady=10, sticky=tk.W)

            cvv_entry = tk.Entry(payment_window, validate="key", validatecommand=validate_cvv)
            cvv_entry.grid(row=3, column=1, padx=10, pady=10, sticky=tk.W)

            pin_label = tk.Label(payment_window, text="Card PIN:")
            pin_label.grid(row=4, column=0, padx=10, pady=10, sticky=tk.W)

            pin_entry = tk.Entry(payment_window, validate="key", show = "*", validatecommand=(payment_window.register(self.validate_pin), "%P"))
            pin_entry.grid(row=4, column=1, padx=10, pady=10, sticky=tk.W)

            pay_button = tk.Button(payment_window, text="PAY", command=lambda: self.process_payment(order_details, total_cost, payment_window))
            pay_button.grid(row=5, column=0, columnspan=2, pady=10, sticky=tk.W)

            back_button = tk.Button(payment_window, text="Back", command=payment_window.destroy)
            back_button.grid(row=6, column=0, columnspan=2, pady=10, sticky=tk.W)


    def validate_card_number(self, input_text):
        return input_text.isdigit() and len(input_text) <= 16 or len(input_text) == 0
    
    def validate_expiry_date(self, input_text):
    # Allow only digits, "/", and a maximum of 7 characters
        if all(char.isdigit() or char == '/' for char in input_text) and len(input_text) <= 7:
        # Check for the correct format "mm/yyyy" only when the length is 7
            if len(input_text) == 7:
                parts = input_text.split('/')
                if len(parts) == 2 and len(parts[0]) <= 2 and len(parts[1]) <= 4:
                # Additional checks for month and year can be added if needed
                    return True
                else:
                    messagebox.showerror("Invalid Format", "Please enter the date in the format 'mm/yyyy'.")
                    return False
            else:
                return True
        else:
            messagebox.showerror("Invalid Characters", "Please enter only digits and '/' for the date.")
            return False

    def validate_pin(self, input_text):
    # Validate the PIN to allow only digits with a maximum length of 4
        return input_text.isdigit() and len(input_text) <= 4 or len(input_text) == 0

    def validate_cvv(self, input_text):
        return input_text.isdigit() and len(input_text) <= 3  or len(input_text) == 0

    def validate_name(self, input_text):
        return all(char.isalpha() or char.isspace() for char in input_text) or len(input_text) == 0

    def process_payment(self, order_details, total_cost, payment_window):
        # Perform payment processing here (mocked for illustration)
        # You would typically integrate with a payment gateway for real transactions
        success = messagebox.askyesno("Payment Success", "Do you want to proceed with the order?")
        
        if success:
            # Save the order to the database using SQLAlchemy
            order = Order(orderinfo=order_details, cost=total_cost)
            self.session.add(order)
            self.session.commit()

            # Fetch the recently added order
            order = self.session.query(Order).order_by(Order.id.desc()).first()

            # Display order information in the message box
            if order:
                message = f"Order placed successfully!\n\nOrder id: {order.id}\nOrdered Items: {order.orderinfo}\n\nTotal Cost: ${order.cost:.2f}"
                messagebox.showinfo("Order Placed", message)

            # Clear order information
            self.order_items.clear()
            self.update_order_listbox()  # Update the order_listbox
            self.total_cost_label.config(text="Total Cost: $0.00")

            # Close the payment window
            payment_window.destroy()
        else:
            messagebox.showinfo("Payment Cancelled", "Payment cancelled. Your order has not been placed.")

    def view_order(self):
    # Fetch data from the 'orders' table using SQLAlchemy
        orders_data = self.session.query(Order.id, Order.orderinfo, Order.cost).all()

        if not orders_data:
            messagebox.showinfo("No Orders", "No orders found.")
        else:
        # Create a new window for viewing orders
            view_order_window = Toplevel(self.master)
            view_order_window.title("Orders")
            view_order_window.geometry("675x350")
            view_order_window.resizable(False,False)
            self.center_window(view_order_window)


        # Create a Label and Entry for searching by ID
            search_label = tk.Label(view_order_window, text="Search by Order ID:")
            search_label.grid(row=0, column=0, padx=5, pady=10, sticky=tk.W)

            search_entry = tk.Entry(view_order_window)
            search_entry.grid(row=0, column=1, padx=5, pady=10, sticky=tk.W)

        # Create a Listbox to display the orders
            order_listbox = tk.Listbox(view_order_window, height=10, width=60)
            order_listbox.grid(row=1, column=0, columnspan=2, padx=10, pady=10, sticky=tk.W)

        # Insert data into the Listbox
            for order in orders_data:
                order_listbox.insert(tk.END, f"Order ID: {order.id}, Items: {order.orderinfo}, Total Cost: ${order.cost:.2f}")

        # Add a search button
            search_button = tk.Button(view_order_window, text="Search", command=lambda: self.search_order(orders_data, search_entry.get(), order_listbox))
            search_button.grid(row=0, column=2, padx=5, pady=10, sticky=tk.W)

            update_order_button = tk.Button(view_order_window, text="UPDATE ORDER", command=lambda: self.update_order(orders_data, order_listbox))
            update_order_button.grid(row=2, column=0, columnspan=2, padx=10,  sticky=tk.W)

            delete_button = tk.Button(view_order_window, text="CANCEL ORDER", command=lambda: self.delete_order(orders_data, order_listbox))
            delete_button.grid(row=3, column=0, columnspan=2, padx=10, sticky=tk.W)

        # Add a close button
            close_button = tk.Button(view_order_window, text="Back", command=view_order_window.destroy)
            close_button.grid(row=4, column=0, columnspan=2, padx=10, sticky=tk.W)


    def search_order(self, orders_data, search_id, order_listbox):
        
        order_listbox.delete(0, tk.END)

    # Search for orders by ID
        matching_orders = []
        for order in orders_data:
            if str(order[0]) == search_id:
                matching_orders.append(order)

    # Display the search result in the Listbox  
        for order in matching_orders:
            order_listbox.insert(tk.END, f"Order ID: {order[0]}, Items: {order[1]}, Total Cost: ${order[2]:.2f}")

    # If no matching orders are found, show a message
        if not matching_orders:
            messagebox.showinfo("No Match", "No order found.")

    def extract_name_and_price(self, item):
    # Extract item name and quantity from the menu item string
        parts = item.split(" x")
        item_name = parts[0]
        item_quantity = int(parts[1]) if len(parts) > 1 else 1
        item_price = self.menu_items.get(item_name, 0)
        return item_name, item_quantity, item_price

    def update_order_listbox(self):
    # Clear and update the order_listbox with the updated order_items
        self.order_listbox.delete(0, tk.END)
        total_cost = 0

        for order_item in self.order_items:
            item_name, quantity, item_price = order_item
            total_price = quantity * item_price
            total_cost += total_price
            self.order_listbox.insert(tk.END, f"{item_name} (x{quantity}) - ${total_price:.2f}")

    # Update the total_cost_label
        self.total_cost_label.config(text=f"Total Cost: ${total_cost:.2f}")
    
    def update_order(self, orders_data, order_listbox):
    # Get the selected order ID
        selected_order = order_listbox.curselection()

        if not selected_order:
        # Display a message if no order is selected
            messagebox.showinfo("No Selection", "Please choose an order to update.")
            return

        order_id = orders_data[selected_order[0]][0]

    # Retrieve the current order details from the database
        order_data = self.session.query(Order.orderinfo, Order.cost).filter(Order.id == order_id).first()

# Process the result as needed
        if order_data:
            order_info, total_cost = order_data
            print(f"Order Info: {order_info}, Total Cost: {total_cost}")

        else:
            messagebox.showerror("Error", "Order not found.")
            return

        order_info, total_cost = order_data

    # Create a new window for updating the order
        update_order_window = Toplevel(self.master)
        update_order_window.title("Update Order")
        update_order_window.geometry("910x200")
        update_order_window.resizable(False,False)
        self.center_window(update_order_window)

    # Display the current order info for reference
        current_order_label = tk.Label(update_order_window, text=f"Current Order: {order_info} Cost: {total_cost}")
        current_order_label.grid(row=0, column=0, columnspan=2, padx=10, pady=10, sticky=tk.W)

        quantities_label = tk.Label(update_order_window, text="NOTE\n"
                                    "\nEnter the new quantities for each item in the order they were made, separated by commas." 
                                    "\nIf you do not want to change the quantity of an item, type the previous quantity."
                                    "\nIf you want to remove an item from the order, enter '0' for that item.")
        quantities_label.grid(row=1, column=0, padx=0, pady=10, sticky=tk.W)

        # quantities_entry = tk.Entry(update_order_window, validate="key", validatecommand=(update_order_window.register(self.validate_input), "%P"))
        quantities_entry = tk.Entry(
            update_order_window,
            validate="key",
            validatecommand=(update_order_window.register(self.validate_input), "%P")
        )
        quantities_entry.grid(row=1, column=1, padx=10, sticky=tk.W)

    # Add a button to confirm the update
        confirm_button = tk.Button(update_order_window, text="Confirm Update", command=lambda: self.update_and_close(order_id, quantities_entry.get(), update_order_window))
        confirm_button.grid(row=1, column=2, columnspan=2, padx=10,  sticky=tk.W)
        
    
    def validate_input(self, input_text):
    # Validate the input to allow only numbers and at most one comma after each number
        if all(char.isdigit() or char == ',' for char in input_text):
        # Check for consecutive commas
            if ',' in input_text and ',,' in input_text:
            # Display an error message for multiple consecutive commas
                messagebox.showerror("Validation Error", "Do not place multiple commas after each other.")
                return False
            return True
        else:
        # Display a general error message for invalid input
            messagebox.showerror("Validation Error", "Invalid input. Please use numbers and commas only.")
            return False

    def update_and_close(self, order_id, new_quantities_str, update_order_window):
        try:
            new_quantities = [int(qty) for qty in new_quantities_str.split(',')]
        except ValueError as e:
            messagebox.showerror("Invalid Quantities", str(e))
            return

    # Retrieve the current order details from the database using SQLAlchemy
        order = self.session.query(Order).filter_by(id=order_id).first()

        if not order:
            messagebox.showerror("Error", "Order not found.")
            return

        order_info, total_cost = order.orderinfo, order.cost

    # Update the quantities in the order_info string
        items = order_info.split(', ')
        updated_items = []

        for item, new_quantity in zip(items, new_quantities):
            name, qty = item.rsplit(' x', 1)
            new_quantity = int(new_quantity)  # Ensure new_quantity is an integer

            if name in self.menu_items:
                updated_items.append(f"{name} x{new_quantity}")
            else:
                updated_items.append(item)

        updated_order_info = ', '.join(updated_items)

        new_order_info = ', '.join([f"{re.sub(' x[0-9]+', '', name)} x{new_quantity}" for name, new_quantity in zip(items, new_quantities)])

    # Calculate the new total cost
        menu_items_info = [item.strip() for item in new_order_info.split(', ')]
        new_prices = [int(new_quantity) * self.menu_items.get(name.split(' -')[0].strip(), 0) for name, new_quantity in zip(items, new_quantities)]
        new_total_cost = sum(new_prices)

    # Update the order in the database using SQLAlchemy
        order.orderinfo = new_order_info
        order.cost = new_total_cost
        self.session.commit()

    # Display the updated order details
        self.view_order()

    # Close the update_order_window
        update_order_window.destroy()


    def delete_order(self, orders_data, order_listbox):
    # Get the selected order ID
        selected_order = order_listbox.curselection()

        if not selected_order:
        # Display a message if no order is selected
            messagebox.showinfo("No Selection", "Please choose an order to cancel.")
            return

        order_id = orders_data[selected_order[0]][0]

    # Delete the order from the database using SQLAlchemy
        order = self.session.query(Order).filter_by(id=order_id).first()

        if not order:
            messagebox.showerror("Error", "Order not found.")
            return

        self.session.delete(order)
        self.session.commit()
        messagebox.showinfo("Success", "Order successfully cancelled")

    # Remove the selected order from the listbox
        order_listbox.delete(selected_order)


    def on_close(self):
        # Close the SQLite connection when the GUI is closed
        self.session.close()
        self.master.destroy()

if __name__ == "__main__":
    root = tk.Tk()
    app = OrderManagementApp(root)
    root.mainloop() 


