import tkinter as tk
from tkinter import messagebox, simpledialog, ttk
from tkcalendar import DateEntry
import sqlite3
import datetime as dt
from datetime import datetime, date
from dateutil.parser import parse
import sqlalchemy
from sqlalchemy.orm import Session, sessionmaker, relationship, aliased
from sqlalchemy import distinct, update, exists, create_engine, inspect, insert, Column, Integer, String, Date, ForeignKey, CheckConstraint
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
engine = create_engine('sqlite:///reservation.db', echo=True)
Base.metadata.create_all(engine)

Session = sessionmaker(bind=engine)
session = Session()
 
class Table(Base):
    __tablename__ = 'tables'

    id = Column(Integer, primary_key=True)
    branch = Column(String, nullable=False)
    table_amount = Column(Integer, nullable=False)
    reset_date = Column(Date, default=dt.date.today())
    branch_number = Column(Integer, nullable=False)

    __table_args__ = (
        CheckConstraint('table_amount >= 0', name='check_min_table_amount'),
        CheckConstraint('table_amount <= 100', name='check_max_table_amount'),
    )

class Reservation(Base):
    __tablename__ = 'reservations'

    id = Column(Integer, primary_key=True)
    date = Column(Date, nullable=False)
    branch = Column(String, nullable=False)
    table_amount = Column(Integer, nullable=False)
    table_id = Column(Integer, ForeignKey('tables.id'))
    table = relationship('Table', back_populates='reservations')
    branch_number = Column(String, nullable=False)

Table.reservations = relationship('Reservation', order_by=Reservation.id, back_populates='table')

# Replace 'sqlite:///reservation.db' with your actual database connection string

class Reservations:
    def __init__(self, master, engine):
        self.master = master
        self.master.title("RESERVATIONS")
        self.master.minsize(width=500, height=250)
        self.master.resizable(False, False)
        
        Base.metadata.create_all(engine)
        self.session = Session()
        
        self.conn = sqlite3.connect('reservation.db')
        self.cur = self.conn.cursor()
        
        # GUI elements
        self.label_date = tk.Label(master, text="Select reservation date:")
        self.entry_date = DateEntry(master, width=12, background='darkblue', borderwidth=2, mindate=dt.date.today())
        self.label_branch = tk.Label(master, text="Choose branch(scroll for more branches):")

        self.selected_branch = tk.StringVar()
        self.branches_listbox = ttk.Combobox(master, textvariable=self.selected_branch , state= "readonly")
        self.populate_branches_listbox()
      
        self.selected_restaurant = tk.StringVar()

        restaurant_label = tk.Label(self.master, text="Select Restaurant:")

        self.restaurant_dropdown = ttk.Combobox(self.master, textvariable=self.selected_restaurant, state= "readonly")

        self.restaurant_dropdown.bind("<<ComboboxSelected>>", self.enable_restaurant_dropdown)
        self.branches_listbox.bind("<<ComboboxSelected>>", self.enable_restaurant_dropdown)

        entry_button = tk.Frame(self.master)

        self.table_amount_label = tk.Label(master, text="Input number of tables:")
        self.table_amount_entry = tk.Entry(entry_button)

        # Validate the table_amount for numeric values only
        vcmd = master.register(self.validate_table_amount)
        self.table_amount_entry.config(validate="key", validatecommand=(vcmd, "%P"))

        self.button_reserve = tk.Button(entry_button, text="RESERVE ", command=self.reserve_table)


        self.label_options= tk.Label(master, text="Other options:")

        # Layout
        self.label_date.pack()
        self.entry_date.pack()
        self.label_branch.pack()
        self.branches_listbox.pack()
        restaurant_label.pack()
        self.restaurant_dropdown.pack()
        self.table_amount_label.pack()
        entry_button.pack()
        self.table_amount_entry.pack(side = tk.LEFT)
        self.button_reserve.pack(side = tk.RIGHT)

        self.label_options.pack()

        self.button_show_reservations = tk.Button(master, text="VIEW RESERVATIONS", command=self.show_reservations)
        self.button_show_reservations.pack()

        self.button_show_tables_left = tk.Button(master, text="CHECK TABLE AVAILABILITY FOR SPECIFIC DATE", command=self.show_tables_left_on_date)
        self.button_show_tables_left.pack()

        self.center_window(self.master)
        
        # Call create_tables to ensure tables are created
        # self.create_tables()
    
    # def create_tables(self):
    #     # Create a reservation table if it doesn't exist
    #     try:
    #         Base.metadata.create_all(engine)
    #         Table.__table__.create(bind=self.engine, checkfirst=True)
    #         Reservation.__table__.create(bind=self.engine, checkfirst=True)
    #         with self.session.begin():
    #             self.session.query(Table).filter(Table.id == 8).one_or_none() or \
    #                 self.session.execute("INSERT INTO tables (id, branch, table_amount, reset_date) VALUES (8, 'Birmingham', 60, DATE('now'))")
    #     except Exception as e:
    #         messagebox.showerror("Error", f"Error creating tables: {e}")
        
    def enable_restaurant_dropdown(self, event):
        selected_index = self.branches_listbox.get()
        
        if selected_index:       
            restaurants = self.get_all_restaurants()
            self.restaurant_dropdown['values'] = restaurants
            self.restaurant_dropdown['state'] = 'readonly'
    
    def get_all_restaurants(self):
        try:
            restaurants = self.session.query(distinct(Table.branch_number)).all()
            restaurant_names = [restaurant[0] for restaurant in restaurants]

            return restaurant_names
        except sqlalchemy.exc.SQLAlchemyError as e:
            messagebox.showerror("Error", f"SQLAlchemy error: {e}")
            return []
        finally:
            self.session.close()

    def center_window(self, window, reference_window=None):
    # Get the screen width and height
        screen_width = window.winfo_screenwidth()
        screen_height = window.winfo_screenheight()

        if reference_window:
        # Center the window relative to the reference window
            x_position = reference_window.winfo_x() + (reference_window.winfo_width() - window.winfo_reqwidth()) // 2
            y_position = reference_window.winfo_y() + (reference_window.winfo_height() - window.winfo_reqheight()) // 2
        else:
        # Center the window on the screen, considering minsize
            min_width, min_height = window.minsize()
            x_position = (screen_width - max(window.winfo_reqwidth(), min_width)) // 2
            y_position = (screen_height - max(window.winfo_reqheight(), min_height)) // 2

    # Set the window position
        window.geometry(f"+{x_position}+{y_position}")


    def validate_table_amount(self, new_value):
        # Validate that the input for table_amount contains only numeric values
        if new_value.isdigit():
            return int(new_value) >= 1
        return new_value == ""
    
    def reset_fields(self):
        # Reset the input fields after successfully reserving a table
        self.entry_date.delete(0, tk.END)
        self.branches_listbox.set('')
        self.table_amount_entry.delete(0, tk.END)

    # def populate_branches_listbox(self):
    #     # Fetch branches from the 'tables' table and populate the listbox

    #     branches = self.cur.execute("SELECT DISTINCT branch FROM tables").fetchall()
    #     today = datetime.date.today()

    #     for branch in branches:
    #         self.cur.execute("INSERT INTO tables (branch, table_amount, reset_date) VALUES (?, 100, ?)",
    #                      (branch[0], today))

    #         self.branches_listbox.insert(tk.END, branch[0])

    #     self.branches_listbox.config(height=min(5, len(branches)))
    
    def populate_branches_listbox(self):
    # Fetch branches from the 'tables' table and populate the listbox
        branches = self.get_branches_from_tables()
        today = date.today()

        if not branches:
            messagebox.showerror("Error", "No branches found in the 'tables' table.")
            return

        self.branches_listbox['values'] = branches


    def check_capacity(self, table_amount, selected_branch, reservation_date, selected_restaurant):
        try:
        # Create a new session
            session = Session()
            table_amount = int(table_amount)

        # Query the 'tables' table to check the remaining capacity
            table_entry = session.query(Table).filter(
                Table.branch == selected_branch,
                Table.reset_date == reservation_date,
                Table.branch_number == selected_restaurant
            ).one_or_none()

            if table_entry:
                remaining_capacity = table_entry.table_amount
                if remaining_capacity >= table_amount:
                    return remaining_capacity
                elif remaining_capacity <= 0:
                    messagebox.showerror("Error", "No tables available")
                else:
                    messagebox.showerror("Error", f"Only {remaining_capacity} tables available")
            else:
            # Insert a new row if it doesn't exist
                new_table_entry = Table(branch=selected_branch, table_amount=100, reset_date=reservation_date, branch_number=selected_restaurant)
                session.add(new_table_entry)
                session.commit()

            # Query again to get the newly inserted entry
                updated_entry = session.query(Table).filter(
                    Table.branch == selected_branch,
                    Table.reset_date == reservation_date,
                    Table.branch_number == selected_restaurant
                ).one_or_none()
                remaining_capacity = updated_entry.table_amount

                if remaining_capacity >= table_amount:
                    return remaining_capacity
                else:
                    messagebox.showerror("Error", f"Only {remaining_capacity} tables available")

        except Exception as e:
            messagebox.showerror("Error", f"Error checking capacity: {e}")

        finally:
            if session:
                session.close()  # Close the session

        return None
    # Close the session


    def reserve_table(self):
        number_of_tables = 100

        date = self.entry_date.get_date()
        selected_branch = self.branches_listbox.get()
        table_amount = self.table_amount_entry.get()

        if not date or not selected_branch or not table_amount:
            messagebox.showerror("Error", "Please enter all fields")
            return

        # Get the selected branch from the listbox
    
        # Retrieve the selected restaurant
        selected_restaurant = self.selected_restaurant.get()

        remaining_capacity = self.check_capacity(table_amount, selected_branch, date,  selected_restaurant )
        if remaining_capacity is not None:
            # Reserve table
            table_entry = self.session.query(Table).filter(Table.branch == selected_branch, Table.reset_date == date, Table.branch_number == selected_restaurant).one_or_none()

            try:
                # Update the table amount in the 'tables' table
                if table_entry:
                    table_entry.table_amount -= int(table_amount)
                else:
                    # Insert a new row if it doesn't exist
                    new_table_entry = Table(branch=selected_branch, table_amount=number_of_tables, reset_date=date, branch_number=selected_restaurant)
                    self.session.add(new_table_entry)

                    # Update the 'table_amount' in the 'tables' table for the current branch, date, and restaurant
                    self.session.query(Table).filter(Table.branch == selected_branch, Table.reset_date == date, Table.branch_number == selected_restaurant).update({Table.table_amount: Table.table_amount - int(table_amount)})

                # Insert the reservation into the 'reservations' table
                new_reservation = Reservation(date=date, branch=selected_branch, table_amount=int(table_amount), branch_number=selected_restaurant)

                if table_entry:
                    new_reservation.table_id = table_entry.id

                self.session.add(new_reservation)

                messagebox.showinfo("Success", "Table reserved successfully")
                self.reset_fields()
                self.session.commit()
            except Exception as e:
                messagebox.showerror("Error", f"Error reserving table: {e}")
            finally:
                if self.session:
                    self.session.close()
        else:
            print("Capacity check failed")

    def get_table_id(self, branch, date, branch_number):
        try:
            with Session() as session:
                result = session.query(Table.id).filter_by(branch=branch, reset_date=date, branch_number=branch_number).scalar()
                if result:
                    return result
                else:
                    return None
        except sqlalchemy.exc.SQLAlchemyError as e:
            messagebox.showerror("Error", f"SQLAlchemy error: {e}")
            return None


    def show_reservations(self):
    # Create a new window for displaying reservations
        self.reservations_window = tk.Toplevel(self.master)
        self.reservations_window.minsize(width=500, height=350)
        self.reservations_window.title("RESERVATION DETAILS")
        self.center_window(self.reservations_window)

    # Create a search bar
        search_frame = ttk.Frame(self.reservations_window)
        search_frame.pack(side=tk.TOP, expand=True, fill=tk.X, pady=10)

        search_label = tk.Label(search_frame, text="Enter Reservation ID:")
        search_label.pack(side=tk.LEFT, padx=10)

        search_entry = tk.Entry(search_frame)
        search_entry.pack(side=tk.LEFT, padx=10)

        search_button = tk.Button(search_frame, text="Search", command=lambda: self.search_reservation(search_entry.get()))
        search_button.pack(side=tk.LEFT, padx=10)

    # Create a Listbox and Scrollbar in the top frame
        frame1 = ttk.Frame(self.reservations_window)
        frame1.pack(side=tk.TOP, expand=True, fill=tk.BOTH)

        reservations_listbox = tk.Listbox(frame1, selectmode=tk.SINGLE)
        scrollbar = tk.Scrollbar(frame1, command=reservations_listbox.yview)
        reservations_listbox.config(yscrollcommand=scrollbar.set)

    # Populate Listbox with reservations data
        self.populate_reservations_listbox(reservations_listbox)

    # Pack Listbox and Scrollbar
        reservations_listbox.pack(expand=True, fill=tk.BOTH)
        scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

    # Create a frame for the delete button in the bottom frame
        frame2 = ttk.Frame(self.reservations_window)
        frame2.pack(side=tk.BOTTOM)

    # Add an "Update Reservation" button
        update_button = tk.Button(frame2, text="UPDATE RESERVATION", command=self.update_reservation)
        update_button.pack()

    # Add a "Delete Reservation" button
        delete_button = tk.Button(frame2, text="CANCEL RESERVATION", command=lambda: self.delete_reservation(reservations_listbox))
        delete_button.pack()

    # Add a "Go Back" button
        go_back_button = tk.Button(frame2, text="Go Back", command=self.show_first_window)
        go_back_button.pack()

    def search_reservation(self, reservation_id):
        # Search for the reservation with the given ID and display details
        if reservation_id.isdigit():
            reservation_id = int(reservation_id)

            result = self.session.query(Reservation.id, Reservation.date, Reservation.branch, Reservation.table_amount, Reservation.branch_number) \
                .filter(Reservation.id == reservation_id).one_or_none()
            self.session.close()

            if result:
                reservation_details = f"Reservation ID: {result.id}  Date: {result.date}  Branch: {result.branch}  Tables: {result.table_amount} Restaurant Number: {result.branch_number}"
                messagebox.showinfo("Reservation Details", reservation_details)
            else:
                messagebox.showinfo("Reservation Details", f"No reservation found with ID {reservation_id}")
        else:
            messagebox.showerror("Invalid Input", "Please enter a valid Reservation ID.")

            

    def populate_reservations_listbox(self, listbox):
        try:
        # Open a new session
            with Session() as session:
            # Fetch reservations from the 'reservations' table
                reservations = session.query(Reservation.id, Reservation.date, Reservation.branch, Reservation.table_amount, Reservation.branch_number).all()

            # Populate the listbox with reservation information
                for reservation in reservations:
                    reservation_details = f"Reservation ID: {reservation.id}  Date: {reservation.date}  Branch: {reservation.branch}  Tables: {reservation.table_amount} Branch Number: {reservation.branch_number}"
                    listbox.insert(tk.END, reservation_details)

        except sqlalchemy.exc.SQLAlchemyError as e:
            messagebox.showerror("Error", f"SQLAlchemy error: {e}")
        finally:
            session.close()

    # def populate_reservations_listbox(self, listbox):
    #     # Fetch reservations from the 'reservations' table and populate the listbox
    #     with self.session.begin():
    #         reservations = self.session.query(Reservation.id, Reservation.date, Reservation.branch, Reservation.table_amount).all()
        
    #         for reservation in reservations:
    #             reservation_details = f"Reservation ID: {reservation.id}  Date: {reservation.date}  Branch: {reservation.branch}  Tables: {reservation.table_amount}"
    #             listbox.insert(tk.END, reservation_details)

    def update_reservation(self):
        
    # Get the reservation ID from the user
        reservation_id = simpledialog.askinteger("Input", "Enter Reservation ID to update:")
        date = self.entry_date.get_date()

        if reservation_id is None:
            return  # User clicked Cancel

        if not self.check_reservation_id_exists(reservation_id):
            messagebox.showerror("Error", f"Reservation does not exist.")
            return
        
        result = (
                    self.session.query(Reservation.branch, Reservation.table_id, Reservation.table_amount, Reservation.date, Reservation.branch_number)
                        .filter(Reservation.id == reservation_id)
                        .first()
                )

        if not result:
            messagebox.showerror("Error", f"Reservation with ID {reservation_id} not found.")

        existing_branch, existing_table_id, existing_table_amount, existing_date, existing_branch_number = result

    # Get the column to update and the new value
        while True:
            column_to_update = simpledialog.askstring("Input", "Enter what to update ('date' 'branch' 'table_amount' or 'branch_number') in that format:")
            if not column_to_update:
                return  # User clicked Cancel

            try:
                inspector = inspect(engine)
                columns = [column["name"] for column in inspector.get_columns("reservations")]

                if column_to_update not in columns:
                    messagebox.showerror("Error", f"The column '{column_to_update}' does not exist. Column inputted has to match any of the four given")
                    continue
                else:
                    break
            except sqlalchemy.exc.SQLAlchemyError as e:
                messagebox.showerror("Error", f"SQLAlchemy error: {e}")
                return

    # If the column to update is 'branch', provide a dropdown menu with options from the 'tables' table
        if column_to_update == 'branch':
    # Fetch branches from the 'tables' table and create a list for the dropdown menu
            branches = self.get_branches_from_tables()
            if not branches:
                messagebox.showerror("Error", "No branches found in the 'tables' table.")
                return

    # Create a top-level window for the dropdown menu
            dropdown_window = tk.Toplevel(self.master)
            dropdown_window.title("UPDATE BRANCH")
            name = tk.Label(dropdown_window, text="Choose new branch:")
            name.pack()

    # Create a StringVar to hold the selected branch
            selected_branch = tk.StringVar()

    # Create a dropdown menu with the branches
            branch_menu = ttk.Combobox(dropdown_window, textvariable=selected_branch, values=branches)
            branch_menu.pack(padx=10, pady=10)

    # Add an "OK" button to confirm the selection
            ok_button = tk.Button(dropdown_window, text="OK", command=lambda: dropdown_window.destroy())
            ok_button.pack(pady=5)


            with Session() as session:
                ReservationAlias = aliased(Reservation)
                result = (
                    session.query(Reservation.table_amount, Reservation.table_id, Reservation.branch_number)
                    .filter(Reservation.id == reservation_id)
                    .first()
                )
            if result:
                table_from_result, old_table_id, branch_number= result
                print(table_from_result)
                print (old_table_id)

            # Wait for the dropdown window to be closed
            dropdown_window.wait_window()

            # Retrieve the selected branch
            new_value = selected_branch.get()

            # Get the new table_id for the selected branch
            new_table_id = self.get_table_id(new_value, date, branch_number)
            print(new_table_id )

            with Session() as session:
                reservation = session.query(Reservation).filter_by(id=reservation_id).first()
                reservation.branch = new_value

                reservation.table_id = new_table_id

                old_table = session.query(Table).filter_by(id=old_table_id).first()
                old_table.table_amount += table_from_result

                # Check if the entry for the new branch and date already exists in the 'tables' table
                new_table_entry = session.query(Table).filter_by(branch=new_value, reset_date=date).first()

                if new_table_entry:
                    # If the record exists, update the 'table_amount'
                    new_table_entry.table_amount -= table_from_result
                else:
                    # If the record does not exist, insert a new row
                    new_table_entry = Table(
                        branch=new_value,
                        reset_date=date,
                        table_amount=(100 - int(table_from_result)), 
                        branch_number = branch_number
                    )
                    session.add(new_table_entry)

                session.commit()
                messagebox.showinfo("Success", "Branch successfully updated")
                    

        elif column_to_update == 'date':
            # If the column to update is 'date', use DateEntry for input
            new_value = self.ask_for_date_entry()
            print(new_value)
            if new_value is None:
                return  # User clicked Cancel
            try:
                if not new_value and column_to_update != 'date':
                    raise ValueError("Please enter a new value.")

                # Convert the new_value to a Python date object
                new_date = datetime.strptime(new_value, '%Y-%m-%d').date()

                with Session() as session:
                    session.execute(
                        update(Reservation)
                        .where(Reservation.id == reservation_id)
                        .values({Reservation.date: new_date})
                    )
                    
                    existing_table_entry = session.query(Table).filter_by(branch=existing_branch, reset_date=new_value).first()

                    if existing_table_entry:
                # If the record exists, update the 'table_amount'
                        session.execute(
                        update(Table)
                        .where((Table.branch == existing_branch) & (Table.reset_date == existing_date))
                        .values({Table.table_amount: Table.table_amount +  existing_table_amount})
                        )

                        session.execute(
                        update(Table)
                        .where((Table.branch == existing_branch) & (Table.reset_date == new_date))
                        .values({Table.table_amount: Table.table_amount - existing_table_amount})
                        )

                    
                    else:
                # If the record does not exist, insert a new row
                        session.execute(
                        update(Table)
                        .where((Table.branch == existing_branch) & (Table.reset_date == existing_date))
                        .values({Table.table_amount: Table.table_amount +  existing_table_amount})
                        )

                        new_table_entry = Table(
                                                branch=existing_branch, 
                                                reset_date = datetime.strptime(new_value, '%Y-%m-%d').date(), 
                                                table_amount=(100 - int(existing_table_amount))
                                                )
                        
                        session.execute(insert(Table).values(
                            branch=new_table_entry.branch,
                            table_amount=new_table_entry.table_amount,
                            reset_date=new_table_entry.reset_date)
                            )
                                            
                    session.commit()             

                print(f"Updated reservation in the database: {reservation_id}")

                messagebox.showinfo("Success", "Date successfully updated ")
                print("Reservation updated successfully.")
            except ValueError as ve:
                messagebox.showerror("Error", str(ve))
                print(f"Error: {ve}")

            except sqlite3.Error as e:
                messagebox.showerror("Error", f"SQLite error: {e}")
                print(f"Error: {e}")

        elif column_to_update == 'table_amount':
    # Ask the user if they want to add or remove tables
            add_or_remove = simpledialog.askstring("Input", "Do you want to add or remove tables?")

            if not add_or_remove:
                return  # User clicked Cancel

    # Prompt the user for the value to add or remove
            try:
                if add_or_remove =="add":
                    user_input_value = simpledialog.askinteger("Input", "Enter the value to add")
                    if user_input_value is None:
                        return  # User clicked Cancel
                    
                elif add_or_remove =="ADD":
                    user_input_value = simpledialog.askinteger("Input", "Enter the value to add")
                    if user_input_value is None:
                        return  # User clicked Cancel
                
                elif add_or_remove == "remove":
                    user_input_value = simpledialog.askinteger("Input", "Enter the value to remove")
                    if user_input_value is None:
                        return  # User clicked Cancel
                    
                elif add_or_remove == "REMOVE":
                    user_input_value = simpledialog.askinteger("Input", "Enter the value to remove")
                    if user_input_value is None:
                        return  # User clicked Cancel
            except ValueError:
                messagebox.showerror("Error", "Invalid input. Please enter a valid numeric value.")
                return

    # Get the branch and table_id from the reservation information
            with Session() as session:
                result = (
                    session.query(Reservation.branch, Reservation.table_id)
                    .filter(Reservation.id == reservation_id)
                    .first()
                )
            if not result:
                messagebox.showerror("Error", f"Reservation with ID {reservation_id} not found.")
                return

            existing_branch, existing_table_id = result

            with self.conn:
                if add_or_remove.lower() == 'add':
            # Increase the available tables in the 'tables' table and update the reservation
                    with Session() as session:
    # Update table_amount in 'tables' table
                        session.execute(
                            update(Table)
                            .where((Table.branch == existing_branch) & (Table.id == existing_table_id))
                            .values({Table.table_amount: Table.table_amount - user_input_value})
                        )
                        session.commit()

    # Update table_amount in 'reservations' table
                        session.execute(
                            update(Reservation)
                            .where(Reservation.id == reservation_id)
                            .values({Reservation.table_amount: Reservation.table_amount + user_input_value})
                        )
                        session.commit()
                        
                    messagebox.showinfo("Added", "Table successfully added")
                    print(f"Added tables to the 'tables' table and updated reservation: {reservation_id}")
                    print(f"Added tables back to the 'tables' table for branch: {existing_branch}, id: {existing_table_id}")
                elif add_or_remove.lower() == 'remove':
            # Decrease the available tables in the 'tables' table and update the reservation
                    with Session() as session:
    # Update table_amount in 'tables' table
                        session.execute(
                            update(Table)
                            .where((Table.branch == existing_branch) & (Table.id == existing_table_id))
                            .values({Table.table_amount: Table.table_amount - user_input_value})
                        )
                        session.commit()

    # Update table_amount in 'reservations' table
                        session.execute(
                            update(Reservation)
                            .where(Reservation.id == reservation_id)
                            .values({Reservation.table_amount: Reservation.table_amount - user_input_value})
                        )
                        session.commit()
                    messagebox.showinfo("Added", "Table successfully removed")
                    print(f"Removed tables from the 'tables' table and updated reservation: {reservation_id}")
                else:
                    messagebox.showerror("Error", "Invalid choice. Please choose 'add' or 'remove'.")
                    return
        elif column_to_update == 'branch_number':
            try:
        # Use SQLAlchemy to get distinct branch numbers from the 'tables' table
                branch_numbers_query = self.session.query(distinct(Table.branch_number)).all()

        # Extract branch numbers from the query result
                branch_numbers = [str(branch_number[0]) for branch_number in branch_numbers_query]

                if not branch_numbers:
                    messagebox.showerror("Error", "No branch numbers found in the 'tables' table.")
                    return

        # Create a top-level window for the dropdown menu
                dropdown_window = tk.Toplevel(self.master)
                dropdown_window.title("UPDATE BRANCH NUMBER")
                name = tk.Label(dropdown_window, text="Choose new branch number:")
                name.pack()

        # Create a StringVar to hold the selected branch number
                selected_branch_number = tk.StringVar()

        # Create a dropdown menu with the dynamically fetched branch numbers
                branch_number_menu = ttk.Combobox(dropdown_window, textvariable=selected_branch_number, values=branch_numbers)
                branch_number_menu.pack(padx=10, pady=10)

        # Add an "OK" button to confirm the selection
                ok_button = tk.Button(dropdown_window, text="OK", command=lambda: dropdown_window.destroy())
                ok_button.pack(pady=5)

        # Wait for the dropdown window to be closed
                dropdown_window.wait_window()

                new_value = selected_branch_number.get()

        # Update branch_number in 'tables' table
                self.session.execute(
                    update(Table)
                    .where((Table.branch == existing_branch) & (Table.id == existing_table_id))
                    .values({Table.branch_number: new_value})
                )
                self.session.commit()

        # Update branch_number in 'reservations' table
                self.session.execute(
                    update(Reservation)
                    .where(Reservation.id == reservation_id)
                    .values({Reservation.branch_number: new_value})
                )
                self.session.commit()

                messagebox.showinfo("Updated", "Branch number successfully updated")

            except sqlalchemy.exc.SQLAlchemyError as e:
                self.session.rollback()  # Rollback the transaction in case of an error
                messagebox.showerror("Error", f"SQLAlchemy error: {e}")
            finally:
                self.session.close()


    def get_branches_from_tables(self):
        try:
            # Use SQLAlchemy to get distinct branch names from the 'tables' table
            branches = self.session.query(distinct(Table.branch)).all()

            # Extract branch names from the query result
            branch_names = [branch[0] for branch in branches]

            return branch_names
        except sqlalchemy.exc.SQLAlchemyError as e:
            messagebox.showerror("Error", f"SQLAlchemy error: {e}")
            return []
        finally:
            self.session.close()
        
    
    def ask_for_date_entry(self):
    # Create a top-level window for the DateEntry widget
        date_window = tk.Toplevel(self.master)
        date_window.title("UPDATE DATE")
        name = tk.Label(date_window, text="Choose new date:")
        name.pack()
        

    # Create a DateEntry widget
        date_entry = DateEntry(date_window, width=12, background='darkblue', borderwidth=2, mindate=dt.date.today())
        date_entry.pack(padx=10, pady=10)

        def on_ok():
        # Retrieve the selected date
            new_date = date_entry.get_date()
        # Convert the date to a string if needed
            new_date_str = new_date.strftime('%Y-%m-%d')
            self.selected_date = new_date_str
            print(new_date_str)
            
            date_window.destroy()

    # Add an "OK" button to confirm the date selection
        ok_button = tk.Button(date_window, text="OK", command=on_ok)
        ok_button.pack(pady=5)

    # Wait for the date window to be closed
        date_window.wait_window()
        return self.selected_date

        
    
    def delete_reservation(self, listbox):
        # Get the selected index from the listbox
        selected_index = listbox.curselection()

        if selected_index:
            # Get the selected item from the listbox
            selected_item = listbox.get(selected_index)

            # Extract the reservation ID from the selected item (assuming it's the first part of the information)
            reservation_id = self.extract_reservation_id(selected_item)

            # Fetch the table amount for the reservation
            table_amount = self.get_reservation_table_amount(reservation_id)

            # Delete the reservation from the database
            if reservation_id:
                try:
                    with self.session.begin():
                        # Delete the reservation from the 'reservations' table
                        reservation_alias = aliased(Reservation)
                        self.session.query(reservation_alias).filter(reservation_alias.id == reservation_id).delete()


                    # Add the tables back to the 'tables' table
                    self.add_tables_back(selected_item, table_amount)

                    messagebox.showinfo("Deleted", "Reservation successfully cancelled")

                    # Delete the selected item from the listbox
                    listbox.delete(selected_index)
                    print(f"Deleted reservation from the listbox with ID: {reservation_id}")

                    self.session.commit()  # Commit the transaction

                except sqlalchemy.exc.SQLAlchemyError as e:
                    self.session.rollback()  # Rollback the transaction in case of an error
                    messagebox.showerror("Error", f"SQLAlchemy error: {e}")

                finally:
                    self.session.close()  # Close the session

            else:
                print("Failed to extract reservation ID from the selected item")
        else:
            messagebox.showinfo("No selection", "Please select a reservation to cancel")
            print("No reservation selected")

    
    def get_reservation_table_amount(self, reservation_id):
        try:
            # Use SQLAlchemy to query the 'reservations' table
            result = self.session.query(Reservation.table_amount).filter(Reservation.id == reservation_id).scalar()
            return result if result is not None else 0

        except sqlalchemy.exc.SQLAlchemyError as e:
            messagebox.showerror("Error", f"SQLAlchemy error: {e}")
            return 0
        finally:
            self.session.close()
    
    def add_tables_back(self, reservation_info, table_amount):
        # Extract the branch and date from the reservation information
        branch = self.extract_branch_from_reservation(reservation_info)
        date_info = self.extract_date_from_reservation(reservation_info)
        table_amount = self.extract_table_amount_from_reservation(reservation_info)

        try:
            # Use SQLAlchemy to update the 'tables' table
            with self.session.begin():
                self.session.query(Table).filter(Table.branch == branch, Table.reset_date == date_info).\
                    update({Table.table_amount: Table.table_amount + table_amount})
                self.session.commit()
            print(f"Added tables back to the 'tables' table for branch: {branch}, date: {date_info}")

        except sqlalchemy.exc.SQLAlchemyError as e:
            messagebox.showerror("Error", f"SQLAlchemy error: {e}")
        
    def get_branch_numbers_from_tables(self):
        try:
        # Use SQLAlchemy to get distinct branch numbers from the 'tables' table
            branch_numbers = self.session.query(distinct(Table.branch_number)).all()

        # Extract branch numbers from the query result
            branch_numbers_list = [branch_number[0] for branch_number in branch_numbers]

            return branch_numbers_list
        except sqlalchemy.exc.SQLAlchemyError as e:
            messagebox.showerror("Error", f"SQLAlchemy error: {e}")
            return []
        finally:
            self.session.close()


    def show_tables_left_on_date(self):
    # Get the user input for a specific date
        specific_date = simpledialog.askstring("Input", "Enter the date to check (YYYY-MM-DD):")

        if not specific_date:
            return  # User clicked Cancel

        try:
        # Fetch all branches and branch numbers
            entered_date = datetime.strptime(specific_date, '%Y-%m-%d').date()
            today = datetime.now().date()

            if entered_date < today:
                messagebox.showerror("Invalid Date", "Past date not allowed.")
                return
            all_branches = self.get_branches_from_tables()
            all_branch_numbers = self.get_branch_numbers_from_tables()

            info_message = ""

        # Display information for each branch
            for branch in all_branches:
            # Display information for each branch number
                for branch_number in all_branch_numbers:
                # Use SQLAlchemy to query the 'tables' table
                    result = self.session.query(Table.table_amount).filter(
                        Table.reset_date == specific_date,
                        Table.branch == branch,
                        Table.branch_number == branch_number
                    ).first()

                    parse(specific_date)

                    if result is not None:
                        tables_left_value = str(result[0]).strip('(),')
                        info_message += f"Branch: {branch}, Branch Number: {branch_number}, Tables Left: {tables_left_value}\n"
                    else:
                    # If information is not available in the database, assume Tables Left: 100
                        info_message += f"Branch: {branch}, Branch Number: {branch_number}, Tables Left: 100\n"

        # Show all information in a single messagebox
            if info_message:
                messagebox.showinfo("Tables Left on Date", info_message)
            else:
                messagebox.showinfo("Tables Left on Date", f"No information found for {specific_date}")

        except ValueError:
            messagebox.showerror("Error", "Invalid date. Please enter a valid date in the format YYYY-MM-DD.")

        except sqlalchemy.exc.SQLAlchemyError as e:
            messagebox.showerror("Error", f"SQLAlchemy error: {e}")
        finally:
            self.session.close()

    def extract_branch_from_reservation(self, reservation_info):
    # Extract the branch from the reservation information
        if "Branch:" in reservation_info:
            branch_part = reservation_info.split("Branch:")[1].strip()
            return branch_part.split()[0]

        return None
    
    
    def extract_table_amount_from_reservation(self, reservation_info):
    # Extract the table amount from the reservation information
        if "Tables:" in reservation_info:
            table_part = reservation_info.split("Tables:")[1].strip()
            return table_part.split()[0]

        return None
    
    def extract_date_from_reservation(self, reservation_info):
    # Extract the date from the reservation information
        if "Date:" in reservation_info:
            date_part = reservation_info.split("Date:")[1].strip()
            return date_part.split()[0]

        return None

    def check_reservation_id_exists(self, reservation_id):
    # Check if the reservation ID exists in the reservations table

        try:
        # Use SQLAlchemy to check if a reservation with the given ID exists
            exists_query = self.session.query(exists().where(Reservation.id == reservation_id))
            return exists_query.scalar()
        except sqlalchemy.exc.SQLAlchemyError as e:
            messagebox.showerror("Error", f"SQLAlchemy error: {e}")
            return False
        finally:
            self.session.close()
        

    def extract_reservation_id(self, reservation_info):
        # Check if the information string starts with "Reservation ID:" and extract the ID
        if reservation_info.startswith("Reservation ID:"):
            parts = reservation_info.split()

            # Check if the ID is present after "Reservation ID:" and is a valid integer
            if len(parts) > 2 and parts[2].isdigit():
                return int(parts[2])

        return None


    def show_first_window(self):
        if hasattr(self, 'reservations_window'):  # Check if the window has been created
            self.reservations_window.destroy()  # Destroy the second window if it exists
        self.master.deiconify()  # Uniconify the main window
        self.master.title("RESERVATIONS")
        self.master.minsize(width=500, height=200)
        self.master.resizable(False, False)  # Reset the title if needed
        self.center_window(self.master)

if __name__ == "__main__":
    root = tk.Tk()
    app = Reservations(root, engine)
    root.mainloop()





# import tkinter as tk
# from tkinter import messagebox, simpledialog, ttk
# from tkcalendar import DateEntry
# import sqlite3
# import datetime
# from dateutil.parser import parse
# # from alchemy import Base, Table, Reservation, engine, Session

# class Reservation:
#     def __init__(self, master):
#         self.master = master
#         self.master.title("RESERVATIONS")
#         self.master.minsize(width=500, height=250)
#         self.master.resizable(False, False)
        
#         # Base.metadata.create_all(engine)
#         # self.session = Session()
        
#         self.conn = sqlite3.connect('reservation.db')
#         self.cur = self.conn.cursor()
        

#         # GUI elements
#         self.label_date = tk.Label(master, text="Select reservation date:")
#         self.entry_date = DateEntry(master, width=12, background='darkblue', borderwidth=2, mindate=datetime.date.today())
#         self.label_branch = tk.Label(master, text="Choose branch(scroll for more branches):")
#         self.branches_listbox = tk.Listbox(master, selectmode=tk.SINGLE)
#         self.populate_branches_listbox()

#         entry_button = tk.Frame(self.master)

#         self.table_amount_label = tk.Label(master, text="Input number of tables:")
#         self.table_amount_entry = tk.Entry(entry_button)

#         # Validate the table_amount for numeric values only
#         vcmd = master.register(self.validate_table_amount)
#         self.table_amount_entry.config(validate="key", validatecommand=(vcmd, "%P"))

#         self.button_reserve = tk.Button(entry_button, text="RESERVE ", command=self.reserve_table)

#         self.label_options= tk.Label(master, text="Other options:")

#         # Layout
#         self.label_date.pack()
#         self.entry_date.pack()
#         self.label_branch.pack()
#         self.branches_listbox.pack()
#         self.table_amount_label.pack()
#         entry_button.pack()
#         self.table_amount_entry.pack(side = tk.LEFT)
#         self.button_reserve.pack(side = tk.RIGHT)

#         self.label_options.pack()

#         self.button_show_reservations = tk.Button(master, text="VIEW RESERVATIONS", command=self.show_reservations)
#         self.button_show_reservations.pack()

#         self.button_show_tables_left = tk.Button(master, text="CHECK TABLE AVAILABILITY FOR SPECIFIC DATE", command=self.show_tables_left_on_date)
#         self.button_show_tables_left.pack()

#         self.center_window(self.master)
#         # Call create_tables to ensure tables are created
#         self.create_tables()
    
#     def create_tables(self):
#         # Create a reservation table if it doesn't exist

#         self.cur.execute("""
#                 CREATE TABLE IF NOT EXISTS tables (
#                     id INTEGER PRIMARY KEY ,
#                     branch TEXT NOT NULL,
#                     table_amount INT NOT NULL,
#                     reset_date DATE
#                 )
#             """)
        
#         self.cur.execute("""
#                 CREATE TABLE IF NOT EXISTS reservations (
#                     id INTEGER PRIMARY KEY AUTOINCREMENT,
#                     date DATE NOT NULL,
#                     branch TEXT NOT NULL,
#                     table_amount INT NOT NULL,
#                     table_id INT,
#                     FOREIGN KEY (table_id) REFERENCES tables(id)
#                 )
#             """)
        
#         # self.cur.execute("""INSERT INTO tables (id, branch, table_amount, reset_date) 
#         #                 VALUES (8, 'Birmingham', 60, DATE('now'));""") 


#     # Commit changes)
#         self.conn.commit()
        
#     def center_window(self, window, reference_window=None):
#     # Get the screen width and height
#         screen_width = window.winfo_screenwidth()
#         screen_height = window.winfo_screenheight()

#         if reference_window:
#         # Center the window relative to the reference window
#             x_position = reference_window.winfo_x() + (reference_window.winfo_width() - window.winfo_reqwidth()) // 2
#             y_position = reference_window.winfo_y() + (reference_window.winfo_height() - window.winfo_reqheight()) // 2
#         else:
#         # Center the window on the screen, considering minsize
#             min_width, min_height = window.minsize()
#             x_position = (screen_width - max(window.winfo_reqwidth(), min_width)) // 2
#             y_position = (screen_height - max(window.winfo_reqheight(), min_height)) // 2

#     # Set the window position
#         window.geometry(f"+{x_position}+{y_position}")


#     def validate_table_amount(self, new_value):
#         # Validate that the input for table_amount contains only numeric values
#         if new_value.isdigit():
#             return int(new_value) >= 1
#         return new_value == ""
    
#     def reset_fields(self):
#         # Reset the input fields after successfully reserving a table
#         self.entry_date.delete(0, tk.END)
#         self.branches_listbox.selection_clear(0, tk.END)
#         self.table_amount_entry.delete(0, tk.END)

#     # def populate_branches_listbox(self):
#     #     # Fetch branches from the 'tables' table and populate the listbox

#     #     branches = self.cur.execute("SELECT DISTINCT branch FROM tables").fetchall()
#     #     today = datetime.date.today()

#     #     for branch in branches:
#     #         self.cur.execute("INSERT INTO tables (branch, table_amount, reset_date) VALUES (?, 100, ?)",
#     #                      (branch[0], today))

#     #         self.branches_listbox.insert(tk.END, branch[0])

#     #     self.branches_listbox.config(height=min(5, len(branches)))
    
#     def populate_branches_listbox(self):
#     # Fetch branches from the 'tables' table and populate the listbox

#         branches = self.get_branches_from_tables()
#         today = datetime.date.today()

#         if not branches:
#             messagebox.showerror("Error", "No branches found in the 'tables' table.")
#             return

#         for branch in branches:
#             self.branches_listbox.insert(tk.END, branch)

#         self.branches_listbox.config(height=min(5, len(branches)))


#     def check_capacity(self, table_amount):
#     # Check if the table_amount exceeds the remaining capacity for the branch  

#         number_of_tables = 100

#         selected_branch_0 = self.branches_listbox.curselection()
#         selected_branch = self.branches_listbox.get(selected_branch_0)

#         if not selected_branch_0:
#             messagebox.showerror("Error", "Please select a branch")
#             return None

#     # Get the first selected branch (assuming you only allow single selection)

#         table_amount = int(self.table_amount_entry.get())
#         reservation_date = self.entry_date.get_date()

#         result = self.cur.execute("SELECT table_amount FROM tables WHERE branch=? AND reset_date=?", (selected_branch,reservation_date)).fetchone()

#         if result:
#             remaining_capacity = result[0]

#             print(remaining_capacity)

#             if remaining_capacity >= table_amount:
#                 return remaining_capacity
                         
#             else:
#                 messagebox.showerror("Error", f"Only {remaining_capacity} tables available")
#         else:
#                 # Insert a new row if it doesn't exist
#                     self.cur.execute("INSERT INTO tables (branch, table_amount, reset_date) VALUES (?, ?, ?)",
#                                  (selected_branch, number_of_tables, reservation_date))
#                     result = self.cur.execute("SELECT table_amount FROM tables WHERE branch=? AND reset_date=?", (selected_branch,reservation_date)).fetchone()
#                     remaining_capacity = result[0]

#                     if remaining_capacity >= table_amount:
#                         return remaining_capacity
                         
#                     else:
#                         messagebox.showerror("Error", f"Only {remaining_capacity} tables available")


#     def reserve_table(self):
#         number_of_tables = 100

#         date = self.entry_date.get_date()
#         selected_branch_0 = self.branches_listbox.curselection()
#         table_amount = self.table_amount_entry.get()

#         if not date or not selected_branch_0 or not table_amount:
#             messagebox.showerror("Error", "Please enter all fields")
#             return

#     # Get the selected branch from the listbox
#         selected_branch = self.branches_listbox.get(selected_branch_0)

#         remaining_capacity = self.check_capacity(table_amount)
#         if remaining_capacity is not None:
#         # Reserve table
#             table_id = self.get_table_id(selected_branch, date )

#             try:
#             # Update the table amount in the 'tables' table
#             # Check if a row with the same branch and reset date already exists
#                 existing_row = self.cur.execute("SELECT id FROM tables WHERE branch=? AND reset_date=?", (selected_branch, date)).fetchone()

#                 if existing_row:
#                 # Update the existing row
#                     self.cur.execute("UPDATE tables SET table_amount = table_amount - ? WHERE id = ?", (table_amount, existing_row[0]))
#                 else:
#                 # Insert a new row if it doesn't exist
#                     self.cur.execute("INSERT INTO tables (branch, table_amount, reset_date) VALUES (?, ?, ?)",
#                                  (selected_branch, number_of_tables, date))
                    
#                     self.cur.execute("UPDATE tables SET table_amount = table_amount - ? WHERE branch = ? AND reset_date = ?", 
#                              (table_amount, selected_branch, date))

#             # Insert the reservation into the 'reservations' table
#                 self.cur.execute("INSERT INTO reservations (date, branch, table_amount, table_id) VALUES (?, ?, ?, ?)",
#                              (date, selected_branch, table_amount, table_id))

#             # Update the 'table_amount' in the 'tables' table for the current branch and date
                
#                 self.conn.commit()
#                 messagebox.showinfo("Success", "Table reserved successfully")
#                 self.reset_fields()
#             except sqlite3.Error as e:
#                 messagebox.showerror("Error", f"SQLite error: {e}")
#         else:
#             print("Capacity check failed")

#     def get_table_id(self, branch, date):
#         # Get the table_id for the given branch

#         result = self.cur.execute("SELECT id FROM tables WHERE branch=? AND reset_date=?", (branch, date,)).fetchone()
#         return result[0]

#     def show_reservations(self):
#     # Create a new window for displaying reservations
#         self.reservations_window = tk.Toplevel(self.master)
#         self.reservations_window.minsize(width=500, height=350)
#         self.reservations_window.title("RESERVATION DETAILS")
#         self.center_window(self.reservations_window)

#     # Create a search bar
#         search_frame = ttk.Frame(self.reservations_window)
#         search_frame.pack(side=tk.TOP, expand=True, fill=tk.X, pady=10)

#         search_label = tk.Label(search_frame, text="Enter Reservation ID:")
#         search_label.pack(side=tk.LEFT, padx=10)

#         search_entry = tk.Entry(search_frame)
#         search_entry.pack(side=tk.LEFT, padx=10)

#         search_button = tk.Button(search_frame, text="Search", command=lambda: self.search_reservation(search_entry.get()))
#         search_button.pack(side=tk.LEFT, padx=10)

#     # Create a Listbox and Scrollbar in the top frame
#         frame1 = ttk.Frame(self.reservations_window)
#         frame1.pack(side=tk.TOP, expand=True, fill=tk.BOTH)

#         reservations_listbox = tk.Listbox(frame1, selectmode=tk.SINGLE)
#         scrollbar = tk.Scrollbar(frame1, command=reservations_listbox.yview)
#         reservations_listbox.config(yscrollcommand=scrollbar.set)

#     # Populate Listbox with reservations data
#         self.populate_reservations_listbox(reservations_listbox)

#     # Pack Listbox and Scrollbar
#         reservations_listbox.pack(expand=True, fill=tk.BOTH)
#         scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

#     # Create a frame for the delete button in the bottom frame
#         frame2 = ttk.Frame(self.reservations_window)
#         frame2.pack(side=tk.BOTTOM)

#     # Add an "Update Reservation" button
#         update_button = tk.Button(frame2, text="UPDATE RESERVATION", command=self.update_reservation)
#         update_button.pack()

#     # Add a "Delete Reservation" button
#         delete_button = tk.Button(frame2, text="CANCEL RESERVATION", command=lambda: self.delete_reservation(reservations_listbox))
#         delete_button.pack()

#     # Add a "Go Back" button
#         go_back_button = tk.Button(frame2, text="Go Back", command=self.show_first_window)
#         go_back_button.pack()

#     def search_reservation(self, reservation_id):
#     # Search for the reservation with the given ID and display details
#         if reservation_id.isdigit():
#             reservation_id = int(reservation_id)

#             result = self.cur.execute("SELECT id, date, branch, table_amount FROM reservations WHERE id=?", (reservation_id,)).fetchone()

#             if result:
#                 reservation_details = f"Reservation ID: {result[0]}  Date: {result[1]}  Branch: {result[2]}  Tables: {result[3]}"
#                 messagebox.showinfo("Reservation Details", reservation_details)
#             else:
#                 messagebox.showinfo("Reservation Details", f"No reservation found with ID {reservation_id}")
#         else:
#             messagebox.showerror("Invalid Input", "Please enter a valid Reservation ID.")


#     def populate_reservations_listbox(self, listbox):
#         # Fetch reservations from the 'reservations' table and populate the listbox

#         reservations = self.cur.execute("SELECT id, date, branch, table_amount FROM reservations").fetchall()
#         for reservation in reservations:
#             reservation_details = f"Reservation ID: {reservation[0]}  Date: {reservation[1]}  Branch: {reservation[2]}  Tables: {reservation[3]}"
#             listbox.insert(tk.END, reservation_details)

#     def update_reservation(self):
        
#     # Get the reservation ID from the user
#         reservation_id = simpledialog.askinteger("Input", "Enter Reservation ID to update:")
#         date = self.entry_date.get_date()

#         if reservation_id is None:
#             return  # User clicked Cancel

#         if not self.check_reservation_id_exists(reservation_id):
#             messagebox.showerror("Error", f"Reservation does not exist.")
#             return
        
#         # result = self.cur.execute("SELECT branch, table_id, table_amount FROM reservations WHERE id=?", (reservation_id,)).fetchone()

#         # if not result:
#         #     messagebox.showerror("Error", f"Reservation with ID {reservation_id} not found.")

#         # existing_branch, existing_table_id, existing_table_amount = result


#     # Get the column to update and the new value
#         while True:
#             column_to_update = simpledialog.askstring("Input", "Enter the column to update ('date' 'branch' or 'table_amount'):")
#             if not column_to_update:
#                 return  # User clicked Cancel

#             try:
#                 self.cur.execute(f"PRAGMA table_info(reservations);")
#                 columns = [column[1] for column in self.cur.fetchall()]

#                 if column_to_update not in columns:
#                     messagebox.showerror("Error", f"The column '{column_to_update}' does not exist. Column inputted has to match any of the three given")
#                     continue
#                 else:
#                     break
#             except sqlite3.Error as e:
#                 messagebox.showerror("Error", f"SQLite error: {e}")
#                 return

#     # If the column to update is 'branch', provide a dropdown menu with options from the 'tables' table
#         if column_to_update == 'branch':
#     # Fetch branches from the 'tables' table and create a list for the dropdown menu
#             branches = self.get_branches_from_tables()
#             if not branches:
#                 messagebox.showerror("Error", "No branches found in the 'tables' table.")
#                 return

#     # Create a top-level window for the dropdown menu
#             dropdown_window = tk.Toplevel(self.master)
#             dropdown_window.title("UPDATE BRANCH")
#             name = tk.Label(dropdown_window, text="Choose new branch:")
#             name.pack()

#     # Create a StringVar to hold the selected branch
#             selected_branch = tk.StringVar()

#     # Create a dropdown menu with the branches
#             branch_menu = ttk.Combobox(dropdown_window, textvariable=selected_branch, values=branches)
#             branch_menu.pack(padx=10, pady=10)

#     # Add an "OK" button to confirm the selection
#             ok_button = tk.Button(dropdown_window, text="OK", command=lambda: dropdown_window.destroy())
#             ok_button.pack(pady=5)

#             result = self.cur.execute("SELECT table_amount, table_id FROM reservations WHERE id = ?", (reservation_id,)).fetchone()
#             if result:
#                 table_from_result, old_table_id = result
#                 print(table_from_result)
#                 print (old_table_id)

#     # Wait for the dropdown window to be closed
#             dropdown_window.wait_window()

#     # Retrieve the selected branch
#             new_value = selected_branch.get()
        

#     # Get the new table_id for the selected branch
#             new_table_id = self.get_table_id(new_value, date)

#             with self.conn:
#         # Update the 'branch' and 'table_id' columns in the 'reservations' table
#                 self.cur.execute("UPDATE reservations SET branch=?, table_id=? WHERE id=?", (new_value, new_table_id, reservation_id))

#         # Update the 'table_amount' in the 'tables' table for the old branch
#                 self.cur.execute("UPDATE tables SET table_amount = table_amount + ? WHERE id = ?", (table_from_result, old_table_id))

#         # Update the 'table_amount' in the 'tables' table for the new branch
#                 self.cur.execute("UPDATE tables SET table_amount = table_amount - ? WHERE branch = ?", (table_from_result, new_value))
#                 messagebox.showinfo("Success", "Branch successfully updated ")


#         elif column_to_update == 'date':
#         # If the column to update is 'date', use DateEntry for input
#             new_value = self.ask_for_date_entry()
#             print(new_value)
#             if new_value is None:
#                 return  # User clicked Cancel
#             try:
#                 if not new_value and column_to_update != 'date':
#                     raise ValueError("Please enter a new value.")
                
#                 with self.conn:
#                     self.cur.execute("UPDATE reservations SET date = ? WHERE id=?", (new_value, reservation_id))

#                 print(f"Updated reservation in the database: {reservation_id}")

#                 messagebox.showinfo("Success", "Date successfully updated ")
#                 print("Reservation updated successfully.")
#             except ValueError as ve:
#                 messagebox.showerror("Error", str(ve))
#                 print(f"Error: {ve}")
                
#             except sqlite3.Error as e:
#                 messagebox.showerror("Error", f"SQLite error: {e}")
#                 print(f"Error: {e}")

#         elif column_to_update == 'table_amount':
#     # Ask the user if they want to add or remove tables
#             add_or_remove = simpledialog.askstring("Input", "Do you want to add or remove tables?")

#             if not add_or_remove:
#                 return  # User clicked Cancel

#     # Prompt the user for the value to add or remove
#             try:
#                 if add_or_remove =="add" or "ADD":
#                     user_input_value = simpledialog.askinteger("Input", "Enter the value to add")
#                     if user_input_value is None:
#                         return  # User clicked Cancel
                
#                 elif add_or_remove == "remove" or "REMOVE":
#                     user_input_value = simpledialog.askinteger("Input", "Enter the value to remove")
#                     if user_input_value is None:
#                         return  # User clicked Cancel
#             except ValueError:
#                 messagebox.showerror("Error", "Invalid input. Please enter a valid numeric value.")
#                 return

#     # Get the branch and table_id from the reservation information
#             result = self.cur.execute("SELECT branch, table_id FROM reservations WHERE id = ?", (reservation_id,)).fetchone()
#             if not result:
#                 messagebox.showerror("Error", f"Reservation with ID {reservation_id} not found.")
#                 return

#             existing_branch, existing_table_id = result

#             with self.conn:
#                 if add_or_remove.lower() == 'add':
#             # Increase the available tables in the 'tables' table and update the reservation
#                     self.cur.execute("UPDATE tables SET table_amount = table_amount - ? WHERE branch = ? AND id = ?", 
#                             (user_input_value, existing_branch, existing_table_id))
#                     self.cur.execute("UPDATE reservations SET table_amount = table_amount + ? WHERE id=?", (user_input_value, reservation_id))
#                     messagebox.showinfo("Added", "Table successfully added")
#                     print(f"Added tables to the 'tables' table and updated reservation: {reservation_id}")
#                     print(f"Added tables back to the 'tables' table for branch: {existing_branch}, id: {existing_table_id}")
#                 elif add_or_remove.lower() == 'remove':
#             # Decrease the available tables in the 'tables' table and update the reservation
#                     self.cur.execute("UPDATE tables SET table_amount = table_amount - ? WHERE branch = ? AND id = ?", 
#                             (user_input_value, existing_branch, existing_table_id))
#                     self.cur.execute("UPDATE reservations SET table_amount = table_amount - ? WHERE id=?", (user_input_value, reservation_id))
#                     messagebox.showinfo("Added", "Table successfully removed")
#                     print(f"Removed tables from the 'tables' table and updated reservation: {reservation_id}")
#                 else:
#                     messagebox.showerror("Error", "Invalid choice. Please choose 'add' or 'remove'.")
#                     return

#     def get_branches_from_tables(self):
#     # Fetch branches from the 'tables' table

#         try:
#             branches = self.cur.execute("SELECT DISTINCT branch FROM tables").fetchall()
#             return [branch[0] for branch in branches]
#         except sqlite3.Error as e:
#             messagebox.showerror("Error", f"SQLite error: {e}")
#             return None
        
    
#     def ask_for_date_entry(self):
#     # Create a top-level window for the DateEntry widget
#         date_window = tk.Toplevel(self.master)
#         date_window.title("UPDATE DATE")
#         name = tk.Label(date_window, text="Choose new date:")
#         name.pack()
        

#     # Create a DateEntry widget
#         date_entry = DateEntry(date_window, width=12, background='darkblue', borderwidth=2, mindate=datetime.date.today())
#         date_entry.pack(padx=10, pady=10)

#         def on_ok():
#         # Retrieve the selected date
#             new_date = date_entry.get_date()
#         # Convert the date to a string if needed
#             new_date_str = new_date.strftime('%Y-%m-%d')
#             self.selected_date = new_date_str
#             print(new_date_str)
            
#             date_window.destroy()

#     # Add an "OK" button to confirm the date selection
#         ok_button = tk.Button(date_window, text="OK", command=on_ok)
#         ok_button.pack(pady=5)

#     # Wait for the date window to be closed
#         date_window.wait_window()
#         return self.selected_date

        
    
#     def delete_reservation(self, listbox):
#     # Get the selected index from the listbox
#         selected_index = listbox.curselection()

#         if selected_index:
#         # Get the selected item from the listbox
#             selected_item = listbox.get(selected_index)

#         # Extract the reservation ID from the selected item (assuming it's the first part of the information)
#             reservation_id = self.extract_reservation_id(selected_item)

#         # Fetch the table amount for the reservation
#             table_amount = self.get_reservation_table_amount(reservation_id)

#         # Delete the reservation from the database
#             if reservation_id:
                
#                 with self.conn:
#                 # Delete the reservation from the 'reservations' table
#                     self.cur.execute("DELETE FROM reservations WHERE id=?", (reservation_id,))
#                     print(f"Deleted reservation from the database with ID: {reservation_id}")

#                 # Add the tables back to the 'tables' table
#                     self.add_tables_back(selected_item, table_amount)

#                     messagebox.showinfo("Deleted", "Reservation successfully cancelled")

#             # Delete the selected item from the listbox
#                 listbox.delete(selected_index)
#                 print(f"Deleted reservation from the listbox with ID: {reservation_id}")

#             else:
#                 print("Failed to extract reservation ID from the selected item")

#         else:
#             messagebox.showinfo("No selection", "Please select a reservation to cancel")
#             print("No reservation selected")

    
#     def get_reservation_table_amount(self, reservation_id):
#     # Get the table amount for the given reservation

#         try:
#             result = self.cur.execute("SELECT table_amount FROM reservations WHERE id=?", (reservation_id,)).fetchone()
#             return result[0] if result else 0
#         except sqlite3.Error as e:
#             messagebox.showerror("Error", f"SQLite error: {e}")
#             return 0
    
#     def add_tables_back(self, reservation_info, table_amount):
#     # Extract the branch and date from the reservation information
#         branch = self.extract_branch_from_reservation(reservation_info)
#         date_info = self.extract_date_from_reservation(reservation_info)
#         table_amount = self.extract_table_amount_from_reservation(reservation_info)

#         print (table_amount)
#         try:
#             with self.conn:
#                 self.cur.execute("UPDATE tables SET table_amount = table_amount + ? WHERE branch = ? AND reset_date = ?", 
#                                  (table_amount, branch, date_info))
#             print(f"Added tables back to the 'tables' table for branch: {branch}, date: {date_info}")
#         except sqlite3.Error as e:
#             messagebox.showerror("Error", f"SQLite error: {e}")


#     def show_tables_left_on_date(self):
#     # Get the user input for a specific date
#         specific_date = simpledialog.askstring("Input", "Enter the date to check (YYYY-MM-DD):")

#         if not specific_date:
#             return  # User clicked Cancel

#         try:
#         # Query the database to get the remaining tables for the specific date
#             result = self.cur.execute("SELECT branch, table_amount FROM tables WHERE reset_date=?", (specific_date,)).fetchall()

#             parse(specific_date)

#             branches_in_database = set()
#             info_message = ""

#             if result:
#             # Display the remaining tables for each branch on the specific date
#                 for row in result:
#                     branch, tables_left = row
#                     branches_in_database.add(branch)
#                     info_message += f"Branch: {branch}, Tables Left: {tables_left}\n"

#         # Fetch all branches from the 'tables' table
#             all_branches = self.get_branches_from_tables()

#         # Display branches not in the database with 100 tables left
#             for branch in all_branches:
#                 if branch not in branches_in_database:
#                     info_message += f"Branch: {branch}, Tables Left: 100\n"

#         # Show all information in a single messagebox
#             if info_message:
#                 messagebox.showinfo("Tables Left on Date", info_message)
#             else:
#                 messagebox.showinfo("Tables Left on Date", f"No information found for {specific_date}")
#         except ValueError:
#             messagebox.showerror("Error", "Invalid date. Please enter a valid date in the format YYYY-MM-DD.")

#         except sqlite3.Error as e:
#             messagebox.showerror("Error", f"SQLite error: {e}")

#     def extract_branch_from_reservation(self, reservation_info):
#     # Extract the branch from the reservation information
#         if "Branch:" in reservation_info:
#             branch_part = reservation_info.split("Branch:")[1].strip()
#             return branch_part.split()[0]

#         return None
    
    
#     def extract_table_amount_from_reservation(self, reservation_info):
#     # Extract the table amount from the reservation information
#         if "Tables:" in reservation_info:
#             table_part = reservation_info.split("Tables:")[1].strip()
#             return table_part.split()[0]

#         return None
    
#     def extract_date_from_reservation(self, reservation_info):
#     # Extract the date from the reservation information
#         if "Date:" in reservation_info:
#             date_part = reservation_info.split("Date:")[1].strip()
#             return date_part.split()[0]

#         return None

#     def check_reservation_id_exists(self, reservation_id):
#         # Check if the reservation ID exists in the reservations table

#         try:
#             self.cur.execute("SELECT id FROM reservations WHERE id=?", (reservation_id,))
#             return self.cur.fetchone() is not None
#         except sqlite3.Error as e:
#             messagebox.showerror("Error", f"SQLite error: {e}")
#             return False
        

#     def extract_reservation_id(self, reservation_info):
#         # Check if the information string starts with "Reservation ID:" and extract the ID
#         if reservation_info.startswith("Reservation ID:"):
#             parts = reservation_info.split()

#             # Check if the ID is present after "Reservation ID:" and is a valid integer
#             if len(parts) > 2 and parts[2].isdigit():
#                 return int(parts[2])

#         return None


#     def show_first_window(self):
#         if hasattr(self, 'reservations_window'):  # Check if the window has been created
#             self.reservations_window.destroy()  # Destroy the second window if it exists
#         self.master.deiconify()  # Uniconify the main window
#         self.master.title("RESERVATIONS")
#         self.master.minsize(width=500, height=200)
#         self.master.resizable(False, False)  # Reset the title if needed
#         self.center_window(self.master)

# if __name__ == "__main__":
#     root = tk.Tk()
#     app = Reservation(root)
#     root.mainloop()




