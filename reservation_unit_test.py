import unittest
from unittest.mock import patch, MagicMock
from reservation import Reservations, Reservation, Table
from sqlalchemy.testing import mock  # Import mock from SQLAlchemy's testing module
from datetime import datetime
from tkcalendar import DateEntry
import logging

class TestReservations(unittest.TestCase):

    def setUp(self):
        self.mock_engine = MagicMock()
        self.mock_master = MagicMock()

    def test_get_branches_from_tables(self):
        reservations_instance = Reservations(self.mock_master, self.mock_engine)
        fake_branch_names = ["Branch1", "Branch2", "Branch3"]
        fake_session = MagicMock()
        fake_session.query().all.return_value = [(branch,) for branch in fake_branch_names]

        with patch.object(reservations_instance, 'session', fake_session):
            branches = reservations_instance.get_branches_from_tables()
            self.assertEqual(branches, fake_branch_names)

    def test_ask_for_date_entry(self):
        reservations_instance = Reservations(self.mock_master, self.mock_engine)
        with patch('tk.Toplevel') as mock_toplevel, \
             patch.object(DateEntry, 'get_date', return_value=datetime(2023, 1, 1).date()) as mock_get_date:

            date_window = MagicMock()
            mock_toplevel.return_value = date_window
            result = reservations_instance.ask_for_date_entry()
            self.assertEqual(result, '2023-01-01')

    def test_delete_reservation(self):
        reservations_instance = Reservations(self.mock_master, self.mock_engine)
        with patch('tk.Listbox') as mock_listbox, \
             patch('tk.messagebox.showinfo') as mock_showinfo, \
             patch('tk.messagebox.showerror') as mock_showerror, \
             patch.object(reservations_instance, 'extract_reservation_id', return_value=123) as mock_extract_id, \
             patch.object(reservations_instance, 'get_reservation_table_amount', return_value=5) as mock_get_table_amount, \
             patch.object(reservations_instance, 'add_tables_back') as mock_add_tables_back:

            fake_session = MagicMock()
            fake_session.query().delete.return_value = None
            with patch.object(reservations_instance, 'session', fake_session):
                result = reservations_instance.delete_reservation(mock_listbox)
                mock_showinfo.assert_called_once_with("Deleted", "Reservation successfully cancelled")
                mock_add_tables_back.assert_called_once_with("Reservation ID: 123 Branch: TestBranch Date: 2023-01-01 Tables: 5", 5)

            mock_listbox.curselection.return_value = []
            result = reservations_instance.delete_reservation(mock_listbox)
            mock_showinfo.assert_not_called()
            mock_showerror.assert_called_once_with("No selection", "Please select a reservation to cancel")

    def test_check_reservation_id_exists(self):
        reservations_instance = Reservations(self.mock_master, self.mock_engine)
        with patch.object(reservations_instance, 'session') as mock_session:
            mock_session.query().scalar.return_value = True
            exists = reservations_instance.check_reservation_id_exists(123)
            self.assertTrue(exists)

            mock_session.query().scalar.return_value = False
            exists = reservations_instance.check_reservation_id_exists(456)
            self.assertFalse(exists)

    def test_extract_reservation_id(self):
        reservations_instance = Reservations(self.mock_master, self.mock_engine)
        reservation_info = "Reservation ID: 123 Branch: TestBranch Date: 2023-01-01 Tables: 5"
        result = reservations_instance.extract_reservation_id(reservation_info)
        self.assertEqual(result, 123)

        reservation_info = "Invalid Reservation Info"
        result = reservations_instance.extract_reservation_id(reservation_info)
        self.assertIsNone(result)

    def test_show_tables_left_on_date(self):
        reservations_instance = Reservations(self.mock_master, self.mock_engine)
        with patch('tk.simpledialog.askstring', return_value='2023-01-01') as mock_askstring, \
             patch.object(reservations_instance, 'get_branches_from_tables', return_value=['Branch1', 'Branch2']), \
             patch.object(reservations_instance, 'get_branch_numbers_from_tables', return_value=[1, 2]), \
             patch.object(reservations_instance, 'session') as mock_session:
            mock_session.query().first.return_value = (50,)

            with patch('tk.messagebox.showinfo') as mock_showinfo:
                reservations_instance.show_tables_left_on_date()

                mock_showinfo.assert_called_once_with(
                    "Tables Left on Date",
                    "Branch: Branch1, Branch Number: 1, Tables Left: 50\nBranch: Branch1, Branch Number: 2, Tables Left: 50\n"
                    "Branch: Branch2, Branch Number: 1, Tables Left: 50\nBranch: Branch2, Branch Number: 2, Tables Left: 50\n"
                )

    def test_update_reservation(self):
        reservations_instance = Reservations(self.mock_master, self.mock_engine)
        with patch('tk.simpledialog.askstring', return_value='branch') as mock_askstring, \
             patch('tk.simpledialog.askinteger', return_value=10) as mock_askinteger, \
             patch.object(reservations_instance, 'get_branches_from_tables', return_value=['Branch1', 'Branch2']), \
             patch.object(reservations_instance, 'get_table_id', return_value=1), \
             patch.object(reservations_instance, 'session') as mock_session:
            mock_session.query().first.return_value = (10, 1, 2, datetime(2023, 1, 1), 3)
            mock_session.query().filter_by().first.return_value = MagicMock()
            mock_session.query().filter_by().first().branch_number = 3

            with patch('tk.messagebox.showinfo') as mock_showinfo:
                reservations_instance.update_reservation(123)

                mock_showinfo.assert_called_once_with("Success", "Branch successfully updated")

    def test_invalid_column_to_update(self):
        reservations_instance = Reservations(self.mock_master, self.mock_engine)
        with patch('tk.simpledialog.askstring', return_value='invalid_column'), \
             patch('tk.messagebox.showerror') as mock_showerror:
            reservations_instance.update_reservation(123)
            mock_showerror.assert_called_once_with("Error", "The column 'invalid_column' does not exist. Column inputted has to match any of the four given")

    def test_invalid_date_format(self):
        reservations_instance = Reservations(self.mock_master, self.mock_engine)
        with patch('tk.simpledialog.askstring', return_value='date'), \
             patch('tk.simpledialog.askstring', side_effect=['invalid_date', '2023-01-01']), \
             patch('tk.messagebox.showerror') as mock_showerror:
            reservations_instance.update_reservation(123)
            mock_showerror.assert_called_once_with("Error", "Invalid date. Please enter a valid date in the format YYYY-MM-DD.")

    def test_invalid_add_or_remove_input(self):
        reservations_instance = Reservations(self.mock_master, self.mock_engine)
        with patch('tk.simpledialog.askstring', return_value='table_amount'), \
             patch('tk.simpledialog.askstring', side_effect=['invalid_choice', 'add']), \
             patch('tk.messagebox.showerror') as mock_showerror:
            reservations_instance.update_reservation(123)
            mock_showerror.assert_called_once_with("Error", "Invalid choice. Please choose 'add' or 'remove'.")

    def test_invalid_table_amount_input(self):
        reservations_instance = Reservations(self.mock_master, self.mock_engine)
        with patch('tk.simpledialog.askstring', return_value='table_amount'), \
             patch('tk.simpledialog.askstring', side_effect=['add', 'invalid_value', 'add', '5']), \
             patch('tk.messagebox.showerror') as mock_showerror:
            reservations_instance.update_reservation(123)
            mock_showerror.assert_called_once_with("Error", "Invalid input. Please enter a valid numeric value.")

    def test_show_first_window(self):
        reservations_instance = Reservations(self.mock_master,self.mock_engine)
        with patch.object(reservations_instance.master, 'deiconify'), \
             patch.object(reservations_instance.master, 'title') as mock_title, \
             patch.object(reservations_instance.master, 'minsize'), \
             patch.object(reservations_instance.master, 'resizable'), \
             patch.object(reservations_instance, 'center_window'):
            reservations_instance.show_first_window()
            mock_title.assert_called_once_with("RESERVATIONS")

if __name__ == '__main__':
    # Configure logging level to suppress INFO messages
    logging.basicConfig(level=logging.WARNING)

    # Run the tests
    unittest.main()
