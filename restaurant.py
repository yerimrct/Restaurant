import tkinter as tk
from tkinter import *
import tkcalendar
from tkinter import messagebox, simpledialog, ttk, Toplevel
from tkcalendar import DateEntry
import sqlite3
import datetime as dt
from datetime import datetime, date
from dateutil.parser import parse
import sqlalchemy
from sqlalchemy.orm import Session, sessionmaker, relationship, aliased
from sqlalchemy import distinct, update, exists, create_engine, inspect, insert, Column, Integer, String, Float, Date, ForeignKey, CheckConstraint
from sqlalchemy.ext.declarative import declarative_base
import re
import hashlib
from PIL import Image, ImageTk
from enum import Enum
from sqlalchemy.sql import func
import qrcode
from fpdf import FPDF  


class Role(Enum):
    MANAGER = "manager"
    CHEF = "chef"
    HR_DIRECTOR = "HR Director"
    FRONT_END_STAFF = "front-end staff"
    BACK_END_STAFF = "back-end staff"
    DELIVERY_STAFF = "delivery staff"
    
Base = declarative_base()
engine = create_engine('sqlite:///Restaurant/restaurant.db', echo=True)
# Base.metadata.create_all(engine)

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    first_name = Column(String, nullable=False)
    last_name = Column(String, nullable=False)
    username = Column(String, nullable=False, unique=True)
    password = Column(String, nullable=False)  
    role = Column(String, nullable=False)
    usertype = Column(String, nullable=False)
    branch = Column(String, nullable=False)
    restaurant_location = Column(String, nullable=False)

    def __init__(self, first_name, last_name, username, password, role, usertype, branch, restaurant_location):
        self.first_name = first_name
        self.last_name = last_name
        self.username = username
        self.password = password
        self.role = role
        self.usertype = usertype
        self.branch = branch
        self.restaurant_location = restaurant_location
    

class MenuItem(Base):
    __tablename__ = 'menu'
    id = Column(Integer, primary_key=True)
    item = Column(String, nullable=False)
    price = Column(Float, nullable=False)
    category = Column(String, nullable=False)

    def __init__(self, item, price,category):
        self.item = item
        self.price = price
        self.category = category

class Order(Base):
    __tablename__ = 'orders'
    id = Column(Integer, primary_key=True, autoincrement=True)
    orderinfo = Column(String, nullable=False)
    cost = Column(Float, nullable=False)
    branch = Column(String , nullable=False)
    restaurant= Column(String, nullable=False)

class FoodItem(Base):
    __tablename__ = 'fooditem'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, nullable=False)
    quantity = Column(Integer, nullable=False)
    price  = Column(Float, nullable=False)

class FoodCategory(Base):
    __tablename__ = 'foodcategory'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, nullable=False)
    description = Column(String, nullable=False)

class Table(Base):
    __tablename__ = 'tables'

    id = Column(Integer, primary_key=True)
    branch = Column(String, nullable=False)
    table_amount = Column(Integer, nullable=False)
    reset_date = Column(Date, default=dt.date.today())
    branch_number = Column(Integer, nullable=False)

    __table_args__ = (
        CheckConstraint('table_amount >= 0', name='check_min_table_amount'),
        CheckConstraint('table_amount <= 100', name='check_max_table_amount'),
    )

class Reservation(Base):
    __tablename__ = 'reservations'

    id = Column(Integer, primary_key=True)
    date = Column(Date, nullable=False)
    branch = Column(String, nullable=False)
    table_amount = Column(Integer, nullable=False)
    table_id = Column(Integer, ForeignKey('tables.id'))
    table = relationship('Table', back_populates='reservations')
    branch_number = Column(String, nullable=False)

Table.reservations = relationship('Reservation', order_by=Reservation.id, back_populates='table')

Session = sessionmaker(bind=engine)
session = Session()

#Login class by Yeriim 
class Login:
    def __init__(self, root, show_window=True):
        self.root = root
        self.root.title("HORIZON RESTAURANT")
        self.root.geometry("320x250")
        self.frame = tk.Frame(root)
        self.frame.grid(row=0, column=0, sticky='nsew')
        self.root.resizable(False,False)
        
        self.logged_in_user_id = None

        try:
            logo_image = Image.open("/Users/greatmann/Documents/Py programs/Restaurant/Grey___Green_Elegant_Minimal_Good_Taste_Food_Restaurant_Logo-removebg-preview copy.png")
            
            
        except Exception as e:
            print(f"Error loading logo image: {e}")
            return
        logo_image = logo_image.resize((125, 95), Image.BOX)
        self.logo_image = ImageTk.PhotoImage(logo_image)

        self.canvas = tk.Canvas(self.frame, width=200, height=110)
        self.canvas.grid(row=0, column=0, columnspan=5, rowspan=5, padx=10, pady=10)

        self.canvas.create_image(110, 55, anchor=tk.CENTER, image=self.logo_image)

        tk.Label(self.frame, text="Username:").grid(row=5, column=0)
        tk.Label(self.frame, text="Password:").grid(row=6, column=0)

        self.username_entry = tk.Entry(self.frame)
        self.password_entry = tk.Entry(self.frame, show="*")


        self.username_entry.grid(row=5, column=1)
        self.password_entry.grid(row=6, column=1)

        self.engine = create_engine('sqlite:///restaurant.db', echo=True)
        Base.metadata.create_all(self.engine)
        Session = sessionmaker(bind=self.engine)
        self.session = Session()
    

        login_button = tk.Button(self.frame, text="Login", command=self.login)
        login_button.grid(row=7, column=0, pady=10)

        forgotten_password = tk.Button(self.frame, text="forgotten password or username ?", command=self.forgotten_password_or_username)
        forgotten_password.grid(row=7, column=1, pady=10)
        self.center_window(self.root)

        self.root.columnconfigure(0, weight=1)
        self.root.rowconfigure(0, weight=1)

    def center_window(self, window):
        window.update_idletasks()
        requested_width = window.winfo_reqwidth()
        requested_height = window.winfo_reqheight()
        x_position = (window.winfo_screenwidth() - requested_width) // 2
        y_position = (window.winfo_screenheight() - requested_height) // 2

        window.geometry(f"+{x_position}+{y_position}")

    def login(self):
        entered_username = self.username_entry.get().lower() 
        entered_password = self.password_entry.get()

        hashed_password = hashlib.sha256(entered_password.encode('utf-8')).hexdigest()
        print (hashed_password)

        user = self.session.query(User).filter_by(username=entered_username, password=hashed_password).first()

        if user:
            user_role = user.role
            messagebox.showinfo("Login Successful", "Welcome, {}!".format(user.username))
            self.logged_in_user_id = user.id

            self.open_main_window(user_role)

        else:
            messagebox.showerror("Login Failed", "Invalid username or password")
    
    def get_logged_in_user_id(self):
        return self.logged_in_user_id

    def open_main_window(self, user_role):
        self.root.withdraw()

        main_window = tk.Toplevel()
        main_window.protocol("WM_DELETE_WINDOW", lambda: self.main_window_close(main_window))

        try:
            logo_image = Image.open("/Users/greatmann/Documents/Py programs/Restaurant/Grey___Green_Elegant_Minimal_Good_Taste_Food_Restaurant_Logo-removebg-preview copy.png")
            logo_image = logo_image.resize((125, 95), Image.BOX)
            logo_image_tk = ImageTk.PhotoImage(logo_image)

            canvas = tk.Canvas(main_window, width=200, height=110)
            canvas.grid(row=0, column=0, columnspan=5, rowspan=5, padx=10, pady=10)
            canvas.create_image(100, 55, anchor=tk.CENTER, image=logo_image_tk)
        
            main_window.logo_image_tk = logo_image_tk
        except Exception as e:
            print(f"Error loading logo image: {e}")
        
        logged_in_user_id = self.get_logged_in_user_id()
        print (logged_in_user_id)

        MainWindow(main_window, user_role, logged_in_user_id)
        

    def main_window_close(self, main_window):
        self.root.deiconify() 
        main_window.destroy()
        self.reset_login_window(self.root)

    def reset_login_window(self, window):

        self.username_entry.delete(0, tk.END)
        self.password_entry.delete(0, tk.END)
    
    def forgotten_password_or_username(self):
        password_or_username = simpledialog.askstring("Forgotten Password", "Have you forgotten your password or username?")
        
        if password_or_username == "password":
            username = simpledialog.askstring("Forgotten Password", "Enter your username:")
       
            if username:
                user = self.session.query(User).filter_by(username=username).first()
            
                if user:
                    self.reset_password_window(user)
                else:
                    messagebox.showerror("Error", "Username not found.")
            else:
                return False
            
        elif password_or_username == "PASSWORD":
            username = simpledialog.askstring("Forgotten Password", "Enter your username:")
       
            if username:
                user = self.session.query(User).filter_by(username=username).first()
            
                if user:
                    self.reset_password_window(user)
                else:
                    messagebox.showerror("Error", "Username not found.")
            else:
                return False

        elif password_or_username == "username":
            id  = simpledialog.askinteger("Forgotten username", "Enter your ID:")
            if id:
                user = self.session.query(User).filter_by(id=id).first()

                if user:
                    self.retrieve_username_window(user)
                else:
                    messagebox.showerror("Error", "ID not found.")
            else:
                return False
            
        elif password_or_username ==  "USERNAME":
            id  = simpledialog.askinteger("Forgotten username", "Enter your ID:")
            if id:
                user = self.session.query(User).filter_by(id=id).first()

                if user:
                    self.retrieve_username_window(user)
                else:
                    messagebox.showerror("Error", "ID not found.")
        else:
            messagebox.showerror("Error", "Invalid input")
            return False

    
    def reset_password_window(self, user):
        self.root.iconify()

        reset_password_window = tk.Toplevel(self.root)
        reset_password_window.title("PASSWORD RESET")
        self.center_window(reset_password_window)

        tk.Label(reset_password_window, text=f"Resetting password for {user.username}").pack(pady=10)

        new_password_label = tk.Label(reset_password_window, text="Enter new password:")
        new_password_label.pack()

        new_password_entry = tk.Entry(reset_password_window)
        new_password_entry.pack()

        confirm_button = tk.Button(reset_password_window, text="Reset Password", command=lambda: self.confirm_reset_password(user, new_password_entry.get(), reset_password_window))
        confirm_button.pack(pady=10)

        reset_password_window.protocol("WM_DELETE_WINDOW", lambda: self.show_first_window(reset_password_window))


    def confirm_reset_password(self, user, new_password, window):
        if not self.is_valid_password(new_password):
            return

        hashed_new_password = hashlib.sha256(new_password.encode('utf-8')).hexdigest()

        user.password = hashed_new_password
        self.session.commit()

        messagebox.showinfo("Password Reset", "Password reset successful!")
        window.destroy()

        self.root.deiconify()

    def is_valid_password(self, password):
        if len(password) < 8:
            messagebox.showinfo("Length", "Password must be at least 8 characters long")
            return False
        else:
            return True
       
    def retrieve_username_window(self, user):
        self.root.iconify()
        retrieve_username_window = tk.Toplevel(self.root)
        retrieve_username_window.title("Username Retrieval")
        retrieve_username_window.geometry("150x150")
        self.center_window(retrieve_username_window )

        tk.Label(retrieve_username_window, text="Your username is:").pack(pady=10)
        tk.Label(retrieve_username_window, text=user.username).pack()

        confirm_button = tk.Button(retrieve_username_window, text="Finish", command= lambda:self.deiconify_and_destroy(retrieve_username_window))
        confirm_button.pack(pady=10)
        retrieve_username_window.protocol("WM_DELETE_WINDOW", lambda: self.show_first_window(retrieve_username_window))

    def deiconify_and_destroy(self,window):
        window.destroy()
        self.root.deiconify()
    
    def show_first_window(self, window):
        window.destroy()
        self.root.deiconify()  
        self.center_window(self.root)

#MainWindow class by Jubril Shittu
class MainWindow:
    def __init__(self, master,  user_role, login_user_id ):
        self.master = master
        self.master.title("HORIZON RESTAURANT")
        self.master.resizable(False, False)
        self.center_window(self.master)
        self.frame = tk.Frame(master)
        self.frame.grid(row=0, column=0, sticky='nsew')
        self.login_user  = login_user_id

        qr_code_url = "http://192.168.1.6:8000/"  
        
        if user_role != Role.BACK_END_STAFF.value and user_role != Role.DELIVERY_STAFF.value:
            qr_code_url = "http://192.168.1.6:8000/"
            self.qr_code_image = self.generate_qr_code(qr_code_url)
            self.qr_code_label = tk.Label(master, image=self.qr_code_image)
            self.qr_code_label.grid(row=0, column=1)

        if user_role == Role.MANAGER.value:
            self.show_all_buttons()
        elif user_role == Role.FRONT_END_STAFF.value:
            self.show_front_end_buttons()
        elif user_role == Role.BACK_END_STAFF.value:
            self.show_back_end_buttons()
        elif user_role == Role.DELIVERY_STAFF.value:
            self.show_delivery_staff_buttons()
        elif user_role == Role.CHEF.value:
            self.show_chef_buttons()
        elif user_role == Role.HR_DIRECTOR.value:
            self.show_all_buttons()
    

    def generate_qr_code(self, url, size=(200, 200)):
        qr = qrcode.QRCode(version=1, error_correction=qrcode.constants.ERROR_CORRECT_L, box_size=10, border=4)
        qr.add_data(url)
        qr.make(fit=True)
        qr_img = qr.make_image(fill_color="black", back_color="white")
        qr_img = qr_img.resize(size, Image.LANCZOS)  
        qr_tk = ImageTk.PhotoImage(qr_img)
        return qr_tk


    
    def show_all_buttons(self):
        try:
            logo_image = Image.open("/Users/greatmann/Documents/Py programs/Restaurant/Grey___Green_Elegant_Minimal_Good_Taste_Food_Restaurant_Logo-removebg-preview copy.png")

        except Exception as e:
            print(f"Error loading logo image: {e}")
            return
        logo_image = logo_image.resize((125, 95), Image.BOX)
        self.logo_image = ImageTk.PhotoImage(logo_image)

        # Create a Canvas and display the logo
        self.canvas = tk.Canvas(self.frame, width=210, height=110)
        self.canvas.grid(row=0, column=0, columnspan=6, rowspan=5, padx=10, pady=10)

        # Place the logo at the desired position on the Canvas
        self.canvas.create_image(150, 55, anchor=tk.CENTER, image=self.logo_image)

        self.button = tk.Button(self.frame, text="MENU", command=self.open_window(App,width=300, height=475, login_user_id=self.login_user ))
        self.button.grid(row=5, column=1, pady=10)

        # self.button = tk.Button(self.master, text="MENU", command=self.open_menu_window)
        # self.button.grid(row=2, column=0, pady=10)

        self.button2 = tk.Button(self.frame, text="ORDER", command=self.open_window(OrderManagementApp, width = 800, height = 275, login_user_id=self.login_user))
        self.button2.grid(row=5, column=2, pady=10)

        self.button5 = tk.Button(self.frame, text="USERS", command=self.open_window(UserManagement, width=500, height=540, login_user_id=self.login_user))
        self.button5.grid(row=5, column=0, pady=10)

        self.button3 = tk.Button(self.frame, text="INVENTORY", command=self.open_window(Inventory, width = 300, height = 100, login_user_id=self.login_user))
        self.button3.grid(row=5, column=3, pady=10)

        self.button4 = tk.Button(self.frame, text="RESERVATIONS", command=self.open_window(Reservations, width=500, height=270, login_user_id=self.login_user))
        self.button4.grid(row=5, column=4, pady=10)

        self.button6 = tk.Button(self.frame, text="REPORT", command=self.open_window(Report, width=200, height=50, login_user_id=self.login_user))
        self.button6.grid(row=5, column=5, pady=10)

        self.button6 = tk.Button(self.frame, text="OTHERS", command=self.open_window(Others, width=200, height=50, login_user_id=self.login_user))
        self.button6.grid(row=5, column=6, pady=10)

        logout_button = tk.Button(self.frame, text="Logout", command=self.logout)
        logout_button.grid(row=6, column=0,columnspan = 7, pady=2 )

        self.center_window(self.master)
    
        self.master.columnconfigure(0, weight=1)
        self.master.rowconfigure(0, weight=1)
    
    def show_chef_buttons(self):
        try:
            logo_image = Image.open("/Users/greatmann/Documents/Py programs/Restaurant/Grey___Green_Elegant_Minimal_Good_Taste_Food_Restaurant_Logo-removebg-preview copy.png")
            
        except Exception as e:
            print(f"Error loading logo image: {e}")
            return
        logo_image = logo_image.resize((125, 95), Image.BOX)
        self.logo_image = ImageTk.PhotoImage(logo_image)

        # Create a Canvas and display the logo
        self.canvas = tk.Canvas(self.frame, width=200, height=110)
        self.canvas.grid(row=0, column=0, columnspan=5, rowspan=5, padx=10, pady=10)

        # Place the logo at the desired position on the Canvas
        self.canvas.create_image(100, 55, anchor=tk.CENTER, image=self.logo_image)

        self.button = tk.Button(self.frame, text="MENU", command=self.open_window(App,width=300, height=475, login_user_id=self.login_user ))
        self.button.grid(row=5, column=1, pady=10)

        # self.button = tk.Button(self.master, text="MENU", command=self.open_menu_window)
        # self.button.grid(row=2, column=0, pady=10)

        self.button2 = tk.Button(self.frame, text="ORDER", command=self.open_window(OrderManagementApp, width = 800, height = 275, login_user_id=self.login_user))
        self.button2.grid(row=5, column=2, pady=10)

        self.button3 = tk.Button(self.frame, text="INVENTORY", command=self.open_window(Inventory, width = 300, height = 100, login_user_id=self.login_user))
        self.button3.grid(row=5, column=3, pady=10)

        logout_button = tk.Button(self.frame, text="Logout", command=self.logout)
        logout_button.grid(row=6, column=0,columnspan = 6, pady=2 )

        self.center_window(self.master)
    
        self.master.columnconfigure(0, weight=1)
        self.master.rowconfigure(0, weight=1)


    def show_front_end_buttons(self):
        try:
            logo_image = Image.open("/Users/greatmann/Documents/Py programs/Restaurant/Grey___Green_Elegant_Minimal_Good_Taste_Food_Restaurant_Logo-removebg-preview copy.png")
            
        except Exception as e:
            print(f"Error loading logo image: {e}")
            return
        logo_image = logo_image.resize((125, 95), Image.BOX)
        self.logo_image = ImageTk.PhotoImage(logo_image)

        self.canvas = tk.Canvas(self.frame, width=200, height=110)
        self.canvas.grid(row=0, column=0, columnspan=5, rowspan=5, padx=10, pady=10)

        self.canvas.create_image(100, 55, anchor=tk.CENTER, image=self.logo_image)

        self.button2 = tk.Button(self.frame, text="ORDER", command=self.open_window(OrderManagementApp, width = 800, height = 275, login_user_id=self.login_user))
        self.button2.grid(row=5, column=3, pady=10)

        self.button4 = tk.Button(self.frame, text="RESERVATIONS", command=self.open_window(Reservations, width=500, height=270, login_user_id=self.login_user))
        self.button4.grid(row=5, column=4, pady=10)

        logout_button = tk.Button(self.frame, text="Logout", command=self.logout)
        logout_button.grid(row=6, column=0,columnspan = 6, pady=2 )

        self.center_window(self.master)
    
    def show_back_end_buttons(self):
        try:
            logo_image = Image.open("/Users/greatmann/Documents/Py programs/Restaurant/Grey___Green_Elegant_Minimal_Good_Taste_Food_Restaurant_Logo-removebg-preview copy.png")
            
        except Exception as e:
            print(f"Error loading logo image: {e}")
            return
        logo_image = logo_image.resize((125, 95), Image.BOX)
        self.logo_image = ImageTk.PhotoImage(logo_image)

        self.canvas = tk.Canvas(self.frame, width=200, height=110)
        self.canvas.grid(row=0, column=0, columnspan=5, rowspan=5, padx=10, pady=10)

        self.canvas.create_image(100, 55, anchor=tk.CENTER, image=self.logo_image)

        self.button3 = tk.Button(self.frame, text="INVENTORY", command=self.open_window(Inventory, width = 300, height = 100, login_user_id=self.login_user))
        self.button3.grid(row=5, column=3, pady=10)

        self.button2 = tk.Button(self.frame, text="ORDER", command=self.open_window(OrderManagementApp, width = 800, height = 275, login_user_id=self.login_user))
        self.button2.grid(row=5,column=4, padx = 10, pady=10)

        logout_button = tk.Button(self.frame, text="Logout", command=self.logout)
        logout_button.grid(row=6, column=0,columnspan = 6, pady=2 )

        self.center_window(self.master)
    
    def show_delivery_staff_buttons(self):
        try:
            logo_image = Image.open("/Users/greatmann/Documents/Py programs/Restaurant/Grey___Green_Elegant_Minimal_Good_Taste_Food_Restaurant_Logo-removebg-preview copy.png")
            
        except Exception as e:
            print(f"Error loading logo image: {e}")
            return
        logo_image = logo_image.resize((125, 95), Image.BOX)
        self.logo_image = ImageTk.PhotoImage(logo_image)

        self.canvas = tk.Canvas(self.frame, width=200, height=110)
        self.canvas.grid(row=0, column=0, columnspan=5, rowspan=5, padx=10, pady=10)

        self.canvas.create_image(100, 55, anchor=tk.CENTER, image=self.logo_image)

        self.button2 = tk.Button(self.frame, text="ORDER", command=self.open_window(OrderManagementApp, width = 800, height = 275, login_user_id=self.login_user))
        self.button2.grid(row=5, column=0,columnspan = 6, pady=10)

        logout_button = tk.Button(self.frame, text="Logout", command=self.logout)
        logout_button.grid(row=6, column=0,columnspan = 6, pady=2 )

        self.center_window(self.master)

    
    def logout(self): 
        self.master.destroy()
        login_window = tk.Toplevel()
        login_instance = Login(login_window)
        

    def center_window(self, window):      
        window.update_idletasks()

        requested_width = window.winfo_reqwidth()
        requested_height = window.winfo_reqheight()

        # Calculating the centered position, taking the window size into account
        x_position = (window.winfo_screenwidth() - requested_width) // 2
        y_position = (window.winfo_screenheight() - requested_height) // 2

        # Setting the window position
        window.geometry(f"+{x_position}+{y_position}")

    # def open_menu_window(self):
    #     print("Button clicked")
    #     self.open_window(App,width=300, height=475)


    def open_window(self, window_class, width, height, login_user_id):
        def command():
            if self.master.winfo_exists():
            # Iconify the main window
                self.master.iconify()

            # Create a Toplevel window
                self.app_instance = window_class(tk.Toplevel(self.master), login_user_id )
                self.center_window(self.app_instance.root)

            # Set the size of the Toplevel window
                self.app_instance.root.geometry(f"{width}x{height}")

            # Handle the close event of the Toplevel window
                self.app_instance.root.protocol("WM_DELETE_WINDOW", self.on_window_close)
                self.app_instance.root.resizable(False, False)

            # Wait for the Toplevel window to be closed
                self.app_instance.root.wait_window()

            # Deiconify the main window when the Toplevel window is closed
                if self.master.winfo_exists():
                    self.master.deiconify()
                else:
                    print("Main window does not exist.")

        return command

    def on_window_close(self):
        if self.app_instance:
            self.app_instance.root.destroy()  # Destroy the current window
            self.app_instance = None


#UserManagement class by Oussama Fillali
class UserManagement:
    def __init__(self, root, logged_in_user_id):
        print("UserManagement instance created")
        self.root = root
        self.frame = tk.Frame(root)
        self.frame.grid(row=0, column=0, sticky='nsew')
        self.root.title("USERS")
        self.root.geometry("500x1000")


        self.user_Session = sessionmaker(bind=engine)

        logged_in_user = self.get_user_by_id(logged_in_user_id)
        self.user_role = logged_in_user.role
        self.user_branch = logged_in_user.branch
        self.user_restaurant = logged_in_user.restaurant_location
        self.user_id = logged_in_user.id

        self.label = tk.Label(self.frame, text="Users:")
        self.label.pack()

        self.searchLabel = tk.Label(self.frame, text="Search by ID:")
        self.searchLabel.pack()

        self.searchEntry = tk.Entry(self.frame)
        self.searchEntry.pack()

        self.searchButton = tk.Button(self.frame, text="Search", command=self.search_user)
        self.searchButton.pack()

        self.refreshButton = tk.Button(self.frame, text="Refresh list/text box", command=lambda:self.update_listbox(logged_in_user_id))
        self.refreshButton.pack()

        self.listbox = tk.Listbox(self.frame, selectmode=tk.SINGLE, exportselection=False)
        self.listbox.pack()
        self.listbox.bind('<<ListboxSelect>>', self.display_details)

        self.addButton = tk.Button(self.frame, text="Add User", command=self.input_user)
        self.addButton.pack()

        self.removeButton = tk.Button(self.frame, text="Remove User", command=self.remove_user)
        self.removeButton.pack()

        self.updateButton = tk.Button(self.frame, text="Update User", command=self.update_user)
        self.updateButton.pack()

        self.showAllButton = tk.Button(self.frame, text="Show All Users", command=self.show_all_users)
        self.showAllButton.pack()

        self.txt_details = tk.Text(self.frame, width=50, height=9)
        self.txt_details.pack()
        self.center_window(self.root)

        self.usertype_options = ['admin', 'standard']
        self.usertype = tk.StringVar()
        self.usertype_combobox = ttk.Combobox(self.frame, textvariable=self.usertype, values=self.usertype_options, state='readonly')

        self.role_options_1 = ['chef', 'manager', 'HR Director']
        self.role_1 = tk.StringVar()
        self.role_combobox_1 = ttk.Combobox(self.frame, textvariable=self.role_1, values=self.role_options_1, state='readonly')

        self.role_options_2 = ['front-end staff', 'back-end staff', 'delivery staff']
        self.role_2 = tk.StringVar()
        self.role_combobox_2 = ttk.Combobox(self.frame, textvariable=self.role_2, values=self.role_options_2, state='readonly')

        self.branch_option = ['Bristol', 'London', 'Birmingham', 'Manchester', 'Nottingham', 'Glasgow', 'Cardiff']
        self.branch = tk.StringVar()
        self.branch_combobox = ttk.Combobox(self.frame, textvariable=self.branch, values=self.branch_option, state='readonly')

        # restaurant_number is also restaurant_location
        self.restaurant_number_options = ['restaurant 1', 'restaurant 2']
        self.restaurant_number = tk.StringVar()
        self.restaurant_number_combobox = ttk.Combobox(
            self.frame, textvariable=self.restaurant_number, values=self.restaurant_number_options, state='readonly'
        )

        self.logged_in_user_id = logged_in_user_id

        self.update_listbox(logged_in_user_id)

        self.root.columnconfigure(0, weight=1)
        self.root.rowconfigure(0, weight=1)


    def hash_password(self, password):
        # Hash the password using SHA-256 (you can choose a different hash function if needed)
        hash_object = hashlib.sha256(password.encode())
        return hash_object.hexdigest()

    def center_window(self, window):
        window.update_idletasks()
        screen_width = window.winfo_screenwidth()
        screen_height = window.winfo_screenheight()
        requested_width = window.winfo_reqwidth()
        requested_height = window.winfo_reqheight()
        x_position = (screen_width - requested_width) // 2
        y_position = (screen_height - requested_height) // 2
        window.geometry(f"+{x_position}+{y_position}")

    def input_user(self):
            role = None
            usertype = None
            branch = None
            restaurant = None
            session = self.user_Session()

            while True:
                first_name = simpledialog.askstring("Input", "Enter first name:")
                if first_name is None:
                    messagebox.showinfo("Error", "Firstname not inputted. User creation canceled.")
                    return False
                elif not first_name.isalpha():
                    messagebox.showinfo("Error", "First name should contain only letters.")
                    continue  
                else:
                    break  

            while True:
                last_name = simpledialog.askstring("Input", "Enter last name:")
                if last_name is None:
                    messagebox.showinfo("Error", "Lastname not inputted. User creation canceled")
                    return False
                elif not last_name.isalpha():
                    messagebox.showinfo("Error", "Last name should contain only letters.")
                    continue  
                else:
                    break

            while True:
                username = simpledialog.askstring("Input", "Enter username:")
                username = username.lower() 
                if username is None:
                    messagebox.showinfo("Error", "Username not inputted. User creation canceled.")
                    return False
                elif session.query(User).filter_by(username=username).first():
                    messagebox.showinfo("Error", "Username already exists. Please choose a different username.")
                    continue
                else:
                    break

            while True:
                usertype = self.dropdown(self.usertype_combobox, "Select usertype:")
                if usertype is None:
                    messagebox.showinfo("Error", "Usertype not selected. User creation canceled.")
                    return False
                else:
                    break  

            while True:
                if usertype == "standard":
                    role = self.dropdown(self.role_combobox_2, "Select role:")
                elif usertype == "admin":
                    role = self.dropdown(self.role_combobox_1, "Select role:")

                if role is None:
                    messagebox.showinfo("Error", "Role not selected. User creation canceled.")
                    return False
                else:
                    break  

            while True:
                branch = self.dropdown(self.branch_combobox, "Select branch:")
                if branch is None:
                    messagebox.showinfo("Error", "Branch not selected. User creation canceled.")
                    return False
                else:
                    break  

            while True:
                restaurant = self.dropdown(self.restaurant_number_combobox, "Select restaurant:")
                if restaurant is None:
                    messagebox.showinfo("Error", "Restaurant not selected. User creation canceled.") 
                    return False  
                else:
                    break
            while True:
                password = simpledialog.askstring("Input", "Enter password:")
                if password is None:
                    messagebox.showinfo("Error", "Password not inputted. User creation canceled.")
                    return False
                elif self.is_valid_password(password):
                    break  

            hashed_password = self.hash_password(password)

            new_user = User(first_name, last_name, username, hashed_password, role, usertype, branch, restaurant)
            self.add_user(new_user)
            self.update_listbox(self.user_id)
            messagebox.showinfo("Successful", "User added successfully")

    def is_valid_password(self, password):
        if len(password) < 8:
            messagebox.showinfo("Length", "Password must be at least 8 characters long")
            return False     
        else:
            return True


    def add_user(self, user):
        session = self.user_Session()
        session.add(user)
        session.commit()
        session.close()
    
    def search_user(self):
        if self.user_role == "HR Director":
            user_id = self.searchEntry.get()

            try:
                user_id = int(user_id)
            except ValueError:
                messagebox.showinfo("Error", "Please enter a valid user ID.")
                return

            # Get the user by ID
            user = self.get_user_by_id(user_id)

            if user:
        # Clear the listbox before displaying detail
                self.txt_details.config(state=tk.NORMAL)
                self.txt_details.delete(1.0, tk.END)

        # Check if the user is the currently logged-in user
                if user.id == self.logged_in_user_id:
                    self.txt_details.insert(tk.END, "You are the currently logged-in user.")
                else:
                    info_2 = f"Name: {user.first_name} {user.last_name}\nID: {user.id}\nUsername: {user.username} \nRole: {user.role}\nUsertype: {user.usertype}\nBranch: {user.branch}\nRestaurant location: {user.restaurant_location} "
                    self.txt_details.insert(tk.END, info_2)
                    self.listbox.delete(0, tk.END)
                    self.listbox.insert(tk.END, f"{user.first_name} {user.last_name}")
                
                self.txt_details.config(state=tk.DISABLED)
            else:
                messagebox.showinfo("Error", f"No user found with ID {user_id}.")
        else:
            user_id = self.searchEntry.get()

            try:
                user_id = int(user_id)
            except ValueError:
                messagebox.showinfo("Error", "Please enter a valid user ID.")
                return

            # Get the user by ID
            user = self.get_user_by_id(user_id)

            if user:
        # Clear the listbox before displaying detail
                self.txt_details.config(state=tk.NORMAL)
                self.txt_details.delete(1.0, tk.END)

        # Check if the user is the currently logged-in user
                if user.id == self.logged_in_user_id:
                    self.txt_details.insert(tk.END, "You are the currently logged-in user.")
                else:
                    if user.branch == self.user_branch and user.restaurant_location == self.user_restaurant:
                        info_2 = f"Name: {user.first_name} {user.last_name}\nID: {user.id}\nUsername: {user.username} \nRole: {user.role}\nUsertype: {user.usertype}\nBranch: {user.branch}\nRestaurant location: {user.restaurant_location} "
                        self.txt_details.insert(tk.END, info_2)
                        self.listbox.delete(0, tk.END)
                        self.listbox.insert(tk.END, f"{user.first_name} {user.last_name}")
                    else:
                        self.txt_details.insert(tk.END, "User not in your branch.")
                
                    self.txt_details.config(state=tk.DISABLED)
            else:
                messagebox.showinfo("Error", f"No user found with ID {user_id}.")


    
    def get_user_by_id(self, user_id):
        session = self.user_Session()
        user = session.query(User).filter_by(id=user_id).first()
        session.close()
        return user

    def remove_user(self):
        selected_index = self.listbox.curselection()
        if selected_index:
            selected_user = self.get_users(None, None, None, None)[selected_index[0]]
            confirmation = messagebox.askyesno("Confirmation", f"Are you sure you want to remove user?")
            if confirmation:
                self.remove_user_from_db(selected_user)
                messagebox.showinfo("Successful", "User successfully removed")
                self.update_listbox()
            else:
                messagebox.showinfo("Canceled", "Canceled.")
        else:
            messagebox.showinfo("Error", "Please select a user to remove.")

    def remove_user_from_db(self, user):
        session = self.user_Session()
        session.delete(user)
        session.commit()
        session.close()

    def update_user(self):
        usertype = None
        role = None
        password = None
        branch = None
        restaurant = None
        first_name = None
        last_name  = None
        username  = None

        selected_index = self.listbox.curselection()
        if not selected_index:
            messagebox.showinfo("No selection", "Please select a user to update")
            return False 
        print (selected_index)

        user_id_minus_one = (int(self.user_id) - 1)
        if user_id_minus_one != selected_index[0]:
            selected_user = self.get_users(None, None, None, None)[selected_index[0]]
            valid_columns = ['first_name', 'last_name', 'username', 'password', 'role', 'usertype', 'branch', 'restaurant_location']

            column_var = tk.StringVar()

            column_dropdown = ttk.Combobox(self.frame, textvariable=column_var, values=valid_columns, state=tk.DISABLED)
            column_dropdown.pack(pady = 15)

            selected_column = self.dropdown(column_dropdown, "Choose:")

            session = self.user_Session()
            if selected_column:

                if selected_column == "usertype":
                # If updating 'usertype', use the dropdown list
                        while True:
                            usertype= self.dropdown(self.usertype_combobox, "Select usertype:")
                            if usertype == "":
                                messagebox.showinfo("Error", "Usertype not selected. User update canceled.")
                                return False
                            else:
                                new_value = usertype
                                break  
                          
                elif selected_column == "role":
                        usertype = selected_user.usertype
                        while True:
                            if usertype == "standard":
                                role = self.dropdown(self.role_combobox_2, "Select role:")
                            elif usertype == "admin":
                                role = self.dropdown(self.role_combobox_1, "Select role:")

                            if role == "":
                                messagebox.showinfo("Error", "Role not selected. User update canceled.")
                                return False
                            else:
                                new_value = role
                                break

                elif selected_column == "password":
                        while True:
                            password = simpledialog.askstring("Input", "Enter new password:")
                            if password == "":
                                messagebox.showinfo("Error", "Password not inputted. User update canceled.")
                                return False
                            elif self.is_valid_password(password):
                                new_value = self.hash_password(password)
                                break 

                elif selected_column  == "branch":
                        while True:
                            branch = self.dropdown(self.branch_combobox, "Select branch:")
                            if branch == "":
                                messagebox.showinfo("Error", "Branch not selected. User update canceled.")
                                return False
                            else:
                                new_value = branch
                                break 
                    
                elif selected_column  == "restaurant_location": 
                        while True:
                            restaurant = self.dropdown(self.restaurant_number_combobox, "Select restaurant:")
                            if restaurant == "":
                                messagebox.showinfo("Error", "Restaurant not selected. User update canceled.") 
                                return False  
                            else:
                                new_value = restaurant
                                break
                
                elif selected_column  == "first_name": 
                        while True:
                            first_name = simpledialog.askstring("Input", "Enter new first-name:")
                            if first_name == "":
                                messagebox.showinfo("Error", "User update canceled.") 
                                return False  
                            else:
                                new_value = first_name
                                break
                
                elif selected_column  == "last_name": 
                        while True:
                            last_name = simpledialog.askstring("Input", "Enter new last-name:")
                            if last_name  == "":
                                messagebox.showinfo("Error", "User update canceled.") 
                                return False  
                            else:
                                new_value = last_name 
                                break
                elif selected_column  == "username": 
                        while True:
                            username = simpledialog.askstring("Input", "Enter new username:")
                            username = username.lower()
                            if username  == "":
                                messagebox.showinfo("Error", "User update canceled.") 
                                return False  
                            elif session.query(User).filter_by(username=username).first():
                                messagebox.showinfo("Error", "Username already exists. Please choose a different username.")
                                continue
                            else:
                                new_value = username 
                                break
                else:
                        while True:
                            new_value = simpledialog.askstring("Update", f"Enter new value for {selected_column}:")
                            if new_value == "":
                                return False
                            elif not new_value.strip():  # Check if the entered value is an empty string
                                messagebox.showinfo("Error", "New value cannot be empty.")
                            else:
                                break 
            else:
                return False

            print(f"Updating {selected_column} to {new_value} for user {selected_user.username}")
            self.update(selected_user, selected_column, new_value)
            messagebox.showinfo("Successful", "Update successful")
            self.update_listbox(self.user_id)
        else:
            messagebox.showinfo("Error", "User logged in cannot update their profile.")
            return False
    
    def dropdown(self, combobox, prompt):
        dialog = tk.Toplevel(self.root)
        dialog.title("Select Value")
        dialog.geometry("300x150")
        self.center_window(dialog)
        dialog.resizable(False, False)

        label = tk.Label(dialog, text=prompt)
        label.pack()

        selected_value = tk.StringVar()
        combobox = ttk.Combobox(dialog, textvariable=selected_value, values=combobox['values'], state = 'readonly')
        combobox.pack(pady=10)
        

        def on_ok():
            dialog.destroy()

        ok_button = tk.Button(dialog, text="OK", command=on_ok)
        ok_button.pack()

        def on_close():
            messagebox.showinfo("Cancelled", "Update cancelled")
            dialog.destroy()

        dialog.protocol("WM_DELETE_WINDOW", on_close)

        dialog.wait_window(dialog)

        return selected_value.get()

    
    def update(self, user, column_to_update, new_value):
        session = self.user_Session()
        existing_user = session.query(User).get(user.id)

        if existing_user:
            # Update the selected column with the new value
            setattr(existing_user, column_to_update, new_value)
            session.commit()
        session.close()


    def show_all_users(self):

        if self.user_role == "HR Director":
            self.txt_details.config(state=tk.NORMAL)
            all_users = self.get_users(None, None, None, None)
            users_by_role = {}

    # Group users by role
            for user in all_users:
                role = user.role
                if role not in users_by_role:
                    users_by_role[role] = []
                users_by_role[role].append(user)

            info = ""
            for role, users in users_by_role.items():
                info += f"{role}:\n"
                for user in users:
                    info += f"  Name: {user.first_name} {user.last_name}    ID: {user.id}\n"

            self.txt_details.delete(1.0, tk.END)
            self.txt_details.insert(tk.END, info)
            self.txt_details.config(state=tk.DISABLED)
        else:
            self.txt_details.config(state=tk.NORMAL)
            all_users = self.get_users(None, None, self.user_branch, self.user_restaurant )
            users_by_role = {}

    # Group users by role
            for user in all_users:
                role = user.role
                if role not in users_by_role:
                    users_by_role[role] = []
                users_by_role[role].append(user)

            info = ""
            for role, users in users_by_role.items():
                info += f"{role}:\n"
                for user in users:
                    info += f"  Name: {user.first_name} {user.last_name}    ID: {user.id}\n"

            self.txt_details.delete(1.0, tk.END)
            self.txt_details.insert(tk.END, info)
            self.txt_details.config(state=tk.DISABLED)

    def get_users(self, filter_first_name, filter_last_name, branch , restaurant_location ):
        session = self.user_Session()
    
        if filter_first_name and filter_last_name:
        # If both first and last names are provided, filter by both
            users = session.query(User).filter_by(first_name=filter_first_name, last_name=filter_last_name).all()
        elif filter_first_name:
        # If only first name is provided, filter by first name
            users = session.query(User).filter_by(first_name=filter_first_name).all()
        elif filter_last_name:
        # If only last name is provided, filter by last name
            users = session.query(User).filter_by(last_name=filter_last_name).all()
        elif branch and restaurant_location:
            users = session.query(User).filter_by(branch=branch, restaurant_location=restaurant_location).all()
        elif branch:
            users = session.query(User).filter_by(branch=branch).all()
        elif restaurant_location:
            users = session.query(User).filter_by(restaurant_location=restaurant_location).all()
        else:
        # If no filters are provided, retrieve all users
            users = session.query(User).all()

        session.close()
        return users
    
    def get_users_by_first_and_last_name(self, filter_first_name, filter_last_name ):
        session = self.user_Session()
    
        if filter_first_name and filter_last_name:
        # If both first and last names are provided, filter by both
            users = session.query(User).filter_by(first_name=filter_first_name, last_name=filter_last_name).all()
        elif filter_first_name:
        # If only first name is provided, filter by first name
            users = session.query(User).filter_by(first_name=filter_first_name).all()
        elif filter_last_name:
        # If only last name is provided, filter by last name
            users = session.query(User).filter_by(last_name=filter_last_name).all()
        else:
        # If no filters are provided, retrieve all users
            users = session.query(User).all()

        session.close()
        return users

    def update_listbox(self, logged_in_user_id=None):

        if self.user_role  == "HR Director":
            self.txt_details.config(state=tk.NORMAL)
            self.listbox.delete(0, tk.END)
            for user in self.get_users(None, None, None, None):
        # Insert all users into the listbox
                self.listbox.insert(tk.END, f"{user.first_name} {user.last_name}")
            self.txt_details.config(state=tk.DISABLED)
        else:
            self.txt_details.config(state=tk.NORMAL)
            self.listbox.delete(0, tk.END)
            for user in self.get_users(None, None, self.user_branch, self.user_restaurant):
        # Insert all users into the listbox
                self.listbox.insert(tk.END, f"{user.first_name} {user.last_name}")
            self.txt_details.config(state=tk.DISABLED)
    


    def display_details(self, evnt):
        self.txt_details.config(state=tk.NORMAL)
        selected_index = self.listbox.curselection()

        if selected_index:
            selected_item = self.listbox.get(selected_index[0])

        # Split the selected item into first and last names
            selected_names = selected_item.split(' ')

        # Retrieve user information based on first and last names
            selected_users = self.get_users_by_first_and_last_name( filter_first_name=selected_names[0], filter_last_name=selected_names[1])

            if selected_users:
                info = ""
                for user in selected_users:
                    info += (
                        f"Name: {user.first_name} {user.last_name}\n"
                        f"ID: {user.id}\n"
                        f"Username: {user.username}\n"
                        f"Role: {user.role}\n"
                        f"Usertype: {user.usertype}\n"
                        f"Branch: {user.branch}\n"
                        f"Restaurant location: {user.restaurant_location}\n\n"
                    )

                self.txt_details.delete(1.0, tk.END)
                self.txt_details.insert(tk.END, info.strip())
                self.txt_details.config(state=tk.DISABLED)


#Menu class by Oussama Fillali
class Menu(tk.Toplevel):
    def __init__(self, login_instance = None):
        self.engine = create_engine('sqlite:///restaurant.db', echo=True)
        Base.metadata.create_all(self.engine)
        self.Session = sessionmaker(bind=self.engine)
    
    def get_items(self):
        session = self.Session()
        items = session.query(MenuItem).all()
        session.close()
        return items
    
    def add_item(self, menu_item):
        session = self.Session()
        session.add(menu_item)
        session.commit()
        session.close()

    def remove_item(self, menu_item):
        session = self.Session()
        session.delete(menu_item)
        session.commit()
        session.close()

    def update_item_price(self, menu_item, new_price):
        session = self.Session()
        
        # Query for the item using its primary key
        existing_item = session.query(MenuItem).get(menu_item.id)

        if existing_item:
            existing_item.price = new_price
            session.commit()

        session.close()

#App class by Jubril Shittu
class App:
    def __init__(self, root, login_instance = None):
        self.root = root
        self.root.title("Menu")
        self.root.geometry("500x500")

        self.menu = Menu()

        self.label = tk.Label(self.root, text="Menu Items:")
        self.label.pack()

        self.listbox = tk.Listbox(self.root, selectmode=tk.SINGLE, exportselection=False)
        self.listbox.pack()
        self.listbox.bind('<<ListboxSelect>>', self.display_details)

        self.addButton = tk.Button(self.root, text="Add Item", command=self.input_item)
        self.addButton.pack()

        self.removeButton = tk.Button(self.root, text="Remove Item", command=self.remove_item)
        self.removeButton.pack()

        self.updateButton = tk.Button(self.root, text="Update Price", command=self.update_price)
        self.updateButton.pack()

        self.showAllButton = tk.Button(self.root, text="Show All Items", command=self.show_all_items)
        self.showAllButton.pack()

        self.showMenuButton = tk.Button(self.root, text="Show Menu", command=self.show_menu)
        self.showMenuButton.pack()

        self.txt_details = tk.Text(self.root, width=50, height=10)
        self.txt_details.pack()
        self.center_window(self.root)

        self.update_listbox()


    def center_window(self, window):
        # Update idle tasks to ensure that the window size is up-to-date
        window.update_idletasks()

        # Get the screen width and height
        screen_width = window.winfo_screenwidth()
        screen_height = window.winfo_screenheight()

        # Get the requested width and height of the window
        requested_width = window.winfo_reqwidth()
        requested_height = window.winfo_reqheight()

        # Calculate the centered position, taking the window size into account
        x_position = (screen_width - requested_width) // 2
        y_position = (screen_height - requested_height) // 2

        # Set the window position
        window.geometry(f"+{x_position}+{y_position}")

    def input_item(self):
        item = simpledialog.askstring("Input", "Enter item name:")
        if item:
            price = simpledialog.askfloat("Input", "Enter item price:")
            category = simpledialog.askstring("Input", "Enter item category:")

            new_item = MenuItem(item, price, category)
            self.menu.add_item(new_item)
            messagebox.showinfo("Successfull", "Item successfully added")

            self.update_listbox()

    def remove_item(self):
        selected_index = self.listbox.curselection()
        if selected_index:
            selected_item = self.menu.get_items()[selected_index[0]]
            confirmation = messagebox.askyesno("Confirmation", f"Are you sure you want to remove item?")
            if confirmation:
                self.menu.remove_item(selected_item)
                messagebox.showinfo("Successfull", "Item successfully removed")
                self.update_listbox()
            else:
                messagebox.showinfo("Canceled", "Canceled.")
        else:
            messagebox.showinfo("Error", "Please select an item to remove.")

    def update_price(self):
        selected_index = self.listbox.curselection()
        if selected_index:
            selected_item = self.menu.get_items()[selected_index[0]]
            new_price = simpledialog.askfloat("Update Price", f"Enter new price for {selected_item.item}:")
            if new_price is not None:
                # Call the update_item_price method with both the item and the new price
                self.menu.update_item_price(selected_item, new_price)
                messagebox.showinfo("Successfull", "Price successfully updated")
                self.update_listbox()
        else:
            messagebox.showinfo("Error", "Please select an item to update.")

    def show_all_items(self):
        self.txt_details.config(state=tk.NORMAL)
        all_items = self.menu.get_items()
        details_text = ""
        for item in all_items:
            details_text += f"Name: {item.item}\nPrice: £{item.price:.2f}\n"
        self.txt_details.delete(1.0, tk.END)
        self.txt_details.insert(tk.END, details_text)
        self.txt_details.config(state=tk.DISABLED)

    def show_menu(self):
        all_items = self.menu.get_items()

        # Group items by category
        items_by_category = {}
        for item in all_items:
            category = item.category  # Assuming there is a 'category' attribute in MenuItem class
            if category not in items_by_category:
                items_by_category[category] = []
            items_by_category[category].append(item)

        self.txt_details.config(state=tk.NORMAL)

        details_text = ""
        for category, items in items_by_category.items():
            details_text += f"{category}:\n"
            for item in items:
                details_text += f"  Name: {item.item} - Price: £{item.price:.2f}\n"

        self.txt_details.delete(1.0, tk.END)
        self.txt_details.insert(tk.END, details_text)
        self.txt_details.config(state=tk.DISABLED)

    def update_listbox(self):
        self.listbox.delete(0, tk.END)
        for item in self.menu.get_items():
            self.listbox.insert(tk.END, f"{item.item}")

    def display_details(self, evnt):
        selected_index = self.listbox.curselection()
        if selected_index:
            selected_item = self.menu.get_items()[selected_index[0]]
            details_text = f"Name: {selected_item.item}\nPrice: £{selected_item.price:.2f}\n"
            self.txt_details.delete(1.0, tk.END)
            self.txt_details.insert(tk.END, details_text)

#OrderManagementApp class by Sixtus Ubana
class OrderManagementApp:

    def __init__(self, root, login_instance ):

        self.root = root
        self.root.title("Order Management System")
        self.root.geometry("800x275")
        # self.master.resizable(False,False)
 
        # Connect to SQLite database
        engine = create_engine('sqlite:///restaurant.db', echo=True)
        Base.metadata.create_all(engine)
        Session = sessionmaker(bind=engine)
        self.session = Session()

        # Variables to store order information
        self.order_items = []

        login_user = self.get_user_by_id(login_instance)
        self.user_role  =  login_user.role
        self.user_branch = login_user.branch
        self.user_restaurant = login_user.restaurant_location
        
        self.session.commit()

        # GUI components
        self.menu_label = tk.Label(self.root, text="Menu:")
        self.menu_label.grid(row=0, column=0, padx=10, pady=10, sticky=tk.W)

        self.menu_textbox = tk.Text(self.root, width=40, height=10)
        self.menu_textbox.grid(row=1, column=0, padx=10, pady=10, sticky=tk.W)

        self.show_menu_by_category()

        # Assuming you have a SQLAlchemy model named MenuItem
        menu_items = self.session.query(MenuItem.item, MenuItem.price).all()

# Process the result as needed
        for item, price in menu_items:
            print(f"Item: {item}, Price: {price}")

        self.menu_listbox = tk.Listbox(self.root, selectmode=tk.MULTIPLE, height=4)
        self.menu_items = self.fetch_menu_items()

        # Populate the menu_listbox with items from the 'menu' table
        for item, price in self.menu_items.items():
            self.menu_listbox.insert(tk.END, f"{item} - £{self.menu_items[item]['price']:.2f} ")
        self.menu_listbox.grid(row=1, column=1, padx=10, pady=10, sticky=tk.W)

        self.add_to_order_button = tk.Button(self.root, text="ADD TO ORDER", command=self.add_to_order)
        self.add_to_order_button.grid(row=2, column=1, padx=35, sticky=tk.W)

        # self.order_label = tk.Label(master, text="Order Information:")
        # self.order_label.grid(row=0, column=1, padx=10, pady=10, sticky=tk.W)

        self.order_listbox = tk.Listbox(self.root, height=4)
        self.order_listbox.grid(row=1, column=2, padx=10, pady=10, sticky=tk.W)

        self.remove_from_order_button = tk.Button(self.root, text="REMOVE FROM ORDER", command=self.remove_from_order)
        self.remove_from_order_button.grid(row=2, column= 2, padx=17, sticky=tk.W)

        self.place_order_button = tk.Button(self.root, text="PLACE ORDER", command=self.place_order)
        self.place_order_button.grid(row=4, column=1, padx=38, sticky=tk.W)

        self.view_order_button = tk.Button(self.root, text="VIEW ORDERS", command=self.view_order)
        self.view_order_button.grid(row=4, column=2,padx=42, sticky=tk.W)

        self.total_cost_label = tk.Label(self.root, text="Total Cost: £0.00")
        self.total_cost_label.grid(row=0, column=2, padx=10, pady=10, sticky=tk.W)

        if self.user_role == "back-end staff":
            self.add_to_order_button.config(state=tk.DISABLED)
            self.place_order_button.config(state=tk.DISABLED)
            self.remove_from_order_button.config(state = tk.DISABLED)
            self.menu_listbox.config(state = tk.DISABLED)

        elif self.user_role == "delivery staff":
            self.add_to_order_button.config(state=tk.DISABLED)
            self.place_order_button.config(state=tk.DISABLED)
            self.remove_from_order_button.config(state = tk.DISABLED)
            self.menu_listbox.config(state = tk.DISABLED)

        # Close the SQLite connection when the GUI is closed
        self.root.protocol("WM_DELETE_WINDOW", self.on_close)
        self.center_window(self.root)
    
    def get_user_by_id(self, user_id):
        session = self.session
        user = session.query(User).filter_by(id=user_id).first()
        session.close()
        return user
    
    def show_menu_by_category(self):
        # Fetch menu items from the 'menu' table in the database
        menu_items = self.session.query(MenuItem).all()

        # Group menu items by category
        menu_by_category = {}
        for item in menu_items:
            if item.category not in menu_by_category:
                menu_by_category[item.category] = []
            menu_by_category[item.category].append(f"{item.item} - £{item.price:.2f}")

        self.menu_textbox.config(state=tk.NORMAL)

        # Display menu by category in the Text widget
        for category, items in menu_by_category.items():
            self.menu_textbox.insert(tk.END, f"{category}:\n")
            for item in items:
                self.menu_textbox.insert(tk.END, f"  {item}\n")
            self.menu_textbox.insert(tk.END, "\n")

        self.menu_textbox.config(state=tk.DISABLED)

    
    def center_window(self, window):
        window.update_idletasks()  # Ensure that window attributes are updated
        width = window.winfo_width()
        height = window.winfo_height()

        x_position = (window.winfo_screenwidth() - width) // 2
        y_position = (window.winfo_screenheight() - height) // 2

        window.geometry(f"+{x_position}+{y_position}")


    # def fetch_menu_items(self):
    #     # Fetch menu items from the 'menu' table in the database
    #     menu_items = self.session.query(MenuItem).all()
    #     return {item.item: item.price for item in menu_items}
    
    def fetch_menu_items(self):
    # Fetch menu items from the 'menu' table in the database
        menu_items = self.session.query(MenuItem).all()
        return {item.item: {"price": item.price, "category": item.category} for item in menu_items}

    
    
    def add_to_order(self):
        selected_indices = self.menu_listbox.curselection()

        for idx in selected_indices:
            item_entry = self.menu_listbox.get(idx)
            item_name, item_price_str = item_entry.split(" - ")
            item_price = float(item_price_str.replace("£", ""))

            # Extract item name and category
            menu_item = self.menu_items.get(item_name)
            item_category = menu_item.get("category", "Unknown")

        # Check if the item is already in order_items
            existing_item = next((order_item for order_item in self.order_items if order_item[0] == item_name), None)

            if existing_item:
            # If the item is already in order_items, create a new tuple with updated quantity
                updated_item = (existing_item[0], existing_item[1] + 1, existing_item[2])
                self.order_items.remove(existing_item)
                self.order_items.append(updated_item)
            else:
            # If the item is not in order_items, add a new tuple with quantity 1
                self.order_items.append((item_name, 1, item_price))

    # Update the order_listbox
        self.update_order_listbox()

    def remove_from_order(self):
        selected_indices = self.order_listbox.curselection()
        for idx in reversed(selected_indices):
            item = self.order_items[idx]
            if item[1] > 1:
                # If quantity > 1, decrement the quantity
                self.order_items[idx] = (item[0], item[1] - 1)
                self.order_listbox.delete(idx)
                self.order_listbox.insert(idx, f"{item[0]} x{item[1] - 1} - £{self.menu_items[item[0]] * (item[1] - 1):.2f}")
            else:
                # If quantity is 1, remove the item
                del self.order_items[idx]
                self.order_listbox.delete(idx)

    def place_order(self):  
        if not self.order_items:
            messagebox.showinfo("Order Empty", "Please add items to your order before placing.")
        else:
            # Initialize variables for ordered items and total cost
            self.root.iconify()
            ordered_items = []
            total_cost = 0
            final_total_cost = 0
            final_total_cost_0 = 0

            print(self.user_role)
            if self.user_role == "manager"  or  " HR Director" :
                for item_info in self.order_items:
                    item_name, quantity, item_price = item_info
                    total_cost += quantity * item_price
                    ordered_items.append(f"{item_name} - x{quantity}")

                request = messagebox.askyesno("OFFER", "Is customer a worker of the company?")
                if request:
                    discount = 20
                    final_total_cost =  total_cost * (discount/100)
                    final_total_cost_0 = (total_cost - final_total_cost)
                    total_cost = final_total_cost_0
                    print( total_cost)

                else:

                    request_1 = messagebox.askyesno("OFFER", "Do you want to offer a discount?")
                    if request_1:
                        discount = simpledialog.askinteger("DISCOUNT", "How much discount (in percentage)?")
                        final_total_cost = total_cost * ( discount / 100)
                        final_total_cost_0 = (total_cost - final_total_cost)
                        total_cost = final_total_cost_0
                        print(total_cost)

            # Create a comma-separated string of ordered items
                order_details = ", ".join(ordered_items)

            # Create a new window for payment information
                payment_window = Toplevel(self.root)
                payment_window.title("Payment Information")
                payment_window.geometry("400x330")
                payment_window.resizable(False, False)
                self.center_window(payment_window)

            # Validation functions
                validate_card_number = (payment_window.register(self.validate_card_number), "%P")
            
                validate_cvv = (payment_window.register(self.validate_cvv), "%P")
            
                validate_name = (payment_window.register(self.validate_name), "%P")

                cost = tk.Label(payment_window, text=f"Total Cost: £{total_cost:.2f}")
                cost.grid(row=0, column=0, columnspan=2, padx=10, pady=10, sticky=tk.W)

            # Create labels and entry widgets for payment information
                name_label = tk.Label(payment_window, text="Name on Card:")
                name_label.grid(row=1, column=0, padx=10, pady=10, sticky=tk.W)

                name_entry = tk.Entry(payment_window, validate="key", validatecommand=validate_name)
                name_entry.grid(row=1, column=1, padx=10, pady=10, sticky=tk.W)

                card_number_label = tk.Label(payment_window, text="Card Number:")
                card_number_label.grid(row=2, column=0, padx=10, pady=10, sticky=tk.W)

                card_number_entry = tk.Entry(payment_window, validate="key", validatecommand=validate_card_number)
                card_number_entry.grid(row=2, column=1, padx=10, pady=10, sticky=tk.W)

                expiry_label = tk.Label(payment_window, text="Expiry Date (MM/YYYY):")
                expiry_label.grid(row=3, column=0, padx=10, pady=10, sticky=tk.W)

                expiry_date_entry = tk.Entry(payment_window, validate="key", validatecommand=(payment_window.register(self.validate_expiry_date), "%P"))
                expiry_date_entry.grid(row=3, column=1, padx=10, pady=10, sticky=tk.W)

                cvv_label = tk.Label(payment_window, text="CVV:")
                cvv_label.grid(row=4, column=0, padx=10, pady=10, sticky=tk.W)

                cvv_entry = tk.Entry(payment_window, validate="key", validatecommand=validate_cvv)
                cvv_entry.grid(row=4, column=1, padx=10, pady=10, sticky=tk.W)

                pin_label = tk.Label(payment_window, text="Card PIN:")
                pin_label.grid(row=5, column=0, padx=10, pady=10, sticky=tk.W)

                pin_entry = tk.Entry(payment_window, validate="key", show = "*", validatecommand=(payment_window.register(self.validate_pin), "%P"))
                pin_entry.grid(row=5, column=1, padx=10, pady=10, sticky=tk.W)

                pay_button = tk.Button(payment_window, text="PAY", command=lambda: self.process_payment(order_details, total_cost, payment_window))
                pay_button.grid(row=6, column=0, columnspan=2, pady=10, sticky=tk.W)

                back_button = tk.Button(payment_window, text="Back", command=payment_window.destroy)
                back_button.grid(row=7, column=0, columnspan=2, pady=10, sticky=tk.W)

                payment_window.protocol("WM_DELETE_WINDOW", lambda: self.deiconify_and_destroy(payment_window))

            elif self.user_role == "front-end staff":
                for item_info in self.order_items:
                    item_name, quantity, item_price = item_info
                    total_cost += quantity * item_price
                    ordered_items.append(f"{item_name} - x{quantity}")
                request = messagebox.askyesno("OFFER", "Is the customer a worker of the company?")
                if request:
                    discount = 20
                    final_total_cost =  total_cost * (discount/100)
                    final_total_cost_0 = (total_cost - final_total_cost)
                    total_cost = final_total_cost_0
                    print( total_cost)

            # Create a comma-separated string of ordered items
                order_details = ", ".join(ordered_items)

            # Create a new window for payment information
                payment_window = Toplevel(self.root)
                payment_window.title("Payment Information")
                payment_window.geometry("400x330")
                payment_window.resizable(False, False)
                self.center_window(payment_window)

            # Validation functions
                validate_card_number = (payment_window.register(self.validate_card_number), "%P")
            
                validate_cvv = (payment_window.register(self.validate_cvv), "%P")
            
                validate_name = (payment_window.register(self.validate_name), "%P")

                cost = tk.Label(payment_window, text=f"Total Cost: £{total_cost:.2f}")
                cost.grid(row=0, column=0, columnspan=2, padx=10, pady=10, sticky=tk.W)

            # Create labels and entry widgets for payment information
                name_label = tk.Label(payment_window, text="Name on Card:")
                name_label.grid(row=1, column=0, padx=10, pady=10, sticky=tk.W)

                name_entry = tk.Entry(payment_window, validate="key", validatecommand=validate_name)
                name_entry.grid(row=1, column=1, padx=10, pady=10, sticky=tk.W)

                card_number_label = tk.Label(payment_window, text="Card Number:")
                card_number_label.grid(row=2, column=0, padx=10, pady=10, sticky=tk.W)

                card_number_entry = tk.Entry(payment_window, validate="key", validatecommand=validate_card_number)
                card_number_entry.grid(row=2, column=1, padx=10, pady=10, sticky=tk.W)

                expiry_label = tk.Label(payment_window, text="Expiry Date (MM/YYYY):")
                expiry_label.grid(row=3, column=0, padx=10, pady=10, sticky=tk.W)

                expiry_date_entry = tk.Entry(payment_window, validate="key", validatecommand=(payment_window.register(self.validate_expiry_date), "%P"))
                expiry_date_entry.grid(row=3, column=1, padx=10, pady=10, sticky=tk.W)

                cvv_label = tk.Label(payment_window, text="CVV:")
                cvv_label.grid(row=4, column=0, padx=10, pady=10, sticky=tk.W)

                cvv_entry = tk.Entry(payment_window, validate="key", validatecommand=validate_cvv)
                cvv_entry.grid(row=4, column=1, padx=10, pady=10, sticky=tk.W)

                pin_label = tk.Label(payment_window, text="Card PIN:")
                pin_label.grid(row=5, column=0, padx=10, pady=10, sticky=tk.W)

                pin_entry = tk.Entry(payment_window, validate="key", show = "*", validatecommand=(payment_window.register(self.validate_pin), "%P"))
                pin_entry.grid(row=5, column=1, padx=10, pady=10, sticky=tk.W)

                pay_button = tk.Button(payment_window, text="PAY", command=lambda: self.process_payment(order_details, total_cost, payment_window))
                pay_button.grid(row=6, column=0, columnspan=2, pady=10, sticky=tk.W)

                payment_window.protocol("WM_DELETE_WINDOW", lambda: self.deiconify_and_destroy(payment_window))
            else:
                messagebox.showinfo("USER", "User not recognised")
                self.root.deiconify()
                return False


    def validate_card_number(self, input_text):
        return input_text.isdigit() and len(input_text) <= 16 or len(input_text) == 0
    
    def validate_expiry_date(self, input_text):
    # Allow only digits, "/", and a maximum of 7 characters
        if all(char.isdigit() or char == '/' for char in input_text) and len(input_text) <= 7:
        # Check for the correct format "mm/yyyy" only when the length is 7
            if len(input_text) == 7:
                parts = input_text.split('/')
                if len(parts) == 2 and len(parts[0]) <= 2 and len(parts[1]) <= 4:
                # Additional checks for month and year can be added if needed
                    return True
                else:
                    messagebox.showerror("Invalid Format", "Please enter the date in the format 'mm/yyyy'.")
                    return False
            else:
                return True
        else:
            messagebox.showerror("Invalid Characters", "Please enter only digits and '/' for the date.")
            return False

    def validate_pin(self, input_text):
    # Validate the PIN to allow only digits with a maximum length of 4
        return input_text.isdigit() and len(input_text) <= 4 or len(input_text) == 0

    def validate_cvv(self, input_text):
        return input_text.isdigit() and len(input_text) <= 3  or len(input_text) == 0

    def validate_name(self, input_text):
        return all(char.isalpha() or char.isspace() for char in input_text) or len(input_text) == 0

    def process_payment(self, order_details, total_cost, payment_window):
        # Perform payment processing here
        # You would typically integrate with a payment gateway for real transactions
        
        success = messagebox.askyesno("Payment", "Do you want to proceed?")
        
        if success:
            # Save the order to the database using SQLAlchemy
            order = Order(orderinfo=order_details, cost=total_cost, branch = self.user_branch, restaurant = self.user_restaurant)
            self.session.add(order)
            self.session.commit()

            # Fetch the recently added order
            order = self.session.query(Order).order_by(Order.id.desc()).first()

            # Display order information in the message box
            if order:
                message = f"Order successful!\n\nOrder id: {order.id}\nOrdered Items: {order.orderinfo}\n\nTotal Cost: £{order.cost:.2f}"
                messagebox.showinfo("Order Placed", message)

            # Clear order information
            self.order_items.clear()
            self.update_order_listbox()  # Update the order_listbox
            self.total_cost_label.config(text="Total Cost: £0.00")

            # Close the payment window
            payment_window.destroy()
            self.root.deiconify()
        else:
            messagebox.showinfo("Payment Cancelled", "Payment cancelled. Your order has not been placed.")
            self.root.deiconify()

    def process_payment_2(self, order_details, total_cost, payment_window):

        success = messagebox.askyesno("Payment", "Do you want to proceed?")
    
        if success:
        # Retrieve the existing order from the database using SQLAlchemy
            existing_order = self.session.query(Order).filter_by(orderinfo=order_details).first()

            if existing_order:
                existing_order.cost = total_cost
                self.order_items.clear()
                self.update_order_listbox()

                payment_window.destroy()
                self.root.deiconify()
                self.total_cost_label.config(text="Total Cost: £0.00")

                self.session.commit()       
            
                messagebox.showinfo(" Successful", "Update successful!.")
            else:
                messagebox.showerror("Error", "Order not found in the database.")
                self.session.commit()
        else:
            messagebox.showinfo("Payment Cancelled", "Update cancelled.")
            payment_window.destroy()
            self.view_order()

    def view_order(self):
        print(self.user_role)
    # Fetch data from the 'orders' table using SQLAlchemy
        if self.user_role == "HR Director":
            print("First")
            orders_data = self.session.query(Order.id, Order.orderinfo, Order.cost, Order.branch, Order.restaurant).all()

            if not orders_data:
                messagebox.showinfo("No Orders", "No orders found.")
            else:
        # Create a new window for viewing orders
                self.view_order_window = tk.Toplevel(self.root)
                self.view_order_window.title("Orders")
                self.view_order_window.geometry("675x370")
                self.view_order_window.resizable(False, False)
                self.center_window(self.view_order_window)
                self.root.iconify()

                self.view_order_window.protocol("WM_DELETE_WINDOW", lambda: self.deiconify_and_destroy(self.view_order_window))

        # Create a Label and Entry for searching by ID
                search_label = tk.Label(self.view_order_window, text="Search by Order ID:")
                search_label.grid(row=0, column=0, padx=5, pady=10, sticky=tk.W)

                search_entry = tk.Entry(self.view_order_window)
                search_entry.grid(row=0, column=1, padx=5, pady=10, sticky=tk.W)

        # Create a Listbox to display the orders
                order_listbox = tk.Listbox(self.view_order_window, height=10, width=60)
                order_listbox.grid(row=1, column=0, columnspan=2, padx=10, pady=10, sticky=tk.W)

        # Insert data into the Listbox
                for order in orders_data:
                    order_listbox.insert(tk.END, f"Order ID: {order.id}, Items: {order.orderinfo},"
                                         f" Total Cost: £{order.cost:.2f}, Branch: {order.branch}"
                                         f" Restaurant: {order.restaurant}"
                                         )

                search_button = tk.Button(self.view_order_window, text="Search", command=lambda: self.search_order(orders_data, search_entry.get(), order_listbox))
                search_button.grid(row=0, column=2, padx=5, pady=10, sticky=tk.W)

                update_order_button = tk.Button(self.view_order_window, text="UPDATE ORDER", command=lambda: self.update_order(orders_data, order_listbox, self.view_order_window))
                update_order_button.grid(row=2, column=0, columnspan=2, padx=10,  sticky=tk.W)

                delete_button = tk.Button(self.view_order_window, text="CANCEL ORDER", command=lambda: self.delete_order(orders_data, order_listbox))
                delete_button.grid(row=3, column=0, columnspan=2, padx=10, sticky=tk.W)

                close_button = tk.Button(self.view_order_window, text="Back", command=lambda: self.deiconify_and_destroy(self.view_order_window))
                close_button.grid(row=4, column=0, columnspan=2, padx=10, sticky=tk.W)

        elif self.user_role in ["manager", "front-end staff"]:
            print("Second")
            if self.user_role == "manager":
                print("manager")
            elif self.user_role == "front-end staff":
                print("front-end staff")
            desired_branch = self.user_branch
            print(desired_branch)
            desired_restaurant = self.user_restaurant
            print(desired_restaurant)
            orders_data = self.session.query(Order.id, Order.orderinfo, Order.cost, Order.branch, Order.restaurant).filter(
                Order.branch == desired_branch,
                Order.restaurant == desired_restaurant
            ).all()


            if not orders_data:
                messagebox.showinfo("No Orders", "No orders found.")
            else:
        # Create a new window for viewing orders
                self.view_order_window = tk.Toplevel(self.root)
                self.view_order_window.title("Orders")
                self.view_order_window.geometry("675x370")
                self.view_order_window.resizable(False, False)
                self.center_window(self.view_order_window)
                self.root.iconify()

                self.view_order_window.protocol("WM_DELETE_WINDOW", lambda: self.deiconify_and_destroy(self.view_order_window))

        # Create a Label and Entry for searching by ID
                search_label = tk.Label(self.view_order_window, text="Search by Order ID:")
                search_label.grid(row=0, column=0, padx=5, pady=10, sticky=tk.W)

                search_entry = tk.Entry(self.view_order_window)
                search_entry.grid(row=0, column=1, padx=5, pady=10, sticky=tk.W)

                refresh_button = tk.Button(self.view_order_window, text="Refresh Orders", command=self.refresh_orders)
                refresh_button.grid(row=1, column=0, columnspan=2, padx=10, sticky=tk.W)

        # Create a Listbox to display the orders
                order_listbox = tk.Listbox(self.view_order_window, height=10, width=60)
                order_listbox.grid(row=2, column=0, columnspan=2, padx=10, pady=10, sticky=tk.W)

        # Insert data into the Listbox
                for order in orders_data:
                    order_listbox.insert(tk.END, f"Order ID: {order.id}, Items: {order.orderinfo},"
                                         f" Total Cost: £{order.cost:.2f}, Branch: {order.branch}"
                                         f" Restaurant: {order.restaurant}"
                                         )

        # Add a search button
                search_button = tk.Button(self.view_order_window, text="Search", command=lambda: self.search_order(orders_data, search_entry.get(), order_listbox))
                search_button.grid(row=0, column=2, padx=5, pady=10, sticky=tk.W)

                update_order_button = tk.Button(self.view_order_window, text="UPDATE ORDER", command=lambda: self.update_order(orders_data, order_listbox,self.view_order_window ))
                update_order_button.grid(row=4, column=0, columnspan=2, padx=10,  sticky=tk.W)

                delete_button = tk.Button(self.view_order_window, text="CANCEL ORDER", command=lambda: self.delete_order(orders_data, order_listbox))
                delete_button.grid(row=5, column=0, columnspan=2, padx=10, sticky=tk.W)

        # Add a close button
                close_button = tk.Button(self.view_order_window, text="Back", command=lambda: self.deiconify_and_destroy(self.view_order_window))
                close_button.grid(row=6, column=0, columnspan=2, padx=10, sticky=tk.W)

        elif self.user_role in ["chef", "back-end staff"]:
            print("third")

            desired_branch = self.user_branch
            print(desired_branch)
            desired_restaurant = self.user_restaurant
            print(desired_restaurant)
            orders_data = self.session.query(Order.id, Order.orderinfo, Order.cost, Order.branch, Order.restaurant).filter(
                Order.branch == desired_branch,
                Order.restaurant == desired_restaurant
            ).all()


            if not orders_data:
                messagebox.showinfo("No Orders", "No orders found.")
            else:
        # Create a new window for viewing orders
                self.view_order_window = tk.Toplevel(self.root)
                self.view_order_window.title("Orders")
                self.view_order_window.geometry("675x370")
                self.view_order_window.resizable(False, False)
                self.center_window(self.view_order_window)
                self.root.iconify()

                self.view_order_window.protocol("WM_DELETE_WINDOW", lambda: self.deiconify_and_destroy(self.view_order_window))

        # Create a Label and Entry for searching by ID
                search_label = tk.Label(self.view_order_window, text="Search by Order ID:")
                search_label.grid(row=0, column=0, padx=5, pady=10, sticky=tk.W)

                search_entry = tk.Entry(self.view_order_window)
                search_entry.grid(row=0, column=1, padx=5, pady=10, sticky=tk.W)

                refresh_button = tk.Button(self.view_order_window, text="Refresh Orders", command=self.refresh_orders)
                refresh_button.grid(row=1, column=0, columnspan=2, padx=10, sticky=tk.W)

        # Create a Listbox to display the orders
                order_listbox = tk.Listbox(self.view_order_window, selectmode=tk.MULTIPLE, height=10, width=60)
                order_listbox.grid(row=2, column=0, columnspan=2, padx=10, pady=10, sticky=tk.W)

        # Insert data into the Listbox
                for order in orders_data:
                    order_listbox.insert(tk.END, f"Order ID: {order.id}, Items: {order.orderinfo},"
                                         f" Total Cost: £{order.cost:.2f}, Branch: {order.branch}"
                                         f" Restaurant: {order.restaurant}"
                                         )

        # Add a search button
                search_button = tk.Button(self.view_order_window, text="Search", command=lambda: self.search_order(orders_data, search_entry.get(), order_listbox))
                search_button.grid(row=0, column=2, padx=5, pady=10, sticky=tk.W)

                delete_button = tk.Button(self.view_order_window, text="MARK ORDER AS READY", command=lambda: self.order_ready(orders_data, order_listbox))
                delete_button.grid(row=4, column=0, columnspan=2, padx=10, sticky=tk.W)

        # Add a close button
                close_button = tk.Button(self.view_order_window, text="Back", command=lambda: self.deiconify_and_destroy(self.view_order_window))
                close_button.grid(row=5, column=0, columnspan=2, padx=10, sticky=tk.W)
        
        elif self.user_role == "delivery staff":
            desired_branch = self.user_branch
            print(desired_branch)
            desired_restaurant = self.user_restaurant
            print(desired_restaurant)
            orders_data = self.session.query(Order.id, Order.orderinfo, Order.cost, Order.branch, Order.restaurant).filter(
                Order.branch == desired_branch,
                Order.restaurant == desired_restaurant
            ).all()


            if not orders_data:
                messagebox.showinfo("No Orders", "No orders found.")
            else:
        # Create a new window for viewing orders
                self.view_order_window = tk.Toplevel(self.root)
                self.view_order_window.title("Orders")
                self.view_order_window.geometry("675x350")
                self.view_order_window.resizable(False, False)
                self.center_window(self.view_order_window)
                self.root.iconify()

                self.view_order_window.protocol("WM_DELETE_WINDOW", lambda: self.deiconify_and_destroy(self.view_order_window))

        # Create a Label and Entry for searching by ID
                search_label = tk.Label(self.view_order_window, text="Search by Order ID:")
                search_label.grid(row=0, column=0, padx=5, pady=10, sticky=tk.W)

                search_entry = tk.Entry(self.view_order_window)
                search_entry.grid(row=0, column=1, padx=5, pady=10, sticky=tk.W)

                refresh_button = tk.Button(self.view_order_window, text="Refresh Orders", command=self.refresh_orders)
                refresh_button.grid(row=1, column=0, columnspan=2, padx=10, sticky=tk.W)

        # Create a Listbox to display the orders
                order_listbox = tk.Listbox(self.view_order_window,selectmode=tk.MULTIPLE,  height=10, width=60)
                order_listbox.grid(row=2, column=0, columnspan=2, padx=10, pady=10, sticky=tk.W)

        # Insert data into the Listbox
                for order in orders_data:
                    order_listbox.insert(tk.END, f"Order ID: {order.id}, Items: {order.orderinfo},"
                                         f" Total Cost: £{order.cost:.2f}, Branch: {order.branch}"
                                         f" Restaurant: {order.restaurant}"
                                         )


                search_button = tk.Button(self.view_order_window, text="Search", command=lambda: self.search_order(orders_data, search_entry.get(), order_listbox))
                search_button.grid(row=0, column=2, padx=5, pady=10, sticky=tk.W)


                pack_button = tk.Button(self.view_order_window, text="PACK ORDER", command=lambda: self.pack_order(orders_data,order_listbox))
                pack_button.grid(row=4, column=0, columnspan=2, padx=10, sticky=tk.W)

                close_button = tk.Button(self.view_order_window, text="Back", command=lambda: self.deiconify_and_destroy(self.view_order_window))
                close_button.grid(row=5, column=0, columnspan=2, padx=10, sticky=tk.W)
        
    def order_ready(self, orders_data, order_listbox):
    # Get the selected order ID
        selected_order = order_listbox.curselection()

        if not selected_order:
        # Display a message if no order is selected
            messagebox.showinfo("No Selection", "Please choose an order to mark as ready.")
            return
        
        order_id = orders_data[selected_order[0]][0]
        order = self.session.query(Order).filter_by(id=order_id).first()
        if not order:
            messagebox.showerror("Error", "Order not found.")
            return
        else:
            self.session.delete(order)
            self.session.commit()
            messagebox.showinfo("Success", "Order successfully marked")
            order_listbox.delete(selected_order)
    

    def pack_order(self, orders_data, order_listbox):
    # Get the selected order ID
        selected_indices = order_listbox.curselection()

        if not selected_indices:
            messagebox.showinfo("No Selection", "Please select an order to pack.")
            return

        self.orders_data = orders_data
        self.packing_window = tk.Toplevel(self.root)
        self.packing_window.title("Packed Orders")
        self.packing_window.geometry("400x300")
        self.packing_window.resizable(False, False)
        self.center_window(self.packing_window)
        label = tk.Label(self.packing_window, text="Packed Orders:")
        label.pack(pady=10)

    # Listbox to display packed orders
        packed_orders_listbox = tk.Listbox(self.packing_window, selectmode=tk.MULTIPLE, height=10)
        packed_orders_listbox.pack(expand=True, fill=tk.BOTH)

    # Packed orders into the Listbox
        for index in selected_indices:
            order = self.orders_data[index]
            packed_orders_listbox.insert(tk.END, f"Order ID: {order.id}, Items: {order.orderinfo}, Total Cost: £{order.cost:.2f}")

        mark_delivered_button = tk.Button(self.packing_window, text="Mark as Delivered", command=lambda: self.mark_as_delivered(selected_indices, packed_orders_listbox, order_listbox))
        mark_delivered_button.pack(pady=10)

    def refresh_orders(self):
    # Get the current user's branch and restaurant
        desired_branch = self.user_branch
        desired_restaurant = self.user_restaurant

    # Fetch updated orders based on the user's branch and restaurant
        orders_data = self.session.query(Order.id, Order.orderinfo, Order.cost, Order.branch, Order.restaurant).filter(
            Order.branch == desired_branch,
            Order.restaurant == desired_restaurant
        ).all()

    # Clear the existing data in the order_listbox
        self.order_listbox.delete(0, tk.END)

    # Insert the updated data into the order_listbox
        if orders_data is None:
            self.order_listbox.insert(tk.END,"No orders availaible")
        else:
            for order in orders_data:
                self.order_listbox.insert(tk.END, f"Order ID: {order.id}, Items: {order.orderinfo},"
                                          f" Total Cost: £{order.cost:.2f}, Branch: {order.branch}"
                                          f" Restaurant: {order.restaurant}")

    
    def update_main_listbox(self, main_listbox):
    # Clear the main Listbox
        main_listbox.delete(0, tk.END)
        desired_branch = self.user_branch
        desired_restaurant = self.user_restaurant

    # Fetch and insert the latest orders into the main Listbox
        orders_data = self.session.query(Order.id, Order.orderinfo, Order.cost, Order.branch, Order.restaurant).filter(
            Order.branch == desired_branch,
            Order.restaurant == desired_restaurant
        ).all()
        for order in orders_data:
            main_listbox.insert(tk.END, f"Order ID: {order.id}, Items: {order.orderinfo},"
                             f" Total Cost: £{order.cost:.2f}, Branch: {order.branch}"
                             f" Restaurant: {order.restaurant}")
    
    def mark_as_delivered(self, selected_orders,  packed_orders_listbox, main_listbox):
        for order_index in selected_orders:
            order = self.orders_data[order_index]
            order_id = order.id
            order = self.session.query(Order).filter_by(id=order_id).first()
            self.session.delete(order)
            self.session.commit()
            packed_orders_listbox.delete(0, tk.END)
            self.packing_window.destroy()

        self.update_main_listbox(main_listbox)
        messagebox.showinfo("Success", "Nice work!")
    
    
    def deiconify_and_destroy(self,window):
        window.destroy()
        self.root.deiconify()

    def search_order(self, orders_data, search_id, order_listbox,):
        
        order_listbox.delete(0, tk.END)

    # Search for orders by ID
        matching_orders = []
        for order in orders_data:
            if str(order[0]) == search_id:
                matching_orders.append(order)

    # Display the search result in the Listbox  
        for order in matching_orders:
            order_listbox.insert(tk.END, f"Order ID: {order[0]}, Items: {order[1]}, Total Cost: £{order[2]:.2f}")

    # If no matching orders are found, show a message
        if not matching_orders:
            messagebox.showinfo("No Match", "No order found.")

    def extract_name_and_price(self, item):
    # Extract item name and quantity from the menu item string
        parts = item.split(" x")
        item_name = parts[0]
        item_quantity = int(parts[1]) if len(parts) > 1 else 1
        item_price = self.menu_items.get(item_name, 0)
        return item_name, item_quantity, item_price

    def update_order_listbox(self):
    # Clear and update the order_listbox with the updated order_items
        self.order_listbox.delete(0, tk.END)
        total_cost = 0

        for order_item in self.order_items:
            item_name, quantity, item_price = order_item
            total_price = quantity * item_price
            total_cost += total_price
            self.order_listbox.insert(tk.END, f"{item_name} (x{quantity}) - £{total_price:.2f}")

        self.total_cost_label.config(text=f"Total Cost: £{total_cost:.2f}")
    
    def update_order(self, orders_data, order_listbox, window):
    # Get the selected order ID
        selected_order = order_listbox.curselection()

        if not selected_order:
        # Display a message if no order is selected
            messagebox.showinfo("No Selection", "Please choose an order to update.")
            return
        
        window.destroy()

        order_id = orders_data[selected_order[0]][0]

    # Retrieve the current order details from the database
        order_data = self.session.query(Order.orderinfo, Order.cost).filter(Order.id == order_id).first()

# Process the result as needed
        if order_data:
            order_info, total_cost = order_data
            print(f"Order Info: {order_info}, Total Cost: {total_cost}")

        else:
            messagebox.showerror("Error", "Order not found.")
            return

        order_info, total_cost = order_data

    # Create a new window for updating the order
        update_order_window = Toplevel(self.root)
        update_order_window.title("Update Order")
        
        update_order_window.geometry("910x200")
        update_order_window.resizable(False,False)
        self.center_window(update_order_window)

    # Display the current order info for reference
        current_order_label = tk.Label(update_order_window, text=f"Current Order: {order_info} Cost: {total_cost}")
        current_order_label.grid(row=0, column=0, columnspan=2, padx=10, pady=10, sticky=tk.W)

        quantities_label = tk.Label(update_order_window, text="NOTE\n"
                                    "\nEnter the new quantities for each item in the order they were made, separated by commas." 
                                    "\nIf you do not want to change the quantity of an item, type the previous quantity."
                                    "\nIf you want to remove an item from the order, enter '0' for that item.")
        quantities_label.grid(row=1, column=0, padx=0, pady=10, sticky=tk.W)

        # quantities_entry = tk.Entry(update_order_window, validate="key", validatecommand=(update_order_window.register(self.validate_input), "%P"))
        quantities_entry = tk.Entry(
            update_order_window,
            validate="key",
            validatecommand=(update_order_window.register(self.validate_input), "%P")
        )
        quantities_entry.grid(row=1, column=1, padx=10, sticky=tk.W)

    # Add a button to confirm the update
        confirm_button = tk.Button(
            update_order_window,
            text="Confirm Update",
            command=lambda: self.update_and_close(order_id, quantities_entry.get(), update_order_window )
        )

        confirm_button.grid(row=1, column=2, columnspan=2, padx=10,  sticky=tk.W)

        update_order_window.protocol("WM_DELETE_WINDOW", lambda: self.deiconify_and_destroy(update_order_window))
        update_order_window.wait_window()
        
    def validate_input(self, input_text):
    # Validate the input to allow only numbers and at most one comma after each number
        if all(char.isdigit() or char == ',' for char in input_text):
        # Check for consecutive commas
            if ',' in input_text and ',,' in input_text:
            # Display an error message for multiple consecutive commas
                messagebox.showerror("Validation Error", "Do not place multiple commas after each other.")
                return False
            return True
        else:
        # Display a general error message for invalid input
            messagebox.showerror("Validation Error", "Invalid input. Please use numbers and commas only.")
            return False

    def update_and_close(self, order_id, new_quantities_str, window):
        self.window = window
        try:
            new_quantities = [int(qty) for qty in new_quantities_str.split(',')]
        except ValueError as e:
            messagebox.showerror("Invalid Quantities", str(e))
            return

    # Retrieve the current order details from the database using SQLAlchemy
        order = self.session.query(Order).filter_by(id=order_id).first()

        if not order:
            messagebox.showerror("Error", "Order not found.")
            return

        order_info, total_cost = order.orderinfo, order.cost

    # Update the quantities in the order_info string
        items = order_info.split(', ')
        updated_items = []

        for name, new_quantity in zip(items, new_quantities):
            item_name = name.split(' -')[0].strip()
            menu_item_value = self.menu_items.get(item_name, 0)
            print(f"Item Name: {item_name}, Menu Item Value: {menu_item_value}")

        for item, new_quantity in zip(items, new_quantities):
            name, qty = item.rsplit(' x', 1)
            new_quantity = int(new_quantity)  

            if name in self.menu_items:
                updated_items.append(f"{name} x{new_quantity}")
            else:
                updated_items.append(item)

        updated_order_info = ', '.join(updated_items)

        new_order_info = ', '.join([f"{re.sub(' x[0-9]+', '', name)} x{new_quantity}" for name, new_quantity in zip(items, new_quantities)])
    # Calculate the new total cost
        new_quantities = [int(name.split(' x')[1]) for name in new_order_info.split(', ')]

# Calculate the new total cost
        new_prices = [new_quantity * self.menu_items.get(item.split(' -')[0].strip(), {'price': 0})['price'] for item, new_quantity in zip(items, new_quantities)]
        new_total_cost = sum(new_prices)


        self.update_order_payment(total_cost, new_order_info, new_total_cost, window )

    def update_order_payment(self, old_cost, order_details, total_cost, window):
        if total_cost != 0:
            window.destroy()
            if old_cost < total_cost:
                window.destroy()
                update_payment_window = Toplevel(self.root)
                update_payment_window.title("Payment Information")
                update_payment_window.geometry("400x330")
                update_payment_window.resizable(False, False)
                self.center_window(update_payment_window)

                validate_card_number = (update_payment_window.register(self.validate_card_number), "%P")
            
                validate_cvv = (update_payment_window.register(self.validate_cvv), "%P")
            
                validate_name = (update_payment_window.register(self.validate_name), "%P")

                cost = tk.Label(update_payment_window, text=f"Total Cost: £{total_cost:.2f}")
                cost.grid(row=0, column=0, padx=10, pady=10, sticky=tk.W)

                refund = tk.Label(update_payment_window, text=f"Total refund: £{old_cost:.2f}")
                refund.grid(row= 0, column = 1, padx=10, pady=10, sticky=tk.W)

                name_label = tk.Label(update_payment_window, text="Name on Card:")
                name_label.grid(row=2, column=0, padx=10, pady=10, sticky=tk.W)

                name_entry = tk.Entry(update_payment_window, validate="key", validatecommand=validate_name)
                name_entry.grid(row=2, column=1, padx=10, pady=10, sticky=tk.W)

                card_number_label = tk.Label(update_payment_window, text="Card Number:")
                card_number_label.grid(row=3, column=0, padx=10, pady=10, sticky=tk.W)

                card_number_entry = tk.Entry(update_payment_window, validate="key", validatecommand=validate_card_number)
                card_number_entry.grid(row=3, column=1, padx=10, pady=10, sticky=tk.W)

                expiry_label = tk.Label(update_payment_window, text="Expiry Date (MM/YYYY):")
                expiry_label.grid(row=4, column=0, padx=10, pady=10, sticky=tk.W)

                expiry_date_entry = tk.Entry(update_payment_window, validate="key", validatecommand=(update_payment_window.register(self.validate_expiry_date), "%P"))
                expiry_date_entry.grid(row=4, column=1, padx=10, pady=10, sticky=tk.W)

                cvv_label = tk.Label(update_payment_window, text="CVV:")
                cvv_label.grid(row=5, column=0, padx=10, pady=10, sticky=tk.W)

                cvv_entry = tk.Entry(update_payment_window, validate="key", validatecommand=validate_cvv)
                cvv_entry.grid(row=5, column=1, padx=10, pady=10, sticky=tk.W)

                pin_label = tk.Label(update_payment_window, text="Card PIN:")
                pin_label.grid(row=6, column=0, padx=10, pady=10, sticky=tk.W)

                pin_entry = tk.Entry(update_payment_window, validate="key", show="*", validatecommand=(update_payment_window.register(self.validate_pin), "%P"))
                pin_entry.grid(row=6, column=1, padx=10, pady=10, sticky=tk.W)

                pay_button = tk.Button(update_payment_window, text="PAY", command=lambda: self.process_payment_2(order_details, total_cost, update_payment_window))
                pay_button.grid(row=7, column=0, columnspan=2, pady=10, sticky=tk.W)

                validate_card_number = (update_payment_window.register(self.validate_card_number), "%P")
                validate_cvv = (update_payment_window.register(self.validate_cvv), "%P")
                validate_name = (update_payment_window.register(self.validate_name), "%P")

                update_payment_window.protocol("WM_DELETE_WINDOW", lambda: self.deiconify_and_destroy(update_payment_window))
            else:
                refund = (int(old_cost) - int(total_cost))
                messagebox.showinfo("Refund", f"Customer should be refunded £{refund}")
                self.session.commit()
                self.view_order()
        else:
            messagebox.showinfo("USER", "Order cancelled")
            self.view_order()


    def delete_order(self, orders_data, order_listbox):
    # Get the selected order ID
        selected_order = order_listbox.curselection()

        if not selected_order:
        # Display a message if no order is selected
            messagebox.showinfo("No Selection", "Please choose an order to cancel.")
            return
        confirmation = messagebox.askyesno("Confirmation", f"Are you sure you want to remove order?")

        if confirmation:
            order_id = orders_data[selected_order[0]][0]
            order = self.session.query(Order).filter_by(id=order_id).first()
            if not order:
                messagebox.showerror("Error", "Order not found.")
                return
            self.session.delete(order)
            self.session.commit()
            messagebox.showinfo("Success", "Order successfully cancelled")
            order_listbox.delete(selected_order)
        else:
            messagebox.showinfo("Canceled", "Canceled.")

    def on_close(self):
        # Close the SQLite connection when the GUI is closed
        self.session.close()
        self.root.destroy()

#FoodItemApp class by Ali Mahmoud
class FoodItemApp(tk.Toplevel):
    def __init__(self, parent, session, login_instance):
        self.parent = parent
        self.session = session
        self.parent.title("Items")
        self.center_window(self.parent)

        login_user = self.get_user_by_id(login_instance)
        self.user_role = login_user.role


        self.uName = StringVar()
        self.uQuantity = StringVar()
        self.uPrice = StringVar()

        engine = create_engine('sqlite:///restaurant.db', echo=True)
        Base.metadata.create_all(engine)
        Session = sessionmaker(bind=engine)
        self.session = Session()

        self.lstName = Listbox(self.parent, width=15, height=10)
        self.yscroll = Scrollbar(command=self.lstName.yview)
        self.yscroll.grid(row=0, column=3, sticky=NS, rowspan=3)

        self.lstName.grid(row=0, padx=0, pady=0, sticky=W, columnspan=2, rowspan=3)
        self.lstName.configure(yscrollcommand=self.yscroll.set)
        self.yscroll.config(command=self.lstName.yview)

        self.txt = Text(self.parent, height=10)
        self.txt.grid(padx=5, pady=0, row=0, column=5)

        f = Frame(self.parent)
        f.grid(row=1, column=5)

        self.add_button = tk.Button(f, text="Add", command=self.add_new, width=10)
        self.add_button.grid(row=0, column=0, sticky=W, pady=5)
        self.edit_button = tk.Button(f, text="Edit", command=self.edit, width=10)
        self.edit_button.grid(row=0, column=1, sticky=W, pady=5)
        self.remove_button = tk.Button(f, text="Remove", command=self.remove, width=10)
        self.remove_button.grid(row=0, column=2, sticky=E, pady=5)

        if self.user_role == "back-end staff":
            self.add_button.config(state = tk.DISABLED)
            self.edit_button.config(state = tk.DISABLED)
            self.remove_button.config(state = tk.DISABLED)
            

        self.loaded_list = self.upload()
        if self.loaded_list is not None:
            for line in self.loaded_list:
                countryName = line[0]
                self.lstName.insert('end', countryName)

        self.lstName.bind('<<ListboxSelect>>', self.select)
    
    def get_user_by_id(self, user_id):
        session = self.session
        user = session.query(User).filter_by(id=user_id).first()
        session.close()
        return user

    def upload(self):
        food_items = self.session.query(FoodItem).order_by(FoodItem.name).all()
        lst = [(item.name, item.quantity) for item in food_items]
        return lst

    def insert_data(self, lst):
        try:
            food_item = FoodItem(name=lst[0], quantity=lst[1])
            self.session.add(food_item)
            self.session.commit()
        except Exception as err:
            print("Failed to insert", err)
        finally:
            print("Connection closed")

    def delete_data(self, lst):
        try:
            food_item = self.session.query(FoodItem).filter_by(name=lst).first()
            if food_item:
                confirmation = messagebox.askyesno("Confirmation", f"Are you sure you want to delete item?")
                if confirmation:
                    self.session.delete(food_item)
                    self.session.commit()
                else:
                    messagebox.showinfo("Canceled", "Canceled.")
        except Exception as error:
            print("Failed to delete", error)
        finally:
            print("Connection closed")


    def select(self, evt):
        value = self.lstName.curselection()[0]
        templist = self.loaded_list[value]

        self.txt.delete(1.0, END)
        for i in templist:
            self.txt.insert(END, str(i) + '\n')

    def add_new(self):
        nMem = Toplevel(self.parent)
        nMem.title("New Item")
        nMem.geometry("300x250")

        Label(nMem, text="Item Name:").grid(row=0, column=0)
        Entry(nMem, textvariable=self.uName).grid(row=0, column=1)
        Label(nMem, text="Quantity:").grid(row=1, column=0)
        Entry(nMem, textvariable=self.uQuantity).grid(row=1, column=1)
        Label(nMem, text="Price:").grid(row=2, column=0)
        Entry(nMem, textvariable=self.uPrice).grid(row=2, column=1)

        Button(nMem, text="Add", command=self.register).grid(row=4, column=0)
        Button(nMem, text="Cancel", command=nMem.destroy).grid(row=4, column=1)


    def update(self, old_name, listbox):
        new_data = [self.uName.get(), self.uQuantity.get()]
        self.update_data(new_data, old_name)

    # Reload the list
        self.loaded_list = self.upload()
        listbox.delete(0, END)
        for line in self.loaded_list:
            itemName = line[0]

            listbox.insert('end', itemName)

    
    def edit(self):
        selected_index = self.lstName.curselection()
        if selected_index:
            nMem = Toplevel(self.parent)
            nMem.title("Edit Item")
            nMem.geometry("300x250")

        # Get the selected item from the listbox
            selected_item = self.lstName.get(selected_index)

        # Extract the name from the selected item
            old_name = selected_item
            self.uName.set(old_name)

            Label(nMem, text="Item Name:").grid(row=0, column=0)
            Entry(nMem, textvariable=self.uName).grid(row=0, column=1)
            Label(nMem, text="Quantity:").grid(row=1, column=0)
            Entry(nMem, textvariable=self.uQuantity).grid(row=1, column=1)
            Label(nMem, text="Price:").grid(row=2, column=0)
            Entry(nMem, textvariable=self.uPrice).grid(row=2, column=1)

        # Pass the listbox as an argument to the update_data method
            Button(nMem, text="Update", command=lambda: self.update_data([self.uName.get(), self.uQuantity.get(), self.uPrice.get()], old_name, self.lstName)).grid(row=4, column=0)
            Button(nMem, text="Cancel", command=nMem.destroy).grid(row=4, column=1)


    def update_data(self, new_data, old_name, listbox): 
        try:
        # Retrieve the FoodItem object based on the old name
            food_item = self.session.query(FoodItem).filter_by(name=old_name).first()
            print("old name", old_name)
            print("item ",food_item)

            if food_item:
            # Update the FoodItem object with the new data
                food_item.name = new_data[0]
                food_item.quantity = int(new_data[1])  # Convert quantity to int
                food_item.price  = float(new_data[2])

                # new_food_item = FoodItem(name=new_data[0], quantity=int(new_data[1]))
                # self.session.add(new_food_item)
                self.session.commit()

            # Reload the list
                self.loaded_list = self.upload()
                listbox.delete(0, END)
                for line in self.loaded_list:
                    itemName = line[0]
                    listbox.insert('end', itemName)
        except Exception as error:
            print("Failed to update", error)
        finally:
            print("Connection closed")

    
    def center_window(self, window):

        screen_width = window.winfo_screenwidth()
        screen_height = window.winfo_screenheight()

        requested_width = window.winfo_reqwidth()
        requested_height = window.winfo_reqheight()

        x_position = (screen_width - requested_width) // 2
        y_position = (screen_height - requested_height) // 2

        window.geometry(f"+{x_position}+{y_position}")

    def remove(self):
        value = self.lstName.curselection()[0]
        templist = self.loaded_list[value]

        self.delete_data(templist[0])
        self.loaded_list.remove(templist)

        self.lstName.delete(0, END)
        self.txt.delete(1.0, END)

        for line in self.loaded_list:
            ItemName = line[0]
            self.lstName.insert('end', ItemName)

    def register(self):
        list1 = [self.uName.get(), self.uQuantity.get(), self.uPrice.get()]
        self.loaded_list.append(list1)
        self.insert_data(list1)
        self.lstName.insert(END, list1[0])

#FoodCategoryApp class by Ali Mahmoud
class FoodCategoryApp(tk.Toplevel):
    def __init__(self, parent, session, login_instance):
        self.parent = parent
        self.parent.title("Category")
        self.center_window(self.parent)
        self.session = session

        login_user = self.get_user_by_id(login_instance)
        self.user_role = login_user.role

        self.uName = StringVar()
        self.uDescription = StringVar()

        self.lstName = Listbox(self.parent, width=15, height=10)
        self.yscroll = Scrollbar(command=self.lstName.yview)
        self.yscroll.grid(row=0, column=3, sticky=NS, rowspan=3)

        self.lstName.grid(row=0, padx=0, pady=0, sticky=W, columnspan=2, rowspan=3)
        self.lstName.configure(yscrollcommand=self.yscroll.set)
        self.yscroll.config(command=self.lstName.yview)

        self.txt = Text(self.parent, width=30, height=10)
        self.txt.grid(padx=5, pady=0, row=0, column=5)

        f = Frame(self.parent)
        f.grid(row=1, column=5)

        self.add_button = tk.Button(f, text="Add", command=self.add_new, width=10)
        self.add_button.grid(row=0, column=0, sticky=W, pady=5)
        self.edit_button = tk.Button(f, text="Edit", command=self.edit, width=10)
        self.edit_button.grid(row=0, column=1, sticky=W, pady=5)
        self.remove_button = tk.Button(f, text="Remove", command=self.remove, width=10)
        self.remove_button.grid(row=0, column=2, sticky=E, pady=5)

        if self.user_role == "back-end staff":
            self.add_button.config(state = tk.DISABLED)
            self.edit_button.config(state = tk.DISABLED)
            self.remove_button.config(state = tk.DISABLED)

        self.loaded_list = self.upload()
        if self.loaded_list is not None:
            for line in self.loaded_list:
                categoryName = line[0]
                self.lstName.insert('end', categoryName)

        self.lstName.bind('<<ListboxSelect>>', self.select)
    
    def get_user_by_id(self, user_id):
        session = self.session
        user = session.query(User).filter_by(id=user_id).first()
        session.close()
        return user

    def upload(self):
        try:
            categories = self.session.query(FoodCategory).order_by(FoodCategory.name).all()
            lst = [(category.name, category.description) for category in categories]
            return lst
        except Exception as error:
            print("Failed to fetch data", error)
        finally:
            print("Connection closed")

    def insert_data(self, lst):
        try:
            new_category = FoodCategory(name=lst[0], description=lst[1])
            self.session.add(new_category)
            self.session.commit()
        except Exception as error:
            print("Failed to insert", error)
        finally:
            print("Connection closed")

    def delete_data(self, lst):
        try:
            category_to_delete = self.session.query(FoodCategory).filter_by(name=lst).first()
            if category_to_delete:
                confirmation = messagebox.askyesno("Confirmation", f"Are you sure you want to delete category?")
                if confirmation:
                    self.session.delete(category_to_delete)
                    self.session.commit()
                else:
                    messagebox.showinfo("Canceled", "Canceled.")
            else:
                print("Category not found.")
        except Exception as error:
            print("Failed to delete", error)
        finally:
            print("Connection closed")

    def update_data(self, lst, old_name, listbox):
        try:
            category_to_update = self.session.query(FoodCategory).filter_by(name=old_name).first()
            if category_to_update:
                category_to_update.name = lst[0]
                category_to_update.description = lst[1]
                # new_food_item = FoodItem(name = lst[0], description=lst[1])
                # self.session.add(new_food_item)
                self.session.commit()
            else:
                print("Category not found.")
        except Exception as error:
            print("Failed to update", error)
        finally:
            print("Connection closed")

    def select(self, evt):
        value = self.lstName.curselection()[0]
        templist = self.loaded_list[value]

        self.txt.delete(1.0, END)
        for i in templist:
            self.txt.insert(END, str(i) + '\n')

    def add_new(self):
        nMem = Toplevel(self.parent)
        nMem.title("New Category")
        nMem.geometry("300x250")

        Label(nMem, text="Category Name:").grid(row=0, column=0)
        Entry(nMem, textvariable=self.uName).grid(row=0, column=1)
        Label(nMem, text="Description:").grid(row=1, column=0)
        Entry(nMem, textvariable=self.uDescription).grid(row=1, column=1)

        Button(nMem, text="Add", command=self.register).grid(row=4, column=0)
        Button(nMem, text="Cancel", command=nMem.destroy).grid(row=4, column=1)

    
    def update(self, old_name, listbox):
        new_data = [self.uName.get(), self.uDescription.get()]
        self.update_data(new_data, old_name)

        # Reload the list
        self.loaded_list = self.upload()
        listbox.delete(0, END)
        for line in self.loaded_list:
            categoryName = line[0]
            self.lstName.insert('end', categoryName)
    

    def edit(self):
        selected_index = self.lstName.curselection()
        if selected_index:
            nMem = Toplevel(self.parent)
            nMem.title("Edit Category")
            nMem.geometry("300x250")

            old_name = self.lstName.get(selected_index)
            self.uName.set(old_name)

            Label(nMem, text="Category Name:").grid(row=0, column=0)
            Entry(nMem, textvariable=self.uName).grid(row=0, column=1)
            Label(nMem, text="Description:").grid(row=1, column=0)
            Entry(nMem, textvariable=self.uDescription).grid(row=1, column=1)

            Button(nMem, text="Update", command=lambda: self.update_data([self.uName.get(), self.uDescription.get()], old_name, self.lstName)).grid(row=4, column=0)
            Button(nMem, text="Cancel", command=nMem.destroy).grid(row=4, column=1)


    def center_window(self, window):

        screen_width = window.winfo_screenwidth()
        screen_height = window.winfo_screenheight()

        requested_width = window.winfo_reqwidth()
        requested_height = window.winfo_reqheight()

        x_position = (screen_width - requested_width) // 2
        y_position = (screen_height - requested_height) // 2

        window.geometry(f"+{x_position}+{y_position}")

    def remove(self):
        value = self.lstName.curselection()[0]
        templist = self.loaded_list[value]

        self.delete_data(templist[0])
        self.loaded_list.remove(templist)

        self.lstName.delete(0, END)
        self.txt.delete(1.0, END)

        for line in self.loaded_list:
            categoryName = line[0]
            self.lstName.insert('end', categoryName)

    def register(self):
        list1 = [self.uName.get(), self.uDescription.get()]
        self.loaded_list.append(list1)
        self.insert_data(list1)
        self.lstName.insert(END, list1[0])

#Inventory class by Ali Mahmoud        
class Inventory:
    def __init__(self, root, login_instance):
        self.root = root
        self.root.title("Inventory")

        login_user = self.get_user_by_id(login_instance)
        self.user_id = login_user.id

        self.center_window(self.root)

        self.root.geometry("300x100")
        self.root.resizable(False, False)

        frame = Frame(self.root)
        frame.place(relx=0.5, rely=0.5, anchor=CENTER)

        Button(frame, text="Items", command=self.open_food_items, width=20).pack(pady=10)
        Button(frame, text="Categories", command=self.open_food_category, width=20).pack(pady=10)

        # SQLite database setup
        engine = create_engine('sqlite:///restaurant.db', echo=True)
        Base.metadata.create_all(engine)
        Session = sessionmaker(bind=engine)
        self.session = Session()
    
    def get_user_by_id(self, user_id):
        self.session = Session()
        user = self.session.query(User).filter_by(id=user_id).first()
        session.close()
        return user

    def center_window(self, window):
    
        screen_width = window.winfo_screenwidth()
        screen_height = window.winfo_screenheight()

        requested_width = window.winfo_reqwidth()
        requested_height = window.winfo_reqheight()

        x_position = (screen_width - requested_width) // 2
        y_position = (screen_height - requested_height) // 2

        window.geometry(f"+{x_position}+{y_position}")

    def open_food_items(self):
        self.food_items_window = Toplevel(self.root)
        self.center_window(self.food_items_window)
        self.root.iconify()
        self.food_items_window.resizable(False,  False)
        FoodItemApp(self.food_items_window, self.session, self.user_id )
        self.food_items_window.protocol("WM_DELETE_WINDOW", lambda: self.deiconify_and_destroy(self.food_items_window))

    def open_food_category(self):
        self.food_category_window = Toplevel(self.root)
        self.center_window(self.food_category_window)
        self.root.iconify()
        self.food_category_window.geometry("500x200")
        self.food_category_window.resizable(False,  False)
        FoodCategoryApp(self.food_category_window, self.session, self.user_id)
        self.food_category_window.protocol("WM_DELETE_WINDOW", lambda: self.deiconify_and_destroy(self.food_category_window))
        

    def deiconify_and_destroy(self,window):
        window.destroy()
        self.root.deiconify()

#Reservations class by Jubril shittu
class Reservations:
    def __init__(self, root, login_instance):
        self.root = root
        self.root.title("RESERVATIONS")
        self.root.minsize(width=500, height=250)
        self.root.resizable(False, False)

        login_user = self.get_user_by_id(login_instance)
        self.user_role  =  login_user.role
        self.user_branch = login_user.branch
        self.user_restaurant = login_user.restaurant_location
        
        self.session = Session()
        
        
        # GUI elements
        self.label_date = tk.Label(root, text="Select reservation date:")
        self.entry_date = DateEntry(root, width=12, background='darkblue', borderwidth=2, mindate=dt.date.today())
        self.label_branch = tk.Label(root, text="Choose branch(scroll for more branches):")

        self.selected_branch = tk.StringVar()
        self.branches_listbox = ttk.Combobox(root, textvariable=self.selected_branch , state= "readonly")
        self.populate_branches_listbox()
      
        self.selected_restaurant = tk.StringVar()

        restaurant_label = tk.Label(self.root, text="Select Restaurant:")

        self.restaurant_dropdown = ttk.Combobox(self.root, textvariable=self.selected_restaurant, state= "readonly")

        self.restaurant_dropdown.bind("<<ComboboxSelected>>", self.enable_restaurant_dropdown)
        self.branches_listbox.bind("<<ComboboxSelected>>", self.enable_restaurant_dropdown)

        entry_button = tk.Frame(self.root)

        self.table_amount_label = tk.Label(root, text="Input number of tables:")
        self.table_amount_entry = tk.Entry(entry_button)

        # Validate the table_amount for numeric values only
        vcmd = root.register(self.validate_table_amount)
        self.table_amount_entry.config(validate="key", validatecommand=(vcmd, "%P"))

        self.button_reserve = tk.Button(entry_button, text="RESERVE ", command=self.reserve_table)


        self.label_options= tk.Label(root, text="Other options:")

        # Layout
        self.label_date.pack()
        self.entry_date.pack()
        self.label_branch.pack()
        self.branches_listbox.pack()
        restaurant_label.pack()
        self.restaurant_dropdown.pack()
        self.table_amount_label.pack()
        entry_button.pack()
        self.table_amount_entry.pack(side = tk.LEFT)
        self.button_reserve.pack(side = tk.RIGHT)

        self.label_options.pack()

        self.button_show_reservations = tk.Button(root, text="VIEW RESERVATIONS", command=self.show_reservations)
        self.button_show_reservations.pack()

        self.button_show_tables_left = tk.Button(root, text="CHECK TABLE AVAILABILITY FOR SPECIFIC DATE", command=self.show_tables_left_on_date)
        self.button_show_tables_left.pack()

        self.center_window(self.root)
        
    
    def get_user_by_id(self, user_id):
        self.session = Session()
        user = self.session.query(User).filter_by(id=user_id).first()
        session.close()
        return user
        
    def enable_restaurant_dropdown(self, event):
        selected_index = self.branches_listbox.get()
        
        if selected_index:       
            restaurants = self.get_all_restaurants()
            self.restaurant_dropdown['values'] = restaurants
            self.restaurant_dropdown['state'] = 'readonly'
    
    def get_all_restaurants(self):       
        try:
            restaurants = self.session.query(distinct(Table.branch_number)).all()
            restaurant_names = [restaurant[0] for restaurant in restaurants]

            return restaurant_names
        except sqlalchemy.exc.SQLAlchemyError as e:
            messagebox.showerror("Error", f"SQLAlchemy error: {e}")
            return []
        finally:
            self.session.close()

    def center_window(self, window, reference_window=None):
    # Get the screen width and height
        screen_width = window.winfo_screenwidth()
        screen_height = window.winfo_screenheight()

        if reference_window:
        # Center the window relative to the reference window
            x_position = reference_window.winfo_x() + (reference_window.winfo_width() - window.winfo_reqwidth()) // 2
            y_position = reference_window.winfo_y() + (reference_window.winfo_height() - window.winfo_reqheight()) // 2
        else:
        # Center the window on the screen, considering minsize
            min_width, min_height = window.minsize()
            x_position = (screen_width - max(window.winfo_reqwidth(), min_width)) // 2
            y_position = (screen_height - max(window.winfo_reqheight(), min_height)) // 2

    # Set the window position
        window.geometry(f"+{x_position}+{y_position}")


    def validate_table_amount(self, new_value):
        # Validate that the input for table_amount contains only numeric values
        if new_value.isdigit():
            return int(new_value) >= 1
        return new_value == ""
    
    def reset_fields(self):
        # Reset the input fields after successfully reserving a table
        self.entry_date.delete(0, tk.END)
        self.branches_listbox.set('')
        self.table_amount_entry.delete(0, tk.END)

    
    def populate_branches_listbox(self):
    # Fetch branches from the 'tables' table and populate the listbox
        branches = self.get_branches_from_tables()
        today = date.today()

        if not branches:
            messagebox.showerror("Error", "No branches found in the 'tables' table.")
            return

        self.branches_listbox['values'] = branches


    def check_capacity(self, table_amount, selected_branch, reservation_date, selected_restaurant):
        try:
        # Create a new session
            with Session() as session:
                table_amount = int(table_amount)

            # Query the 'tables' table to check the remaining capacity
                table_entry = session.query(Table).filter(
                    Table.branch == selected_branch,
                    Table.reset_date == reservation_date,
                    Table.branch_number == selected_restaurant
                ).one_or_none()

                if table_entry:
                    remaining_capacity = table_entry.table_amount
                    if remaining_capacity >= table_amount:
                        return remaining_capacity
                    elif remaining_capacity <= 0:
                        messagebox.showerror("Error", "No tables available")
                    else:
                        messagebox.showerror("Error", f"Only {remaining_capacity} tables available")
                else:
                # Insert a new row if it doesn't exist
                    new_table_entry = Table(branch=selected_branch, table_amount=100, reset_date=reservation_date, branch_number=selected_restaurant)
                    session.add(new_table_entry)
                    session.commit()

                # Query again to get the newly inserted entry
                    updated_entry = session.query(Table).filter(
                        Table.branch == selected_branch,
                        Table.reset_date == reservation_date,
                        Table.branch_number == selected_restaurant
                    ).one_or_none()
                    remaining_capacity = updated_entry.table_amount

                    if remaining_capacity >= table_amount:
                        return remaining_capacity
                    else:
                        messagebox.showerror("Error", f"Only {remaining_capacity} tables available")

        except Exception as e:
            print(e)
            messagebox.showerror("Error", f"Error checking capacity: {e}")

        finally:
            if session:
                session.close()  # Close the session
        return None

    def reserve_table(self):
        number_of_tables = 100

        date = self.entry_date.get_date()
        selected_branch = self.branches_listbox.get()
        table_amount = self.table_amount_entry.get()
        print(table_amount)

        if not date or not selected_branch or not table_amount:
            messagebox.showerror("Error", "Please enter all fields")
            return

        # Get the selected branch from the listbox
    
        # Retrieve the selected restaurant
        selected_restaurant = self.selected_restaurant.get()

        remaining_capacity = self.check_capacity(table_amount, selected_branch, date,  selected_restaurant )
        if remaining_capacity is not None:
            # Reserve table
            table_entry = self.session.query(Table).filter(Table.branch == selected_branch, Table.reset_date == date, Table.branch_number == selected_restaurant).one_or_none()

            try:
                # Update the table amount in the 'tables' table
                if table_entry:
                    table_entry.table_amount -= int(table_amount)
                else:
                    # Insert a new row if it doesn't exist
                    new_table_entry = Table(branch=selected_branch, table_amount=number_of_tables, reset_date=date, branch_number=selected_restaurant)
                    self.session.add(new_table_entry)

                    # Update the 'table_amount' in the 'tables' table for the current branch, date, and restaurant
                    self.session.query(Table).filter(Table.branch == selected_branch, Table.reset_date == date, Table.branch_number == selected_restaurant).update({Table.table_amount: Table.table_amount - int(table_amount)})

                # Insert the reservation into the 'reservations' table
                new_reservation = Reservation(date=date, branch=selected_branch, table_amount=int(table_amount), branch_number=selected_restaurant)

                if table_entry:
                    new_reservation.table_id = table_entry.id

                self.session.add(new_reservation)

                messagebox.showinfo("Success", "Table reserved successfully")
                self.reset_fields()
                self.session.commit()
            except Exception as e:
                messagebox.showerror("Error", f"Error reserving table: {e}")
            finally:
                if self.session:
                    self.session.close()
        else:
            print("Capacity check failed")

    def get_table_id(self, branch, date, branch_number):
        try:
            with Session() as session:
                result = session.query(Table.id).filter_by(branch=branch, reset_date=date, branch_number=branch_number).scalar()
                if result:
                    return result
                else:
                    return None
        except sqlalchemy.exc.SQLAlchemyError as e:
            messagebox.showerror("Error", f"SQLAlchemy error: {e}")
            return None


    def show_reservations(self):
    # Create a new window for displaying reservations
        self.reservations_window = tk.Toplevel(self.root)
        self.reservations_window.minsize(width=500, height=350)
        self.reservations_window.title("RESERVATION DETAILS")
        self.center_window(self.reservations_window)
        self.root.iconify()

        self.reservations_window.protocol("WM_DELETE_WINDOW", self.show_first_window)

    # Create a search bar
        search_frame = ttk.Frame(self.reservations_window)
        search_frame.pack(side=tk.TOP, expand=True, fill=tk.X, pady=10)

        search_label = tk.Label(search_frame, text="Enter Reservation ID:")
        search_label.pack(side=tk.LEFT, padx=10)

        search_entry = tk.Entry(search_frame)
        search_entry.pack(side=tk.LEFT, padx=10)

        search_button = tk.Button(search_frame, text="Search", command=lambda: self.search_reservation(search_entry.get()))
        search_button.pack(side=tk.LEFT, padx=10)

    # Create a Listbox and Scrollbar in the top frame
        frame1 = ttk.Frame(self.reservations_window)
        frame1.pack(side=tk.TOP, expand=True, fill=tk.BOTH)

        reservations_listbox = tk.Listbox(frame1, selectmode=tk.SINGLE)
        scrollbar = tk.Scrollbar(frame1, command=reservations_listbox.yview)
        reservations_listbox.config(yscrollcommand=scrollbar.set)
        self.reservation_listbox = reservations_listbox

    # Populate Listbox with reservations data
        self.populate_reservations_listbox(reservations_listbox)

    # Pack Listbox and Scrollbar
        reservations_listbox.pack(expand=True, fill=tk.BOTH)
        scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

    # Create a frame for the delete button in the bottom frame
        frame2 = ttk.Frame(self.reservations_window)
        frame2.pack(side=tk.BOTTOM)

    # Add an "Update Reservation" button
        update_button = tk.Button(frame2, text="UPDATE RESERVATION", command=self.update_reservation)
        update_button.pack()

    # Add a "Delete Reservation" button
        delete_button = tk.Button(frame2, text="CANCEL RESERVATION", command=lambda: self.delete_reservation(reservations_listbox))
        delete_button.pack()

    # Add a "Go Back" button
        go_back_button = tk.Button(frame2, text="Go Back", command=self.show_first_window)
        go_back_button.pack()

    def search_reservation(self, reservation_id):
        # Search for the reservation with the given ID and display details
        if self.user_role == "manager" or "HR Director":
            if reservation_id.isdigit():
                reservation_id = int(reservation_id)

                result = self.session.query(Reservation.id, Reservation.date, Reservation.branch, Reservation.table_amount, Reservation.branch_number) \
                    .filter(Reservation.id == reservation_id).one_or_none()
                self.session.close()

                if result:
                    reservation_details = f"Reservation ID: {result.id}  Date: {result.date}  Branch: {result.branch}  Tables: {result.table_amount} Restaurant Number: {result.branch_number}"
                    messagebox.showinfo("Reservation Details", reservation_details)
                else:
                    messagebox.showinfo("Reservation Details", f"No reservation found with ID {reservation_id}")
            else:
                messagebox.showerror("Invalid Input", "Please enter a valid Reservation ID.")
        else:
            if reservation_id.isdigit():
                reservation_id = int(reservation_id)

                result = self.session.query(Reservation.id, Reservation.date, Reservation.branch, Reservation.table_amount, Reservation.branch_number) \
                    .filter(Reservation.id == reservation_id, Reservation.branch == self.user_branch).one_or_none()
                self.session.close()

                if result:
                    reservation_details = f"Reservation ID: {result.id}  Date: {result.date}  Branch: {result.branch}  Tables: {result.table_amount} Restaurant Number: {result.branch_number}"
                    messagebox.showinfo("Reservation Details", reservation_details)
                else:
                    messagebox.showinfo("Reservation Details", f"No reservation found with ID {reservation_id}")
            else:
                messagebox.showerror("Invalid Input", "Please enter a valid Reservation ID.")
     

    def populate_reservations_listbox(self, listbox):
        if self.user_role == "manager" or " HR director":
            try:
            # Open a new session
                with Session() as session:
                # Fetch reservations from the 'reservations' table
                    reservations = session.query(Reservation.id, Reservation.date, Reservation.branch, Reservation.table_amount, Reservation.branch_number).all()

                # Populate the listbox with reservation information
                    for reservation in reservations:
                        reservation_details = f"Reservation ID: {reservation.id}  Date: {reservation.date}  Branch: {reservation.branch}  Tables: {reservation.table_amount} Branch Number: {reservation.branch_number}"
                        listbox.insert(tk.END, reservation_details)

            except sqlalchemy.exc.SQLAlchemyError as e:
                messagebox.showerror("Error", f"SQLAlchemy error: {e}")
            finally:
                session.close()
        else:
            try:
                # Use SQLAlchemy to get distinct branch names from the 'tables' table
                reservations  = self.session.query(Reservation.id, Reservation.date, Reservation.branch, Reservation.table_amount, Reservation.branch_number).filter(Reservation.branch == self.user_branch).all()

                for reservation in reservations:
                        reservation_details = f"Reservation ID: {reservation.id}  Date: {reservation.date} Tables: {reservation.table_amount} Branch Number: {reservation.branch_number}"
                        listbox.insert(tk.END, reservation_details)
            except sqlalchemy.exc.SQLAlchemyError as e:
                messagebox.showerror("Error", f"SQLAlchemy error: {e}")
                return []
            finally:
                self.session.close()

    def update_reservation(self):
        
    # Get the reservation ID from the use
        date = self.entry_date.get_date()
        selected_index = self.reservation_listbox.curselection()
        if selected_index:
            index = selected_index[0]  # Extract the index from the tuple
            selected_item = self.reservation_listbox.get(index)
            match = re.search(r'\b(\d+)\b', selected_item)
            if match:
                reservation_id_str = match.group(1)

            reservation_id = int(reservation_id_str)

            if reservation_id is None:
                return  # User clicked Cancel

            if not self.check_reservation_id_exists(reservation_id):
                messagebox.showerror("Error", f"Reservation does not exist.")
                return
        
            result = (
                        self.session.query(Reservation.branch, Reservation.table_id, Reservation.table_amount, Reservation.date, Reservation.branch_number)
                            .filter(Reservation.id == reservation_id)
                            .first()
                    )

            if not result:
                messagebox.showerror("Error", f"Reservation with ID {reservation_id} not found.")

            existing_branch, existing_table_id, existing_table_amount, existing_date, existing_branch_number = result
        else:
            messagebox.showinfo("Selecy", "Please select a reservation")
            return False

    # Get the column to update and the new value
        while True:
            column_to_update = simpledialog.askstring("Input", "Enter what to update ('date' 'branch' 'table_amount' or 'branch_number') in that format:")
            if not column_to_update:
                return  # User clicked Cancel

            try:
                inspector = inspect(engine)
                columns = [column["name"] for column in inspector.get_columns("reservations")]

                if column_to_update not in columns:
                    messagebox.showerror("Error", f"The column '{column_to_update}' does not exist. Column inputted has to match any of the four given")
                    continue
                else:
                    break
            except sqlalchemy.exc.SQLAlchemyError as e:
                messagebox.showerror("Error", f"SQLAlchemy error: {e}")
                return

    # If the column to update is 'branch', provide a dropdown menu with options from the 'tables' table
        if column_to_update == 'branch':
    # Fetch branches from the 'tables' table and create a list for the dropdown menu
            branches = self.get_branches_from_tables()
            if not branches:
                messagebox.showerror("Error", "No branches found in the 'tables' table.")
                return

    # Create a top-level window for the dropdown menu
            dropdown_window = tk.Toplevel(self.root)
            dropdown_window.title("UPDATE BRANCH")
            name = tk.Label(dropdown_window, text="Choose new branch:")
            name.pack()

    # Create a StringVar to hold the selected branch
            selected_branch = tk.StringVar()

    # Create a dropdown menu with the branches
            branch_menu = ttk.Combobox(dropdown_window, textvariable=selected_branch, values=branches)
            branch_menu.pack(padx=10, pady=10)

    # Add an "OK" button to confirm the selection
            ok_button = tk.Button(dropdown_window, text="OK", command=lambda: dropdown_window.destroy())
            ok_button.pack(pady=5)


            with Session() as session:
                ReservationAlias = aliased(Reservation)
                result = (
                    session.query(Reservation.table_amount, Reservation.table_id, Reservation.branch_number)
                    .filter(Reservation.id == reservation_id)
                    .first()
                )
            if result:
                table_from_result, old_table_id, branch_number= result
                print(table_from_result)
                print (old_table_id)

            # Wait for the dropdown window to be closed
            dropdown_window.wait_window()

            # Retrieve the selected branch
            new_value = selected_branch.get()

            # Get the new table_id for the selected branch
            new_table_id = self.get_table_id(new_value, date, branch_number)
            print(new_table_id )

            with Session() as session:
                reservation = session.query(Reservation).filter_by(id=reservation_id).first()
                reservation.branch = new_value

                reservation.table_id = new_table_id

                old_table = session.query(Table).filter_by(id=old_table_id).first()
                old_table.table_amount += table_from_result

                # Check if the entry for the new branch and date already exists in the 'tables' table
                new_table_entry = session.query(Table).filter_by(branch=new_value, reset_date=date).first()

                if new_table_entry:
                    # If the record exists, update the 'table_amount'
                    new_table_entry.table_amount -= table_from_result
                else:
                    # If the record does not exist, insert a new row
                    new_table_entry = Table(
                        branch=new_value,
                        reset_date=date,
                        table_amount=(100 - int(table_from_result)), 
                        branch_number = branch_number
                    )
                    session.add(new_table_entry)

                session.commit()
                messagebox.showinfo("Success", "Branch successfully updated")
                    

        elif column_to_update == 'date':
            # If the column to update is 'date', use DateEntry for input
            new_value = self.ask_for_date_entry()
            print(new_value)
            if new_value is None:
                return 
            try:
                if not new_value and column_to_update != 'date':
                    raise ValueError("Please enter a new value.")

                # Convert the new_value to a Python date object
                new_date = datetime.strptime(new_value, '%Y-%m-%d').date()

                with Session() as session:
                    session.execute(
                        update(Reservation)
                        .where(Reservation.id == reservation_id)
                        .values({Reservation.date: new_date})
                    )
                    
                    existing_table_entry = session.query(Table).filter_by(branch=existing_branch, reset_date=new_value).first()

                    if existing_table_entry:
                # If the record exists, update the 'table_amount'
                        session.execute(
                        update(Table)
                        .where((Table.branch == existing_branch) & (Table.reset_date == existing_date))
                        .values({Table.table_amount: Table.table_amount +  existing_table_amount})
                        )

                        session.execute(
                        update(Table)
                        .where((Table.branch == existing_branch) & (Table.reset_date == new_date))
                        .values({Table.table_amount: Table.table_amount - existing_table_amount})
                        )

                    
                    else:
                # If the record does not exist, insert a new row
                        session.execute(
                        update(Table)
                        .where((Table.branch == existing_branch) & (Table.reset_date == existing_date))
                        .values({Table.table_amount: Table.table_amount +  existing_table_amount})
                        )

                        new_table_entry = Table(
                                                branch=existing_branch, 
                                                reset_date = datetime.strptime(new_value, '%Y-%m-%d').date(), 
                                                table_amount=(100 - int(existing_table_amount))
                                                )
                        
                        session.execute(insert(Table).values(
                            branch=new_table_entry.branch,
                            table_amount=new_table_entry.table_amount,
                            reset_date=new_table_entry.reset_date)
                            )
                                            
                    session.commit()             

                print(f"Updated reservation in the database: {reservation_id}")

                messagebox.showinfo("Success", "Date successfully updated ")
                print("Reservation updated successfully.")
            except ValueError as ve:
                messagebox.showerror("Error", str(ve))
                print(f"Error: {ve}")

            except sqlite3.Error as e:
                messagebox.showerror("Error", f"SQLite error: {e}")
                print(f"Error: {e}")

        elif column_to_update == 'table_amount':
    # Ask the user if they want to add or remove tables
            add_or_remove = simpledialog.askstring("Input", "Do you want to add or remove tables?")

            if not add_or_remove:
                return   # User clicked Cancel

    # Prompt the user for the value to add or remove
            try:
                if add_or_remove =="add":
                    user_input_value = simpledialog.askinteger("Input", "Enter the value to add")
                    if user_input_value is None:
                        return  
                    
                elif add_or_remove =="ADD":
                    user_input_value = simpledialog.askinteger("Input", "Enter the value to add")
                    if user_input_value is None:
                        return  
                
                elif add_or_remove == "remove":
                    user_input_value = simpledialog.askinteger("Input", "Enter the value to remove")
                    if user_input_value is None:
                        return 
                    
                elif add_or_remove == "REMOVE":
                    user_input_value = simpledialog.askinteger("Input", "Enter the value to remove")
                    if user_input_value is None:
                        return 
            except ValueError:
                messagebox.showerror("Error", "Invalid input. Please enter a valid numeric value.")
                return

    # Get the branch and table_id from the reservation information
            with Session() as session:
                result = (
                    session.query(Reservation.branch, Reservation.table_id)
                    .filter(Reservation.id == reservation_id)
                    .first()
                )
            if not result:
                messagebox.showerror("Error", f"Reservation with ID {reservation_id} not found.")
                return

            existing_branch, existing_table_id = result

            with session:
                if add_or_remove.lower() == 'add':
            # Increase the available tables in the 'tables' table and update the reservation
                    with Session() as session:
    # Update table_amount in 'tables' table
                        session.execute(
                            update(Table)
                            .where((Table.branch == existing_branch) & (Table.id == existing_table_id))
                            .values({Table.table_amount: Table.table_amount - user_input_value})
                        )
                        session.commit()

    # Update table_amount in 'reservations' table
                        session.execute(
                            update(Reservation)
                            .where(Reservation.id == reservation_id)
                            .values({Reservation.table_amount: Reservation.table_amount + user_input_value})
                        )
                        session.commit()
                        
                    messagebox.showinfo("Added", "Table successfully added")
                    print(f"Added tables to the 'tables' table and updated reservation: {reservation_id}")
                    print(f"Added tables back to the 'tables' table for branch: {existing_branch}, id: {existing_table_id}")
                elif add_or_remove.lower() == 'remove':
            # Decrease the available tables in the 'tables' table and update the reservation
                    with Session() as session:
    # Update table_amount in 'tables' table
                        session.execute(
                            update(Table)
                            .where((Table.branch == existing_branch) & (Table.id == existing_table_id))
                            .values({Table.table_amount: Table.table_amount - user_input_value})
                        )
                        session.commit()

    # Update table_amount in 'reservations' table
                        session.execute(
                            update(Reservation)
                            .where(Reservation.id == reservation_id)
                            .values({Reservation.table_amount: Reservation.table_amount - user_input_value})
                        )
                        session.commit()
                    messagebox.showinfo("Added", "Table successfully removed")
                    print(f"Removed tables from the 'tables' table and updated reservation: {reservation_id}")
                else:
                    messagebox.showerror("Error", "Invalid choice. Please choose 'add' or 'remove'.")
                    return
        elif column_to_update == 'branch_number':
            try:
                branch_numbers_query = self.session.query(distinct(Table.branch_number)).all()

        # Extract branch numbers from the query result
                branch_numbers = [str(branch_number[0]) for branch_number in branch_numbers_query]

                if not branch_numbers:
                    messagebox.showerror("Error", "No branch numbers found in the 'tables' table.")
                    return

        # Create a top-level window for the dropdown menu
                dropdown_window = tk.Toplevel(self.root)
                dropdown_window.title("UPDATE BRANCH NUMBER")
                name = tk.Label(dropdown_window, text="Choose new branch number:")
                name.pack()

        # Create a StringVar to hold the selected branch number
                selected_branch_number = tk.StringVar()

        # Create a dropdown menu with the dynamically fetched branch numbers
                branch_number_menu = ttk.Combobox(dropdown_window, textvariable=selected_branch_number, values=branch_numbers)
                branch_number_menu.pack(padx=10, pady=10)

        # Add an "OK" button to confirm the selection
                ok_button = tk.Button(dropdown_window, text="OK", command=lambda: dropdown_window.destroy())
                ok_button.pack(pady=5)

        # Wait for the dropdown window to be closed
                dropdown_window.wait_window()

                new_value = selected_branch_number.get()

        # Update branch_number in 'tables' table
                self.session.execute(
                    update(Table)
                    .where((Table.branch == existing_branch) & (Table.id == existing_table_id))
                    .values({Table.branch_number: new_value})
                )
                self.session.commit()

        # Update branch_number in 'reservations' table
                self.session.execute(
                    update(Reservation)
                    .where(Reservation.id == reservation_id)
                    .values({Reservation.branch_number: new_value})
                )
                self.session.commit()

                messagebox.showinfo("Updated", "Branch number successfully updated")

            except sqlalchemy.exc.SQLAlchemyError as e:
                self.session.rollback()  # Rollback the transaction in case of an error
                messagebox.showerror("Error", f"SQLAlchemy error: {e}")
            finally:
                self.session.close()


    def get_branches_from_tables(self):
        if self.user_role == "manager" or "HR Director":
            try:
                # Use SQLAlchemy to get distinct branch names from the 'tables' table
                branches = self.session.query(distinct(Table.branch)).all()

                # Extract branch names from the query result
                branch_names = [branch[0] for branch in branches]

                return branch_names
            except sqlalchemy.exc.SQLAlchemyError as e:
                messagebox.showerror("Error", f"SQLAlchemy error: {e}")
                return []
            finally:
                self.session.close()
        else:
            try:
                # Use SQLAlchemy to get distinct branch names from the 'tables' table
                branches = self.session.query(distinct(Table.branch)).filter(Table.branch == self.user_branch).all()


                # Extract branch names from the query result
                branch_names = [branch[0] for branch in branches]

                return branch_names
            except sqlalchemy.exc.SQLAlchemyError as e:
                messagebox.showerror("Error", f"SQLAlchemy error: {e}")
                return []
            finally:
                self.session.close()
        
    
    def ask_for_date_entry(self):
    # Create a top-level window for the DateEntry widget
        date_window = tk.Toplevel(self.root)
        date_window.title("UPDATE DATE")
        name = tk.Label(date_window, text="Choose new date:")
        name.pack()
        

    # Create a DateEntry widget
        date_entry = DateEntry(date_window, width=12, background='darkblue', borderwidth=2, mindate=dt.date.today())
        date_entry.pack(padx=10, pady=10)

        def on_ok():
        # Retrieve the selected date
            new_date = date_entry.get_date()
        # Convert the date to a string if needed
            new_date_str = new_date.strftime('%Y-%m-%d')
            self.selected_date = new_date_str
            print(new_date_str)
            
            date_window.destroy()

    # Add an "OK" button to confirm the date selection
        ok_button = tk.Button(date_window, text="OK", command=on_ok)
        ok_button.pack(pady=5)

    # Wait for the date window to be closed
        date_window.wait_window()
        return self.selected_date

        
    
    def delete_reservation(self, listbox):
        # Get the selected index from the listbox
        selected_index = listbox.curselection()

        if selected_index:
            # Get the selected item from the listbox
            confirmation = messagebox.askyesno("Confirmation", f"Are you sure you want to delete item?")
            if confirmation: 
                selected_item = listbox.get(selected_index)

            # Extract the reservation ID from the selected item (assuming it's the first part of the information)
                reservation_id = self.extract_reservation_id(selected_item)

            # Fetch the table amount for the reservation
                table_amount = self.get_reservation_table_amount(reservation_id)

            # Delete the reservation from the database
                if reservation_id:
                    try:
                        with self.session.begin():
                        # Delete the reservation from the 'reservations' table
                            reservation_alias = aliased(Reservation)
                            self.session.query(reservation_alias).filter(reservation_alias.id == reservation_id).delete()


                    # Add the tables back to the 'tables' table
                        self.add_tables_back(selected_item, table_amount)

                        messagebox.showinfo("Deleted", "Reservation successfully cancelled")

                    # Delete the selected item from the listbox
                        listbox.delete(selected_index)
                        print(f"Deleted reservation from the listbox with ID: {reservation_id}")

                        self.session.commit()  # Commit the transaction

                    except sqlalchemy.exc.SQLAlchemyError as e:
                        self.session.rollback()  # Rollback the transaction in case of an error
                        messagebox.showerror("Error", f"SQLAlchemy error: {e}")

                    finally:
                        self.session.close()  # Close the session

                else:
                    print("Failed to extract reservation ID from the selected item")
            else:
                messagebox.showinfo("Canceled", "Canceled.")
        else:
            messagebox.showinfo("No selection", "Please select a reservation to cancel")
            print("No reservation selected")

    
    def get_reservation_table_amount(self, reservation_id):
        try:
            # Use SQLAlchemy to query the 'reservations' table
            result = self.session.query(Reservation.table_amount).filter(Reservation.id == reservation_id).scalar()
            return result if result is not None else 0

        except sqlalchemy.exc.SQLAlchemyError as e:
            messagebox.showerror("Error", f"SQLAlchemy error: {e}")
            return 0
        finally:
            self.session.close()
    
    def add_tables_back(self, reservation_info, table_amount):
        # Extract the branch and date from the reservation information
        branch = self.extract_branch_from_reservation(reservation_info)
        date_info = self.extract_date_from_reservation(reservation_info)
        table_amount = self.extract_table_amount_from_reservation(reservation_info)

        try:
            # Use SQLAlchemy to update the 'tables' table
            with self.session.begin():
                self.session.query(Table).filter(Table.branch == branch, Table.reset_date == date_info).\
                    update({Table.table_amount: Table.table_amount + table_amount})
                self.session.commit()
            print(f"Added tables back to the 'tables' table for branch: {branch}, date: {date_info}")

        except sqlalchemy.exc.SQLAlchemyError as e:
            messagebox.showerror("Error", f"SQLAlchemy error: {e}")
        
    def get_branch_numbers_from_tables(self):
        try:
        # Use SQLAlchemy to get distinct branch numbers from the 'tables' table
            branch_numbers = self.session.query(distinct(Table.branch_number)).all()

        # Extract branch numbers from the query result
            branch_numbers_list = [branch_number[0] for branch_number in branch_numbers]

            return branch_numbers_list
        except sqlalchemy.exc.SQLAlchemyError as e:
            messagebox.showerror("Error", f"SQLAlchemy error: {e}")
            return []
        finally:
            self.session.close()


    def show_tables_left_on_date(self):
    # Get the user input for a specific date
        specific_date = simpledialog.askstring("Input", "Enter the date to check (YYYY-MM-DD):")

        if not specific_date:
            return  # User clicked Cancel

        try:
        # Fetch all branches and branch numbers
            entered_date = datetime.strptime(specific_date, '%Y-%m-%d').date()
            today = datetime.now().date()

            if entered_date < today:
                messagebox.showerror("Invalid Date", "Past date not allowed.")
                return
            all_branches = self.get_branches_from_tables()
            all_branch_numbers = self.get_branch_numbers_from_tables()

            info_message = ""

        # Display information for each branch
            for branch in all_branches:
            # Display information for each branch number
                for branch_number in all_branch_numbers:
                # Use SQLAlchemy to query the 'tables' table
                    result = self.session.query(Table.table_amount).filter(
                        Table.reset_date == specific_date,
                        Table.branch == branch,
                        Table.branch_number == branch_number
                    ).first()

                    parse(specific_date)

                    if result is not None:
                        tables_left_value = str(result[0]).strip('(),')
                        info_message += f"Branch: {branch}, Branch Number: {branch_number}, Tables Left: {tables_left_value}\n"
                    else:
                    # If information is not available in the database, assume Tables Left: 100
                        info_message += f"Branch: {branch}, Branch Number: {branch_number}, Tables Left: 100\n"

        # Show all information in a single messagebox
            if info_message:
                messagebox.showinfo("Tables Left on Date", info_message)
            else:
                messagebox.showinfo("Tables Left on Date", f"No information found for {specific_date}")

        except ValueError:
            messagebox.showerror("Error", "Invalid date. Please enter a valid date in the format YYYY-MM-DD.")

        except sqlalchemy.exc.SQLAlchemyError as e:
            messagebox.showerror("Error", f"SQLAlchemy error: {e}")
        finally:
            self.session.close()

    def extract_branch_from_reservation(self, reservation_info):
    # Extract the branch from the reservation information
        if "Branch:" in reservation_info:
            branch_part = reservation_info.split("Branch:")[1].strip()
            return branch_part.split()[0]

        return None
    
    
    def extract_table_amount_from_reservation(self, reservation_info):
    # Extract the table amount from the reservation information
        if "Tables:" in reservation_info:
            table_part = reservation_info.split("Tables:")[1].strip()
            return table_part.split()[0]

        return None
    
    def extract_date_from_reservation(self, reservation_info):
    # Extract the date from the reservation information
        if "Date:" in reservation_info:
            date_part = reservation_info.split("Date:")[1].strip()
            return date_part.split()[0]

        return None

    def check_reservation_id_exists(self, reservation_id):
    # Check if the reservation ID exists in the reservations table

        try:
        # Use SQLAlchemy to check if a reservation with the given ID exists
            exists_query = self.session.query(exists().where(Reservation.id == reservation_id))
            return exists_query.scalar()
        except sqlalchemy.exc.SQLAlchemyError as e:
            messagebox.showerror("Error", f"SQLAlchemy error: {e}")
            return False
        finally:
            self.session.close()
        

    def extract_reservation_id(self, reservation_info):
        # Check if the information string starts with "Reservation ID:" and extract the ID
        if reservation_info.startswith("Reservation ID:"):
            parts = reservation_info.split()

            # Check if the ID is present after "Reservation ID:" and is a valid integer
            if len(parts) > 2 and parts[2].isdigit():
                return int(parts[2])

        return None


    def show_first_window(self):
        if hasattr(self, 'reservations_window'):  # Check if the window has been created
            self.reservations_window.destroy()  
        self.root.deiconify()  
        self.root.title("RESERVATIONS")
        self.root.minsize(width=500, height=200)
        self.root.resizable(False, False)  # Reset the title if needed
        self.center_window(self.root)

#Report class by Yerim 
class Report:
    def __init__(self, root, login_instance):
        self.root = root
        self.root.title("REPORT")
        self.session = Session()

        login_user = self.get_user_by_id(login_instance)
        self.user_role  = login_user.role
        self.user_restaurant = login_user.restaurant_location
        self.user_branch = login_user.branch

        self.report_type_var = tk.StringVar()

        self.custom_report_button = tk.Button(
            self.root, text="Generate Custom Report", command=self.get_selected_branches
        )
        self.custom_report_button.pack(pady=10)

    def generate_custom_report(self, branch):
        try:
            self.branch = branch

            report_type_window = tk.Toplevel(self.root)
            report_type_window.title("Select Report Type")
            self.center_window(report_type_window)
            report_type_window.resizable(False, False)

            report_type_label = tk.Label(report_type_window, text="Select Report Type:")
            report_type_label.pack(pady=5)
  
            report_type_options = ["Sales", "Reservations", "Users","Inventory stock","Monthly Expenditure"]

        # Use the existing self.report_type_var
            self.report_type_var.set("")  # Clear any previous value
            report_type_combobox = ttk.Combobox(
                report_type_window, textvariable=self.report_type_var, values=report_type_options, state="readonly"
            )
            report_type_combobox.pack(pady=5)

            def on_ok():
                try:
                    report_type = self.report_type_var.get()
                    branch = self.branch
                    report_type_window.destroy()
                    self.generate_custom_report_with_branch(branch, report_type )
                except tk.TclError as e:
                    print(f"Error in on_ok: {e}")

            ok_button = tk.Button(report_type_window, text="OK", command=on_ok)
            ok_button.pack(pady=10)

        # Bind the protocol method for the WM_DELETE_WINDOW event (window close)
            report_type_window.protocol("WM_DELETE_WINDOW", lambda: self.close_window(report_type_window))

        # Wait for the report type window to be closed
            report_type_window.wait_window()
            self.custom_report_window_open = True

        except Exception as e:
            print(f"Error in generate_custom_report: {e}")
            messagebox.showerror("Error", f"An error occurred: {e}")

        finally:
            self.session.close()
    
    def get_user_by_id(self, user_id):
        self.session = Session()
        user = self.session.query(User).filter_by(id=user_id).first()
        session.close()
        return user

    def generate_custom_report_with_branch(self, selected_branch, report_type):
        try:
            # Create a top-level window for the report
            
            report_window = tk.Toplevel(self.root)
            report_window.title(f"{report_type} Report")
            self.center_window(report_window)
            report_window.resizable(False, False)

            # Customize the report generation based on the selected parameters
            report_data = self.generate_report_data(report_type, [selected_branch])

            # Display the report on the screen
            self.display_report(report_window, report_data, report_type)

            # Save the report as a PDF file
            pdf_filename = f"{report_type}_Report.pdf"
            self.save_as_pdf(pdf_filename, report_data)

            messagebox.showinfo("Report Generated", f"Custom report '{report_type}' generated successfully.")

        except Exception as e:
            print(f"Error: {e}")
        finally:
            self.session.close()

    def on_report_ok(self, selected_branch, report_type_combobox):
        report_type = report_type_combobox
        if report_type and selected_branch:

            # Customize the report generation based on the selected parameters
            report_data = self.generate_report_data(report_type, selected_branch)

            # Display the report on the screen
            self.display_report(report_data, report_type)

            # Save the report as a PDF file
            pdf_filename = f"{report_type}_Report.pdf"
            self.save_as_pdf(pdf_filename, report_data)

            messagebox.showinfo("Report Generated", f"Custom report '{report_type}' generated successfully.")
        else:
            messagebox.showinfo("Report Generation Canceled", "Custom report generation canceled.")

    def get_branches_from_tables(self):
        try:
            branches = self.session.query(distinct(Table.branch)).all()

            branch_names = [branch[0] for branch in branches]

            return branch_names
        except sqlalchemy.exc.SQLAlchemyError as e:
            print(f"Error fetching branches: {e}")
            return []
        finally:
            self.session.close()

    def get_branches_from_tables_for_managers(self):
        try:
            branches = self.session.query(distinct(Table.branch)).filter(Table.branch == self.user_branch).all()
            branch_names = [branch[0] for branch in branches]

            return branch_names
        except sqlalchemy.exc.SQLAlchemyError as e:
            print(f"Error fetching branches: {e}")
            return []
        finally:
            self.session.close()

    def get_selected_branches(self):
    # Use the Report class to get the branches dynamically
        self.root.iconify()
        if self.user_role == "manager":
            all_branches = self.get_branches_from_tables_for_managers()
        else:
            all_branches = self.get_branches_from_tables()

        dropdown_window = tk.Toplevel(self.root)
        dropdown_window.title("SELECT BRANCH")
        branches_combobox = ttk.Combobox(dropdown_window, values=all_branches, state="readonly")
        branches_combobox.pack(padx=10, pady=10)
        dropdown_window.resizable(False, False)

        self.center_window(dropdown_window)
        selected_branch_var = tk.StringVar()

        def on_ok(selected_branch_var):
            selected_branch_var.set(branches_combobox.get())
            branch = selected_branch_var.get()
            dropdown_window.destroy()
            self.generate_custom_report(branch)

        ok_button = tk.Button(dropdown_window, text="OK", command=lambda: on_ok(selected_branch_var))
        ok_button.pack(pady=10)

    # Bind the protocol method for the WM_DELETE_WINDOW event (window close)
        dropdown_window.protocol("WM_DELETE_WINDOW", lambda: self.close_window(dropdown_window))

        dropdown_window.wait_window()
        if selected_branch_var is None:
            return False
        else:
            return selected_branch_var.get()


    def generate_report_data(self, report_type, selected_branches):

        report_data = {"Selected Branches": selected_branches, " Data": []}

        if self.user_role == "manager":

            if report_type == "Sales":
            # Amount of orders currently placed
                total_orders = (
                self.session.query(func.count(Order.id))
                .filter(Order.branch == self.user_branch)
                .scalar()
            )
                total_amount_spent = (
                self.session.query(func.sum(Order.cost))
                .filter(Order.branch == self.user_branch)
                .scalar()
            )

                report_data[" Data"].extend([
                    {"Item": "Total Orders", "Value": total_orders if total_orders is not None else 0},
                    {"Item": "Total Amount Sales", "Value": total_amount_spent if total_amount_spent is not None else 0}
                ])

            elif report_type == "Reservations":
            # Amount of current reservations for each branch
                reservations_by_branch = {}
                for branch in selected_branches:
                    reservations_count = (
                        self.session.query(func.count(Reservation.id))
                        .filter(Reservation.branch == branch)
                        .scalar()
                    )
                    reservations_by_branch[branch] = reservations_count if reservations_count is not None else 0

                report_data[" Data"].append({"Item": "Reservations for this branch", "Value": reservations_by_branch})

            elif report_type == "Users":
            # Amount of users
                total_users = self.session.query(func.count(User.id)).filter(User.branch == self.user_branch).scalar()

                report_data[" Data"].append({"Item": "Total Workers", "Value": total_users if total_users is not None else 0})
                
            elif report_type == "Inventory stock":
                    # Get total quantity of each item in the FoodItem table
                    inventory_data = (
                        self.session.query(FoodItem.name, func.sum(FoodItem.quantity).label('total_quantity'))
                        
                        .group_by(FoodItem.name)
                        .all()
                    )
                    for item_data in inventory_data:
                        report_data[" Data"].append({"Item": item_data.name, "Value": item_data.total_quantity})
            elif report_type == "Monthly Expenditure":
    # Monthly expenditure for each food item in selected branches
                expenditure_data = (
                    self.session.query(
                        FoodItem.name,
                        func.sum(FoodItem.quantity * FoodItem.price).label('total_expenditure')
                    )
                     
                    .all()
                )
                for item_data in expenditure_data:
                    report_data[" Data"].append({
                        "Item": f"Expenditure ", 
                        "Value": item_data.total_expenditure if item_data.total_expenditure is not None else 0
                    })
            return report_data
        else:
            if report_type == "Sales":
                for branch in selected_branches:
                    total_orders = (
                    self.session.query(func.count(Order.id))
                    .filter(Order.branch == branch)
                    .scalar()
                )
                total_amount_spent = (
                    self.session.query(func.sum(Order.cost))
                    .filter(Order.branch == branch)
                    .scalar()
                )

                report_data[" Data"].extend([
                    {"Item": f"Total Orders ({branch})", "Value": total_orders if total_orders is not None else 0},
                    {"Item": f"Total Amount Sales ({branch})", "Value": total_amount_spent if total_amount_spent is not None else 0}
        ])
            elif report_type == "Reservations":
            # Amount of current reservations for each branch
                reservations_by_branch = {}
                for branch in selected_branches:
                    reservations_count = (
                        self.session.query(func.count(Reservation.id))
                        .filter(Reservation.branch == branch)
                        .scalar()
                    )
                    reservations_by_branch[branch] = reservations_count if reservations_count is not None else 0

                    report_data[" Data"].append({"Item": "Reservations for this branch", "Value": reservations_by_branch})

            elif report_type == "Users":
                for branch in selected_branches:
            # Amount of users
                    total_users = self.session.query(func.count(User.id)).filter(User.branch == branch).scalar()

                    report_data[" Data"].append({"Item": "Total Workers", "Value": total_users if total_users is not None else 0})
                
            elif report_type == "Inventory stock":
                    # Get total quantity of each item in the FoodItem table
                    inventory_data = (
                        self.session.query(FoodItem.name, func.sum(FoodItem.quantity).label('total_quantity'))
                        
                        .group_by(FoodItem.name)
                        .all()
                    )
                    for item_data in inventory_data:
                        report_data[" Data"].append({"Item": item_data.name, "Value": item_data.total_quantity})
            elif report_type == "Monthly Expenditure":
    # Monthly expenditure for each food item in selected branches
                expenditure_data = (
                    self.session.query(
                        FoodItem.name,
                        func.sum(FoodItem.quantity * FoodItem.price).label('total_expenditure')
                    )
                     
                    .all()
                )

                for item_data in expenditure_data:
                    report_data[" Data"].append({
                        "Item": f"Expenditure", 
                        "Value": item_data.total_expenditure if item_data.total_expenditure is not None else 0
                        
                    })
                    
            

            return report_data

    def display_report(self, window, report_data, report_type):
    # Create a frame to hold the labels
        frame = tk.Frame(window)
        frame.pack(pady=10)

        self.center_window(window)

    # Create and pack labels for the report data
        label_type = tk.Label(frame, text=f"{report_type} Report", font=("Arial", 14, "bold"))
        label_type.pack()

        label_branches = tk.Label(frame, text=f"Selected Branches: {', '.join(report_data['Selected Branches'])}")
        label_branches.pack()

        label_data = tk.Label(frame, text="Report Data:")
        label_data.pack()

        for item in report_data[" Data"]:
            label_item = tk.Label(frame, text=f"{item['Item']}: {item['Value']}")
            label_item.pack()

    # Button to close the report window and go back to the root window
        close_button = tk.Button(frame, text="Close Report", command=lambda: self.close_window(window))
        close_button.pack(pady=10)

        window.protocol("WM_DELETE_WINDOW", lambda: self.close_window(window))

    def close_window(self, window):
        window.destroy()
        self.root.deiconify()


    def save_as_pdf(self, pdf_filename, report_data):

        pdf = FPDF()
        pdf.add_page()
        pdf.set_font("Arial", size=12)

        pdf.cell(200, 10, txt=f"{report_data['Report Type']} Report", ln=True, align="C")
        pdf.cell(200, 10, txt=f"Selected Branches: {', '.join(report_data['Selected Branches'])}", ln=True, align="L")

        pdf.ln(10)

        pdf.cell(200, 10, txt="Report Data:", ln=True, align="L")
        for item in report_data["Sample Data"]:
            pdf.cell(200, 10, txt=f"{item['Item']}: {item['Value']}", ln=True, align="L")

        pdf.output(pdf_filename)

    def center_window(self, window):
        window.update_idletasks()
        requested_width = window.winfo_reqwidth()
        requested_height = window.winfo_reqheight()
        x_position = (window.winfo_screenwidth() - requested_width) // 2
        y_position = (window.winfo_screenheight() - requested_height) // 2

        window.geometry(f"+{x_position}+{y_position}")

class Others:
    def __init__(self, root, login_instance):
        self.root = root
        self.root.title("OTHERS")
        self.session = Session()

        login_user = self.get_user_by_id(login_instance)
        self.user_role  = login_user.role
        self.user_restaurant = login_user.restaurant_location
        self.user_branch = login_user.branch

        self.report_type_var = tk.StringVar()

        self.branch_management_button = tk.Button(
            self.root, text="BRANCH MANAGEMENT", command=self.add_or_remove_branch
        )
        self.branch_management_button.pack()

        self.center_window(self.root)

        if self.user_role == "manager":
            self.branch_management_button.config(state=tk.DISABLED)
    
    def center_window(self, window):
        window.update_idletasks()
        requested_width = window.winfo_reqwidth()
        requested_height = window.winfo_reqheight()
        x_position = (window.winfo_screenwidth() - requested_width) // 2
        y_position = (window.winfo_screenheight() - requested_height) // 2

        window.geometry(f"+{x_position}+{y_position}")

    def get_user_by_id(self, user_id):
        session = self.session
        user = session.query(User).filter_by(id=user_id).first()
        session.close()
        return user
    
    def add_or_remove_branch(self):
        self.root.iconify()
        choice_window = tk.Toplevel(self.root)
        choice_window.resizable(False, False)

        self.center_window(choice_window)
        choice_window.title("Branch Management")
        answer = ["view", "add", "remove"]
        
        selected_answer = tk.StringVar(value=answer[0])
        combobox = ttk.Combobox(choice_window, textvariable=selected_answer, values=answer, state="readonly")
        combobox.pack()

        def handle_selection():
            action = selected_answer.get()
            if action == "view":
                self.view_branches()
                choice_window.destroy()
            elif action == "add":
                self.add_branch()
                choice_window.destroy()
            elif action == "remove":
                self.remove_branch()
                choice_window.destroy()
            else:
                messagebox.showerror("Invalid Choice", "Please select a valid option.")

        confirm_button = tk.Button(choice_window, text="Confirm", command=handle_selection)
        confirm_button.pack(pady=10)

        choice_window.protocol("WM_DELETE_WINDOW", lambda: self.show_first_window(choice_window))
        
    def add_branch(self):
        branch_window = tk.Toplevel(self.root)
        branch_window.resizable(False, False)
        branch_window.title("ADD BRANCH")
        self.center_window(branch_window)

        tk.Label(branch_window, text=f"Adding a new branch").pack(pady=10)

        new_branch = tk.Label(branch_window, text="Enter new branch:")
        new_branch.pack()

        new_branch_entry = tk.Entry(branch_window)
        new_branch_entry.pack()

        confirm_button = tk.Button(branch_window, text="Add", command=lambda: self.add(new_branch_entry.get(), branch_window ))
        confirm_button.pack(pady=10)

        self.center_window(branch_window )

        branch_window.protocol("WM_DELETE_WINDOW", lambda: self.show_first_window(branch_window))

    def remove_branch(self):

        branch_window = tk.Toplevel(self.root)
        branch_window.resizable(False, False)
        branch_window.title("REMOVE BRANCH")
        self.center_window(branch_window)

        tk.Label(branch_window, text="Removing branch").pack(pady=10)

        # Create a Combobox to display branch names
        branch_names = self.get_branches_from_tables()
        selected_branch = tk.StringVar(value=branch_names[0] if branch_names else "")  # Default to the first branch if available
        branch_combobox = ttk.Combobox(branch_window, textvariable=selected_branch, values=branch_names, state="readonly")
        branch_combobox.pack()

        confirm_button = tk.Button(branch_window, text="Remove", command=lambda: self.remove(selected_branch.get(), branch_window ))
        confirm_button.pack(pady=10)

        branch_window.protocol("WM_DELETE_WINDOW", lambda: self.show_first_window(branch_window))
    
    def view_branches(self):
        self.root.iconify()

        view_branches_window = tk.Toplevel(self.root)
        view_branches_window.resizable(False, False)
        view_branches_window.title("BRANCHES")
        self.center_window(view_branches_window)

        branch_names = self.get_branches_from_tables()

    # Create a Text widget to display branch names
        branch_text = tk.Text(view_branches_window, height=10, width=30)
        branch_text.pack()

    # Insert branch names into the Text widget
        for branch in branch_names:
            branch_text.insert(tk.END, branch + '\n')
        branch_text.config(state = tk.DISABLED)

        close_button = tk.Button(view_branches_window, text="Close", command = lambda: self.show_first_window(view_branches_window))
        close_button.pack(pady=10)

        view_branches_window.protocol("WM_DELETE_WINDOW", lambda: self.show_first_window(view_branches_window))
    
    def get_branches_from_tables(self):
        try:
            # Use SQLAlchemy to get distinct branch names from the 'tables' table
            branches = self.session.query(distinct(Table.branch)).all()

            # Extract branch names from the query result
            branch_names = [branch[0] for branch in branches]

            return branch_names
        except sqlalchemy.exc.SQLAlchemyError as e:
            messagebox.showerror("Error", f"SQLAlchemy error: {e}")
            return []
        finally:
            self.session.close()
    
    def add(self, branch, window):
        self.branch =  branch
        self.window = window

        print(branch)
        number_of_tables = 100
        restaurant = ["Restaurant 1", "Restaurant 2"]
        for values in restaurant:
            try:
                new_table_entry = Table(branch=branch, table_amount=number_of_tables, reset_date=date.today(), branch_number=values)
                self.session.add(new_table_entry)
                self.session.commit()
            except sqlalchemy.exc.SQLAlchemyError as e:
                messagebox.showerror("Error", f"SQLAlchemy error: {e}")
            
            finally:
                self.session.close()
        messagebox.showinfo("success", "Branch successfully added")
        self.show_first_window(window)
    
    def remove(self, branch, window):
        self.branch =  branch
        self.window = window
        print(branch)
        try:
            branch_to_remove = self.session.query(Table).filter(Table.branch == branch).all()
            for table in branch_to_remove:
                self.session.delete(table)
            self.session.commit()
            messagebox.showinfo("Success", f" '{branch}' successfully removed")
        except sqlalchemy.exc.SQLAlchemyError as e:
            messagebox.showerror("Error", f"SQLAlchemy error: {e}")
        finally:
            self.session.close()
        self.show_first_window(window)
    
    # def view(self,  window):
    #     self.window = window
    #     try:
    #         branch = self.session.query(Table.branch).distinct().all()
    #         rt
    #     except sqlalchemy.exc.SQLAlchemyError as e:
    #         messagebox.showerror("Error", f"SQLAlchemy error: {e}")
    #     finally:
    #         self.session.close()
    #     self.show_first_window(window)
    


    def show_first_window(self, window):
        window.destroy()
        self.root.deiconify()  
        self.center_window(self.root)
        
if __name__ == "__main__":
    root = tk.Tk()
    login_window = Login(root)
    root.mainloop()