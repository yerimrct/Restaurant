import tkinter as tk
from tkinter import messagebox, simpledialog,ttk
from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import hashlib

Base = declarative_base()

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    first_name = Column(String, nullable=False)
    last_name = Column(String, nullable=False)
    username = Column(String, nullable=False, unique=True)
    password = Column(String, nullable=False)  
    role = Column(String, nullable=False)
    usertype = Column(String, nullable=False)

    def __init__(self, first_name, last_name, username, password, role, usertype):
        self.first_name = first_name
        self.last_name = last_name
        self.username = username
        self.password = password
        self.role = role
        self.usertype = usertype
    
    # def hash_password(self, password):
    #     # Hash the password using SHA-256 (you can choose a different hash function if needed)
    #     hash_object = hashlib.sha256(password.encode())
    #     return hash_object.hexdigest()
        
class UserManagement:
    def __init__(self, root):
        self.root = root
        self.root.title("USERS")
        self.root.geometry("500x500")

        self.user_engine = create_engine('sqlite:///restaurant.db', echo=True)
        Base.metadata.create_all(self.user_engine)
        self.user_Session = sessionmaker(bind=self.user_engine)

        self.label = tk.Label(root, text="Users:")
        self.label.pack()

        self.searchLabel = tk.Label(root, text="Search by ID:")
        self.searchLabel.pack()

        self.searchEntry = tk.Entry(root)
        self.searchEntry.pack()

        self.searchButton = tk.Button(root, text="Search", command=self.search_user)
        self.searchButton.pack()

        self.listbox = tk.Listbox(root, selectmode=tk.SINGLE, exportselection=False)
        self.listbox.pack()
        self.listbox.bind('<<ListboxSelect>>', self.display_details)

        self.addButton = tk.Button(root, text="Add User", command=self.input_user)
        self.addButton.pack()

        self.removeButton = tk.Button(root, text="Remove User", command=self.remove_user)
        self.removeButton.pack()

        self.updateButton = tk.Button(root, text="Update User", command=self.update_user)
        self.updateButton.pack()

        self.showAllButton = tk.Button(root, text="Show All Users", command=self.show_all_users)
        self.showAllButton.pack()

        self.txt_details = tk.Text(root, width=50, height=10)
        self.txt_details.pack()
        self.center_window(self.root)

        self.update_listbox()

        self.usertype_options = ['admin', 'standard']
        self.usertype = tk.StringVar()
        self.usertype_combobox = ttk.Combobox(root, textvariable=self.usertype, values=self.usertype_options)

        self.role_options_1 = [ 'chef', 'manager', 'HR Director']
        self.role_1 = tk.StringVar()
        self.role_combobox_1= ttk.Combobox(root, textvariable=self.role_1, values=self.role_options_1)

        self.role_options_2 = ['front-end staff', 'back-end staff', 'delivery staff']
        self.role_2 = tk.StringVar()
        self.role_combobox_2 = ttk.Combobox(root, textvariable=self.role_2, values=self.role_options_2)

        self.branch_option = ['Bristol', 'London', 'Birmingham', 'Manchester','Nottingham', 'Glasgow','Cardiff']
        self.branch = tk.StringVar()
        self.branch_combobox = ttk.Combobox(root, textvariable=self.branch, values=self.branch_option)

        #restaurant_number is also restaurant_location
        self.restaurant_number_options = ['restaurant 1', 'restaurant 2']
        self.restaurant_number = tk.StringVar()
        self.restaurant_number_combobox= ttk.Combobox(root, textvariable=self.restaurant_number, values=self.restaurant_number_options)


    def hash_password(self, password):
        # Hash the password using SHA-256 (you can choose a different hash function if needed)
        hash_object = hashlib.sha256(password.encode())
        return hash_object.hexdigest()

    def center_window(self, window):
        window.update_idletasks()
        screen_width = window.winfo_screenwidth()
        screen_height = window.winfo_screenheight()
        requested_width = window.winfo_reqwidth()
        requested_height = window.winfo_reqheight()
        x_position = (screen_width - requested_width) // 2
        y_position = (screen_height - requested_height) // 2
        window.geometry(f"+{x_position}+{y_position}")

    def input_user(self):
        first_name = simpledialog.askstring("Input", "Enter first name:")
        if first_name:
            last_name = simpledialog.askstring("Input", "Enter last name:")
            username = simpledialog.askstring("Input", "Enter username:")

            usertype = self.dropdown(self.usertype_combobox, "Select usertype:")
        # Check if the user pressed cancel
            if usertype is None:
                messagebox.showinfo("Error", "Usertype selection canceled.")
                return False

            if usertype == "standard":
                role = self.dropdown(self.role_combobox_2, "Select role:")
            elif usertype == "ADMIN":
                role = self.dropdown(self.role_combobox_1, "Select role:")
        # Check if the user pressed cancel
            if role is None:
                messagebox.showinfo("Error", "Role selection canceled.")
                return False

            branch = self.dropdown( self.branch_combobox, "Select branch:")

            if branch is None:
                messagebox.showinfo("Error", "Branch selection canceled.")
                return False
            
            restaurant = self.dropdown( self.restaurant_number_combobox, "Select restaurant:")

            if restaurant is None:
                messagebox.showinfo("Error", "Restaurant selection canceled.")
                return False 

            password = simpledialog.askstring("Input", "Enter password:")

            if password is None:
                messagebox.showinfo("Error", "Password entry canceled.")
                return False

            hashed_password = self.hash_password(password)

            new_user = User(first_name, last_name, username, hashed_password, role, usertype, branch, restaurant)
            self.add_user(new_user)
            messagebox.showinfo("Successful", "User added successfully")
            self.update_listbox()

    def add_user(self, user):
        session = self.user_Session()
        session.add(user)
        session.commit()
        session.close()
    
    def search_user(self):
        user_id = self.searchEntry.get()
        try:
            user_id = int(user_id)
        except ValueError:
            messagebox.showinfo("Error", "Please enter a valid user ID.")
            return

        user = self.get_user_by_id(user_id)

        if user:
            details_text = f"Name: {user.first_name} {user.last_name}\nUsername: {user.username}\nRole: {user.role}\nUsertype: {user.usertype}\n"
            self.txt_details.config(state=tk.NORMAL)
            self.txt_details.delete(1.0, tk.END)
            self.txt_details.insert(tk.END, details_text)
            self.txt_details.config(state=tk.DISABLED)
        else:
            messagebox.showinfo("Error", f"No user found with ID {user_id}.")
    
    def get_user_by_id(self, user_id):
        session = self.user_Session()
        user = session.query(User).filter_by(id=user_id).first()
        session.close()
        return user

    def remove_user(self):
        selected_index = self.listbox.curselection()
        if selected_index:
            selected_user = self.get_users()[selected_index[0]]
            self.remove_user_from_db(selected_user)
            messagebox.showinfo("Successfull", "User successfully removed")
            self.update_listbox()
            
        else:
            messagebox.showinfo("Error", "Please select a user to remove.")

    def remove_user_from_db(self, user):
        session = self.user_Session()
        session.delete(user)
        session.commit()
        session.close()

    def update_user(self):
        selected_index = self.listbox.curselection()
        if selected_index:
            selected_user = self.get_users()[selected_index[0]]
            column_to_update = simpledialog.askstring("Update", "Enter column to update (first_name, last_name, username, password, role, usertype, branch or restaurant):")

            if column_to_update:
            # Check if the entered column is valid
                valid_columns = ['first_name', 'last_name', 'username', 'password', 'role', 'usertype']
                if column_to_update not in valid_columns:
                    messagebox.showinfo("Error", "Invalid column name.")
                    return
                if column_to_update == 'usertype':
                # If updating 'usertype', use the dropdown list
                    new_value = self.dropdown("Select usertype:", self.usertype_options)
                elif column_to_update == 'role':
                # If updating 'role', use the dropdown list
                    usertype = selected_user.usertype
                    if usertype == "standard":
                        role = self.dropdown(self.role_combobox_2, "Select role:")
                    elif usertype == "ADMIN":
                        role = self.dropdown(self.role_combobox_1, "Select role:")
                # Check if the user pressed cancel
                    if role is None:
                        messagebox.showinfo("Error", "Role selection canceled.")
                        return
                    new_value = role

                elif column_to_update == 'password':
                    value = simpledialog.askstring("Update", f"Enter new value for {column_to_update}:")
                # Check if the user pressed cancel
                    if value is None:
                        messagebox.showinfo("Error", "Password entry canceled.")
                        return
                    new_value = self.hash_password(value)
                    
                elif column_to_update  == "branch":
                    new_value = self.dropdown("Select new branch:", self.branch_option)  
                
                elif column_to_update  == "restaurant_number":
                    new_value = self.dropdown("Select new restaurant location:", self.restaurant_number_options)  

                else:
                    new_value = simpledialog.askstring("Update", f"Enter new value for {column_to_update}:")
                # Check if the user pressed cancel
                    if new_value is None:
                        messagebox.showinfo("Error", "Update canceled.")
                        return

            # Call the update_user_details method with the user, column to update, and the new value
                self.update(selected_user, column_to_update, new_value)
                messagebox.showinfo("Successful", "Update successful")
                self.update_listbox()
        else:
            messagebox.showinfo("Error", "Please select a user to update.")

    
    def dropdown(self, combobox, prompt):
        dialog = tk.Toplevel(self.root)
        dialog.title("Select Value")
        dialog.geometry("300x150")
        self.center_window(dialog)

        label = tk.Label(dialog, text=prompt)
        label.pack()

        selected_value = tk.StringVar()
        combobox = ttk.Combobox(dialog, textvariable=selected_value, values=combobox['values'])
        combobox.pack(pady=10)

        def on_ok():
            dialog.destroy()

        ok_button = tk.Button(dialog, text="OK", command=on_ok)
        ok_button.pack()

        dialog.wait_window(dialog)

        return selected_value.get()
    
    def update(self, user, column_to_update, new_value):
        session = self.user_Session()
        existing_user = session.query(User).get(user.id)

        if existing_user:
            # Update the selected column with the new value
            setattr(existing_user, column_to_update, new_value)
            session.commit()
        session.close()


    def show_all_users(self):
        self.txt_details.config(state=tk.NORMAL)
        all_users = self.get_users()
        users_by_role = {}

    # Group users by role
        for user in all_users:
            role = user.role
            if role not in users_by_role:
                users_by_role[role] = []
            users_by_role[role].append(user)

        details_text = ""
        for role, users in users_by_role.items():
            details_text += f"{role}:\n"
            for user in users:
                details_text += f"  Name: {user.first_name} {user.last_name}    ID: {user.id}\n"

        self.txt_details.delete(1.0, tk.END)
        self.txt_details.insert(tk.END, details_text)
        self.txt_details.config(state=tk.DISABLED)

    def get_users(self):
        session = self.user_Session()
        users = session.query(User).all()
        session.close()
        return users

    def update_listbox(self):
        self.listbox.delete(0, tk.END)
        for user in self.get_users():
            self.listbox.insert(tk.END, f"{user.first_name} {user.last_name}")

    def display_details(self, evnt):
        selected_index = self.listbox.curselection()
        if selected_index:
            selected_user = self.get_users()[selected_index[0]]
            details_text = f"Name: {selected_user.first_name} {selected_user.last_name}\nID: {selected_user.id}\nUsername: {selected_user.username}\nRole: {selected_user.role}\nUsertype: {selected_user.usertype}\n"
            self.txt_details.delete(1.0, tk.END)
            self.txt_details.insert(tk.END, details_text)
            
if __name__ == "__main__":
    root = tk.Tk()
    user_app = UserManagement(root)
    root.mainloop()

